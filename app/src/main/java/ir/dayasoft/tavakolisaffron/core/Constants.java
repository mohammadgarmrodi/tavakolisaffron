package ir.dayasoft.tavakolisaffron.core;

public final class Constants {
	public static final String HOST_URL = "http://tavakolisaffron.com/";
	public static final String RestAPIPostfix = HOST_URL +   "api/";
	public static final String DateOfOrigin = "1900-01-01 00:00:00";
	public static final int Currency = 1000;


}
