package ir.dayasoft.tavakolisaffron.core;

import android.content.Context;


import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by daya-mst on 10/15/2016.
 */

public class Date {

    static public String NowPersionDate()
    {
        PersianCalendar now = new PersianCalendar();
        return  now.getPersianYear()+ "/" + (now.getPersianMonth()+ 1) + "/" + now.getPersianDay();
    }
    static public List<String> ConvertDateToSpecialFormat(Context context, String miladiDate)//todo tabasi
    {
        //Result list:
        //position=0 -> date by special format "29دی"
        //position=1 -> shamsiDate
        List<String> list = new ArrayList<>();

        String shamsiDate=Core.Dates.ConvertMiladiToShamsi(miladiDate,false);//
        String tempStr=Core.Dates.GetShamsDateSeparated(miladiDate,context).get(3)+Core.Dates.GetShamsDateSeparated(miladiDate,context).get(2)+shamsiDate.substring(2,4);
        list.add(tempStr);
        list.add(shamsiDate);
        return list;
    }
    static public Integer GetTypeOfDay(String dayName) {
        Integer type = 0;
        switch (dayName) {
            case "شنبه":
                type = 1;
                break;
            case "یکشنبه":
                type = 2;
                break;
            case "دوشنبه":
                type = 3;
                break;
            case "سه شنبه":
                type = 4;
                break;
            case "چهارشنبه":
                type = 5;
                break;
            case "پنجشنبه":
                type = 6;
                break;
            case "جمعه":
                type = 7;
                break;
        }
        return type;
    }

    static public String MinuteToHourString(Integer minute)
    {
        Integer h=0;
        Integer m=0;
        String hourStr, minuteStr;
        if(minute!=null) {

            h = (minute / 60);
            m = (minute % 60);
        }
        if(h<10)
            hourStr="0"+h;
        else hourStr=h.toString();

        if(m<10)
            minuteStr="0"+m;
        else minuteStr=m.toString();

        return hourStr+":"+minuteStr;
    }
    static public String GetTimeWithTypeOfMinuteOrHour(Float res)
    {
        if(res<0)
            res=0F;
        String Type="دقیقه";
        if(res>=60) {
            Type = "ساعت";
            res=res/60;
        }
        String resStr= String.format("%.1f", res);
//        Float result=Float.parseFloat(resStr);
//        resStr=result.toString();
        String[] strings=resStr.split("\\.");
        if (strings.length>1&&strings[1].equals("0"))//for english language
            resStr=strings[0];
        else//for persian language
        {
            String[] tmp=res.toString().split("\\.");
            if(tmp[1].charAt(0)=='0')
            {
                resStr= String.format("%.0f", res);
            }

        }
        return (resStr+Type);
    }
    static public String ConvertToDateFormat(String date) throws ParseException {
        SimpleDateFormat curFormat = new SimpleDateFormat(
                Core.Dates.DateFormatString, Locale.US);
        String res=date;
        if(date!=null)
        {
            res=curFormat.format(curFormat.parse(date)).toString();
        }
        return res;
        /*
        curFormat.format(java.sql.Date.parse(date));
         */
    }
}
