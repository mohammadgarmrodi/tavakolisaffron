package ir.dayasoft.tavakolisaffron.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.icu.util.Freezable;

/**
 * Created by navid on 1/15/2017.
 */

public class SharePref {


    static private SharedPreferences sp;
    static private String prefName = "komori";

    private static final String USER_ID = "UserId";
    private static final String LOGIN_STEP = "LoginStep";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String  COUNTER = "CounterTime";
    private static final String  TOKEN = "Token";
    


    //  Long

    static private void set(Context context,String key, Long value)
    {
        sp = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putLong(key, value);
        editor.commit();

    }

    static private Long get(Context context,String key,Long defValue) {
        sp = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        Long res = sp.getLong(key, defValue);

        return res;
    }

    // String

    static private void set(Context context,String key, String value)
    {
        sp = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.commit();

    }

    static private String get(Context context,String key,String defValue) {
        sp = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        String res = sp.getString(key,defValue);

        return res;
    }

   // int

    static private void set(Context context,String key, int value)
    {
        sp = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(key, value);
        editor.commit();

    }

    static private int get(Context context,String key, int defValue) {
        sp = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        int res = sp.getInt(key,defValue);

        return res;
    }

    // boolean

    static private void set(Context context,String key, boolean value)
    {
        sp = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(key, value);
        editor.commit();

    }

    static private boolean get(Context context,String key, boolean defValue) {
        sp = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        boolean res = sp.getBoolean(key,defValue);

        return res;
    }

    //----------  function -------------------------

    static public void setUserId(Context context, Long id) {
       set(context,USER_ID,id);

      }
    static public Long getUserId(Context context) {

        return get(context,USER_ID,0L);
    }

    static public void setCounterTime(Context context, Long CounterTime ) {

        set(context,COUNTER,CounterTime);
    }
    static public Long getCounterTime(Context context) {

        return get(context,COUNTER,-1l);
    }

    static public void setToken(Context context, String token ) {

        set(context,TOKEN,token);
    }
    static public String getToken(Context context) {

        return get(context,TOKEN,"");
    }

    static public void setLoginStep(Context context, int step) {
        set(context ,LOGIN_STEP , step );
    }
    static public int getLoginStep(Context context) {

        return get(context , LOGIN_STEP , 0);
    }

    static public void setFirstTimeLunch(Context context, Boolean isFirstTime) {
       set(context ,IS_FIRST_TIME_LAUNCH , isFirstTime);
    }
    static public Boolean isFirstTimeLaunch(Context context) {

        return get(context , IS_FIRST_TIME_LAUNCH , true );
    }


}
