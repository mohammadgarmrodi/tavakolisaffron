package ir.dayasoft.tavakolisaffron.core;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import ir.dayasoft.tavakolisaffron.R;


public class NormalEditTextView extends EditText {

    public NormalEditTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public NormalEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);

    }

    public NormalEditTextView(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyEditText);
            String fontName = a.getString(R.styleable.MyEditText_fontNameEditText);
            if (fontName != null) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), fontName);
                setTypeface(myTypeface);
            }
            a.recycle();
        }
    }

}