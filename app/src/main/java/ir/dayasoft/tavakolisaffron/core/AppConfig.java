package ir.dayasoft.tavakolisaffron.core;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by dayapc2 on 8/21/2016.
 */

@SuppressWarnings("ALL")
public class AppConfig {

    static public Typeface GetFontTypeFace(Context context, boolean isBold) {

        Typeface tf;
        if (isBold)
            tf = Typeface.createFromAsset(context.getAssets(), "IRAN_Sans_Bold.ttf");
        else
            tf = Typeface.createFromAsset(context.getAssets(), "IRAN_Sans.ttf");

        return tf;
    }


//    static public void ClearDataTables(Context context){
//
//        context.deleteDatabase(DatabaseHelper.DATABASE_NAME);
//
//        ClearData(context);
//
//
//
//
//    }



//    static public void Logout(Context context){
//        AppConfig.SetIsLogin(context, false);
//        AppConfig.SetIsExit(context, true);
//        Intent goIntent = new Intent(context, SplashActivity.class);
//        goIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        context.startActivity(goIntent);
//
//
//
//    }

//    static public void ClearData(Context context){
//        AppConfig.SetIsLogin(context, false);
//        AppConfig.SetIsExit(context, true);
//        AppConfig.SetIsFirstTime(context, true);
//        AppConfig.setLastUpdateGrade(context,Constants.DateOfOrigin);
//        AppConfig.setLastUpdateExam(context,Constants.DateOfOrigin);
//        AppConfig.setLastUpdateExamSchoolClass(context,Constants.DateOfOrigin);
//
//        Intent goIntent = new Intent(context, SplashActivity.class);
//        goIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        context.startActivity(goIntent);
//
//
//
//    }






}
