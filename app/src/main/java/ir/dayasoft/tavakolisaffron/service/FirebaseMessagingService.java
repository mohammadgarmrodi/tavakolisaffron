package ir.dayasoft.tavakolisaffron.service;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.firebase.messaging.RemoteMessage;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.activity.BaseActivity;
import ir.dayasoft.tavakolisaffron.activity.OrderListDetailActivity;


public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {


    private static final String TAG = "MyGcmListenerService";
    public static final String INTENT_FILTER = "INTENT_FILTER";


    @Override
    public void onMessageReceived(RemoteMessage message) {


        Log.d(TAG, "Message: " + message);


        String image = message.getNotification().getIcon();
        String title = message.getNotification().getTitle();
        String text = message.getNotification().getBody();
        String sound = message.getNotification().getSound();
        String click_action=message.getNotification().getClickAction();
        int id = 0;
        Object subject = message.getData().get("subject");
        Object obj = message.getData().get("id");
        if (obj != null) {
            id = Integer.valueOf(obj.toString());


            // sendBroadCast
//            Intent intent = new Intent(INTENT_FILTER);
//            intent.putExtra("Subject", String.valueOf(subject));
//            sendBroadcast(intent);
            ///


        }

        this.sendNotification(new NotificationData(image, id, title, text, sound,click_action));
    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param notificationData GCM message received.
     */
    private void sendNotification(NotificationData notificationData) {

        Intent resultIntent = new Intent(this, OrderListDetailActivity.class);
        resultIntent.putExtra("invoiceID",notificationData.getId());



        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );


        NotificationCompat.Builder notificationBuilder = null;
        try {

            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(URLDecoder.decode(notificationData.getTitle(), "UTF-8"))
                    .setContentText(URLDecoder.decode(notificationData.getTextMessage(), "UTF-8"))
                    .setAutoCancel(true).setContentIntent(resultPendingIntent)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    ;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (notificationBuilder != null) {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(notificationData.getId(), notificationBuilder.build());
        } else {
            Log.d(TAG, "Não foi possível criar objeto notificationBuilder");
        }
    }


}
