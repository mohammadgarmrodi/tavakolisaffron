package ir.dayasoft.tavakolisaffron.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.activity.LoginActivity;
import ir.dayasoft.tavakolisaffron.activity.OrderListDetailActivity;
import ir.dayasoft.tavakolisaffron.activity.RegisterActivity;
import ir.dayasoft.tavakolisaffron.adapter.BuyBasketAdapter;
import ir.dayasoft.tavakolisaffron.api.InvoiceApi;
import ir.dayasoft.tavakolisaffron.api.InvoiceProductApi;
import ir.dayasoft.tavakolisaffron.api.KomoriProductApi;
import ir.dayasoft.tavakolisaffron.api.RestApiLifeCycle;
import ir.dayasoft.tavakolisaffron.core.NormalEditTextView;
import ir.dayasoft.tavakolisaffron.core.NormalTextView;
import ir.dayasoft.tavakolisaffron.core.SharePref;
import ir.dayasoft.tavakolisaffron.dialog.CreateDialog;
import ir.dayasoft.tavakolisaffron.model.Cart;
import ir.dayasoft.tavakolisaffron.model.Invoice;
import ir.dayasoft.tavakolisaffron.model.InvoiceProduct;
import ir.dayasoft.tavakolisaffron.model.KomoriProduct;
import ir.dayasoft.tavakolisaffron.model.SyncObject;
import ir.dayasoft.tavakolisaffron.model.User;

import static android.view.View.GONE;

/**
 * Created by mohammad on 01/24/2017.
 */

public class BuyBasketFragment extends Fragment implements RestApiLifeCycle, View.OnClickListener {

    protected RelativeLayout firstRelativeLayout;
    protected RelativeLayout buyBasketCheckLoginRelativeLayout;
    protected RelativeLayout buyBasketSubmitRelativeLayout;
    protected NormalTextView buyBasketMoreDescriptionTextView;
    protected NormalEditTextView buyBasketDescriptionEditText;
    protected RelativeLayout buyBasketMoreDescriptionRelativeLayout;
    protected NormalTextView buyBasketAddressTextView;
    protected NormalEditTextView buyBasketAddressEditText;
    protected RelativeLayout buyBasketAddressRelativeLayout;
    protected RelativeLayout buyBasketInformationRelativeLayout;
    protected NormalTextView buyBasketSubmitTextView;
    protected RelativeLayout buyBasketSubmitOrderListRelativeLayout;
    protected ImageView informationIcon;
    protected ScrollView scroolview;
    protected RelativeLayout buyBasketRelativeLayout;
    protected RelativeLayout buyBasketCheckProductRelativeLayout;
    private int count = 0;
    private String moreDescription, address;
    View rootView;
    LinearLayoutManager linearLayoutManager;
    RecyclerView mRecyclerView;
    BuyBasketAdapter buyBasketAdapter;
    private RelativeLayout emptyTextView;
    List<Cart> cartList;
    private String userAddress;
    private NormalTextView registerImageView, registerBeforeImageView;
    private int totalPrice, totalCount;
    private int invoiceID;
    private Long userID;
    private CoordinatorLayout coordinatorLayout;
    CreateDialog createDialog;
    Cart cart;
    User user;
    Invoice invoice;
    private ProgressDialog dataProgressDialog;




    public BuyBasketFragment() {
        // Required empty public constructor
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_buybasket, container, false);
        initView();
        loadData();
        bindData();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
        bindData();
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {

    }

    public static BuyBasketFragment newInstance() {
        BuyBasketFragment fragment = new BuyBasketFragment();
        return fragment;
    }

    private void initView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.buyBasket_showBuyList_recycleView);
        emptyTextView = (RelativeLayout) rootView.findViewById(R.id.buyBasket_noProduct_RelativeLayout);
        registerImageView = (NormalTextView) rootView.findViewById(R.id.buyBasket_register_textView);
        registerImageView.setOnClickListener(this);
        registerBeforeImageView = (NormalTextView) rootView.findViewById(R.id.buyBasket_login_textView);
        registerBeforeImageView.setOnClickListener(this);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        coordinatorLayout = (CoordinatorLayout) rootView.findViewById(R.id.buyBasket_snackBarPosition);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        firstRelativeLayout = (RelativeLayout) rootView.findViewById(R.id.firstRelativeLayout);
        buyBasketCheckLoginRelativeLayout = (RelativeLayout) rootView.findViewById(R.id.buyBasket_CheckLogin_RelativeLayout);
        buyBasketMoreDescriptionTextView = (NormalTextView) rootView.findViewById(R.id.buyBasket_description_textView);
        buyBasketDescriptionEditText = (NormalEditTextView) rootView.findViewById(R.id.buyBasket_description_editText);
        buyBasketMoreDescriptionRelativeLayout = (RelativeLayout) rootView.findViewById(R.id.buyBasket_description_RelativeLayout);
        buyBasketAddressTextView = (NormalTextView) rootView.findViewById(R.id.buyBasket_address_textView);
        buyBasketAddressEditText = (NormalEditTextView) rootView.findViewById(R.id.buyBasket_address_editText);
        buyBasketAddressRelativeLayout = (RelativeLayout) rootView.findViewById(R.id.buyBasket_address_RelativeLayout);
        buyBasketInformationRelativeLayout = (RelativeLayout) rootView.findViewById(R.id.buyBasket_information_relativeLayout);
        buyBasketSubmitTextView = (NormalTextView) rootView.findViewById(R.id.buyBasket_submit_textView);
        buyBasketSubmitRelativeLayout = (RelativeLayout) rootView.findViewById(R.id.buyBasket_submit_relativeLayout);
        buyBasketSubmitOrderListRelativeLayout = (RelativeLayout) rootView.findViewById(R.id.buyBasket_submitOrderList_relativeLayout);
        buyBasketSubmitTextView.setOnClickListener(this);
        informationIcon = (ImageView) rootView.findViewById(R.id.information_icon);
        informationIcon = (ImageView) rootView.findViewById(R.id.informationIcon);
        scroolview = (ScrollView) rootView.findViewById(R.id.scroolview);
        buyBasketRelativeLayout = (RelativeLayout) rootView.findViewById(R.id.buyBasket_RelativeLayout);
        buyBasketCheckProductRelativeLayout = (RelativeLayout) rootView.findViewById(R.id.buyBasket_CheckProduct_RelativeLayout);
        buyBasketCheckProductRelativeLayout.setOnClickListener(this);
        registerImageView.setOnClickListener(this);
        registerBeforeImageView.setOnClickListener(this);
        dataProgressDialog = new ProgressDialog(getContext());
        dataProgressDialog.setCancelable(false);
        dataProgressDialog.setMessage("لطفا صبر کنید...");
        mRecyclerView.setNestedScrollingEnabled(false);

    }

    private void getIntentData() {
        // Type = getArguments().getInt(TypeOfFragment);
    }

    private void loadData() {
        user = new User();
        userID = user.GetUser(getContext()).getUserID();
        userAddress = user.Get(getContext(), userID).getAddress();
        cartList = Cart.GetCartList(getContext());
        totalCount = Cart.GetCartList(getContext()).size();
        totalPrice = Cart.GetSum(getContext()).getKomoriProduct().getPrice();
        moreDescription = buyBasketDescriptionEditText.getText().toString();
        address = buyBasketAddressEditText.getText().toString();
        if (SharePref.getLoginStep(getContext()) == User.STEP_FIRST_LOGIN) {
            buyBasketCheckLoginRelativeLayout.setVisibility(View.VISIBLE);
            buyBasketCheckProductRelativeLayout.setVisibility(GONE);
        }
        if (SharePref.getLoginStep(getContext()) == User.STEP_FINISH_LOGIN) {
            buyBasketCheckLoginRelativeLayout.setVisibility(GONE);
            buyBasketSubmitRelativeLayout.setVisibility(View.VISIBLE);
            buyBasketCheckProductRelativeLayout.setVisibility(View.VISIBLE);
        }
    }

    public void refresh() {
        buyBasketSubmitRelativeLayout.setVisibility(GONE);
        emptyTextView.setVisibility(View.VISIBLE);
        buyBasketCheckProductRelativeLayout.setVisibility(GONE);
        buyBasketAddressRelativeLayout.setVisibility(GONE);
        buyBasketMoreDescriptionRelativeLayout.setVisibility(GONE);

    }

    private void bindData() {
        if (cartList.size() == 0) {
            emptyTextView.setVisibility(View.VISIBLE);
            buyBasketCheckProductRelativeLayout.setVisibility(GONE);
            buyBasketSubmitOrderListRelativeLayout.setVisibility(GONE);
            buyBasketMoreDescriptionRelativeLayout.setVisibility(View.GONE);
            buyBasketAddressRelativeLayout.setVisibility(View.GONE);
            mRecyclerView.setVisibility(GONE);
            buyBasketInformationRelativeLayout.setVisibility(GONE);
        } else {
            emptyTextView.setVisibility(GONE);
            mRecyclerView.setHasFixedSize(true);
            buyBasketAdapter = new BuyBasketAdapter(getActivity(), cartList, BuyBasketFragment.this);
            mRecyclerView.setAdapter(buyBasketAdapter);
        }

    }

    private void attachFragment() {
        // getSupportFragmentManager().a
    }

    private void showDialge() {

    }

    @Override
    public void feedback(Integer type, Object object, int section) {
        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:
                if (section == RestApiLifeCycle.STEP_COMPLETE_INVOICE_PRODUCT) {
                    dataProgressDialog.dismiss();
                    cart = new Cart();
                    cart.Clear(getContext());
                    Intent intent = new Intent(getContext(), OrderListDetailActivity.class);
                    intent.putExtra("invoiceID", invoiceID);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else if (section == RestApiLifeCycle.STEP_COMPLETE_CHECKSTOCK) {
                    dataProgressDialog.dismiss();
                    buyBasketSubmitOrderListRelativeLayout.setVisibility(View.VISIBLE);
                    buyBasketCheckProductRelativeLayout.setVisibility(GONE);
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "محصولات موجود می باشد", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                break;
            case RestApiLifeCycle.STEP_ERROR:
                if (section == RestApiLifeCycle.STEP_ERROR_CHECKSTOCK) {
                    dataProgressDialog.dismiss();
                    createDialog = new CreateDialog(getContext());
                    createDialog.checkProduct((ArrayList) object);
                } else if (section == RestApiLifeCycle.STEP_ERROR_INVOICE) {
                    dataProgressDialog.dismiss();
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "خطا در ارتباط با سرور...لحضاتی دیگر تلاش کنید", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                else if(section==1){
                    dataProgressDialog.dismiss();
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "خطا در ارتباط با سرور...لحضاتی دیگر تلاش کنید", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                else if(section==RestApiLifeCycle.STEP_ERROR_INVOICE){
                    dataProgressDialog.dismiss();
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "خطا در ارتباط با سرور", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                else if(section==RestApiLifeCycle.STEP_ERROR_CHECKSTOCK){
                    dataProgressDialog.dismiss();
                    createDialog = new CreateDialog(getContext());
                    createDialog.checkProduct((ArrayList) object);
                }
                break;
            case RestApiLifeCycle.STEP_START:
                break;
        }
    }

    @Override
    public void passUser(Integer type, User user, int section) {

    }

    @Override
    public void passSyncObject(Integer type, SyncObject syncObject, int section) {
        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:
                if (section == RestApiLifeCycle.STEP_COMPLETE_INVOICE) {
                    dataProgressDialog.dismiss();
                    invoiceID = Integer.valueOf(syncObject.getServerID());
                    InvoiceProductApi invoiceProductApi = new InvoiceProductApi(this, getContext());
                    invoiceProductApi.setOffset(0);
                    invoiceProductApi.setPageSize(5);
                    invoiceProductApi.setUpdateDate(InvoiceProduct.GetLast(getContext()).getUpdateDate());
                    invoiceProductApi.setInvoiceid(invoiceID);
                    invoiceProductApi.GetInvoiceProductByID(getContext());

                    InvoiceApi invoiceApi = new InvoiceApi(this, getContext());
                    invoiceApi.setOffset(0);
                    invoiceApi.setPageSize(5);
                    invoiceApi.setUpdateDate(Invoice.GetLast(getContext()).getUpdateDate());
                    invoiceApi.GetInvoice(getContext());
                }
                break;
            case RestApiLifeCycle.STEP_ERROR:

                break;
            case RestApiLifeCycle.STEP_START:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buyBasket_register_textView:
                Intent intent = new Intent(getContext(), RegisterActivity.class);
                startActivity(intent);
                break;
            case R.id.buyBasket_CheckProduct_RelativeLayout:
                dataProgressDialog.show();
                KomoriProductApi komoriProductApi = new KomoriProductApi(this, getContext());
                KomoriProduct komoriProduct = new KomoriProduct();
                komoriProduct.setCartList(Cart.Get(getContext()));
                komoriProductApi.PostCheckStock(getContext(), komoriProduct);
                break;
            case R.id.buyBasket_login_textView:
                Intent intent1 = new Intent(getContext(), LoginActivity.class);
                startActivity(intent1);
                break;
            case R.id.buyBasket_submit_textView:
                count++;
                if (count == 1) {
                    buyBasketMoreDescriptionRelativeLayout.setVisibility(View.VISIBLE);
                    buyBasketDescriptionEditText.requestFocus();
                } else if (count == 2) {
                    if (userAddress == null) {
                        userAddress = "";
                    }
                    buyBasketAddressEditText.setText(String.valueOf(userAddress));
                    buyBasketMoreDescriptionRelativeLayout.setVisibility(View.VISIBLE);
                    buyBasketAddressRelativeLayout.setVisibility(View.VISIBLE);
                    buyBasketInformationRelativeLayout.setVisibility(View.VISIBLE);
                    buyBasketAddressEditText.requestFocus();
                } else {
                    if (buyBasketAddressEditText.getText().length() == 0) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "آدرس را وارد کنید", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else {
                        dataProgressDialog.show();
                        moreDescription = buyBasketDescriptionEditText.getText().toString();
                        address = buyBasketAddressEditText.getText().toString();
                        InvoiceApi invoiceApi = new InvoiceApi(BuyBasketFragment.this, getContext());
                        invoice = new Invoice();
                        invoice.setAddress(address);
                        invoice.setDescription(moreDescription);
                        invoice.setPrice(totalPrice);
                        invoice.setCount(totalCount);
                        invoice.setCartList(Cart.Get(getContext()));
                        invoiceApi.PostInvoice(getContext(), invoice);
                    }
                }
                break;
        }
    }


}
