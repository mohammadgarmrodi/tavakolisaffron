package ir.dayasoft.tavakolisaffron.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.adapter.CategoryAdapter;
import ir.dayasoft.tavakolisaffron.api.RestApiLifeCycle;
import ir.dayasoft.tavakolisaffron.model.Category;
import ir.dayasoft.tavakolisaffron.model.SyncObject;
import ir.dayasoft.tavakolisaffron.model.User;


/**
 * Created by mohammad on 02/18/2017.
 */

public class CategoryFragment extends Fragment implements View.OnClickListener  {

    View rootView;
    private List<Category> categoryList;
    private RecyclerView mRecyclerView;
    LinearLayoutManager linearLayoutManager;
    private CategoryAdapter subCategoryAdapter;
    private int categoryID;
    public static String ARG_CategoryID = "categoryID";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_category, container, false);
        getIntentData();
        initView();
        loadData();
        bindData();
        return rootView;
    }

    private void initView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.subcategory_product_recycleView);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(linearLayoutManager);


      /*  //You need to add the following line for this solution to work; thanks skayred
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener( new View.OnKeyListener()
        {
            @Override
            public boolean onKey( View v, int keyCode, KeyEvent event )
            {
                if( keyCode == KeyEvent.KEYCODE_BACK )
                {
                    return true;
                }
                return false;
            }
        } );*/

    }

    public CategoryFragment() {
        // Required empty public constructor
    }

    public static CategoryFragment newInstance() {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private void getIntentData() {

    }

    private void loadData() {
        categoryList = Category.GetCategoryByParent(getContext(), 0);
    }

    private void bindData() {
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        subCategoryAdapter = new CategoryAdapter(getContext(), categoryList, CategoryFragment.this);
        mRecyclerView.setAdapter(subCategoryAdapter);
    }

    private void attachFragment() {
        // getSupportFragmentManager().a
    }

    private void showDialge() {

    }



    public Boolean popUpCategory()
    {
        if (categoryID==0)
        {
            return true;
        }
        else
        {
            categoryList=Category.GetCategoryByParent(getActivity(),categoryList.get(0).getParentID());
            subCategoryAdapter.notifyDataSetChanged();

            return false;
        }
    }

    @Override
    public void onClick(View v) {

    }

    public void LoadSubData(int categoryID) {
        categoryList = Category.GetCategoryByParent(getContext(), categoryID);
        subCategoryAdapter = new CategoryAdapter(getContext(), categoryList, CategoryFragment.this);
        mRecyclerView.setAdapter(subCategoryAdapter);
    }

    public void refreshfragment() {
        loadData();
        bindData();
    }

    public void LoadFragment(int categoryID) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, KomoriProductFragment.newInstance(categoryID)).addToBackStack(null);
        fragmentTransaction.commit();
    }



}
