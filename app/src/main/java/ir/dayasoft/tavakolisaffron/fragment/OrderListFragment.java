package ir.dayasoft.tavakolisaffron.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.adapter.OrderListAdapter;
import ir.dayasoft.tavakolisaffron.api.InvoiceApi;
import ir.dayasoft.tavakolisaffron.api.RestApiLifeCycle;
import ir.dayasoft.tavakolisaffron.core.SharePref;
import ir.dayasoft.tavakolisaffron.model.Invoice;
import ir.dayasoft.tavakolisaffron.model.SyncObject;
import ir.dayasoft.tavakolisaffron.model.User;
import ir.dayasoft.tavakolisaffron.utility.NetworkHelper;

import static android.view.View.GONE;

/**
 * Created by mohammad on 01/24/2017.
 */

public class OrderListFragment extends Fragment implements RestApiLifeCycle, View.OnClickListener {


    protected ImageView informationIcon;
    protected RelativeLayout orderListEmptyRelativeLayout;
    protected CoordinatorLayout orderListSnackBarPosition;
    View rootView;
    LinearLayoutManager linearLayoutManager;
    RecyclerView mRecyclerView;
    OrderListAdapter orderListAdapter;
    private List<Invoice> invoiceList;
    private TextView emptyTextView;
    InvoiceApi invoiceApi;
    ProgressDialog dataProgressDialog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_orderlist, container, false);
        if (NetworkHelper.isOnline(getActivity())) {
            initView();
            loadData();
        }
        else {

        }
        return rootView;
    }




    public static OrderListFragment newInstance() {
        OrderListFragment fragment = new OrderListFragment();
        return fragment;
    }

    private void initView() {

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.orderList_showOrder_RecycleView);
        emptyTextView = (TextView) rootView.findViewById(R.id.orderList_empty_textView);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        informationIcon = (ImageView) rootView.findViewById(R.id.information_icon);
        orderListEmptyRelativeLayout = (RelativeLayout) rootView.findViewById(R.id.orderList_empty_relativeLayout);
        orderListSnackBarPosition = (CoordinatorLayout) rootView.findViewById(R.id.orderList_snackBarPosition);
        dataProgressDialog = new ProgressDialog(getContext());
        dataProgressDialog.setMessage("لطفا صبر کنید...");
        dataProgressDialog.setCancelable(false);

    }

    private void getIntentData() {

        // Type = getArguments().getInt(TypeOfFragment);

    }

    private void loadData() {
        dataProgressDialog.show();
        invoiceList = Invoice.Get(getContext());
        invoiceApi = new InvoiceApi(OrderListFragment.this, getContext());
        invoiceApi.setOffset(0);
        invoiceApi.setPageSize(5);
        invoiceApi.setUpdateDate(Invoice.GetLast(getContext()).getUpdateDate());
        invoiceApi.GetInvoice(getContext());
    }

    private void bindData() {
        if (invoiceList.size() == 0) {
            orderListEmptyRelativeLayout.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(GONE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            orderListAdapter = new OrderListAdapter(getActivity(), invoiceList);
            mRecyclerView.setAdapter(orderListAdapter);
        }
    }

    private void attachFragment() {
        // getSupportFragmentManager().a
    }

    private void showDialge() {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void feedback(Integer type, Object object, int section) {
        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:
                if (section == RestApiLifeCycle.STEP_COMPLETE_INVOICE) {
                    dataProgressDialog.dismiss();
                    bindData();
                }
                break;
            case RestApiLifeCycle.STEP_ERROR:
                if (section == RestApiLifeCycle.STEP_ERROR_INVOICE) {
                    dataProgressDialog.dismiss();
                    orderListEmptyRelativeLayout.setVisibility(View.VISIBLE);
                    emptyTextView.setVisibility(View.VISIBLE);
                    emptyTextView.setText("خطا در ارتباط با سرور");
                    Snackbar snackbar = Snackbar.make(orderListSnackBarPosition, "خطا در دریافت اطلاعات...لحظاتی دیگر تلاش کنید", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                break;
            case RestApiLifeCycle.STEP_START:
                break;
        }
    }

    @Override
    public void passUser(Integer type, User user, int section) {

    }

    @Override
    public void passSyncObject(Integer type, SyncObject syncObject, int section) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }


    }
}
