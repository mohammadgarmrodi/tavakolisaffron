package ir.dayasoft.tavakolisaffron.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.adapter.ProductAdapter;
import ir.dayasoft.tavakolisaffron.model.Cart;
import ir.dayasoft.tavakolisaffron.model.KomoriProduct;
import ir.dayasoft.tavakolisaffron.utility.ItemOffsetDecoration;


/**
 * Created by mohammad on 01/24/2017.
 */

public class KomoriProductFragment extends Fragment implements View.OnClickListener {

    protected LinearLayout komoriProductPriceFilter;
    protected LinearLayout komoriProductWeightFilter;
    protected LinearLayout komoriProductCompanyFilter;
    protected LinearLayout filterLinearLayout;
    View rootView;
    LinearLayoutManager linearLayoutManager;
    RecyclerView mRecyclerView;
    ProductAdapter komoriProductAdapter;
    List<KomoriProduct> komoriProductList;
    int categoryID;
    public static String ARG_CategoryID = "categoryID";

    public KomoriProductFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_komoriproduct, container, false);
        getArgumentsData();
        initView();
        loadData();
        bindData();
        return rootView;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {

    }

    private void initView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.komoriProduct_productList_recycleView);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(linearLayoutManager);


    }

    public static KomoriProductFragment newInstance(int Fk_CategoryID) {
        Bundle args = new Bundle();
        KomoriProductFragment fragment = new KomoriProductFragment();
        args.putInt(ARG_CategoryID, Fk_CategoryID);
        fragment.setArguments(args);
        return fragment;
    }

    private void getArgumentsData() {
        categoryID = getArguments().getInt(ARG_CategoryID);
    }

    private void getIntentData() {
    }

    private void loadData() {
        komoriProductList = KomoriProduct.GetProudctByCategory(getContext(), categoryID);

    }

    private void bindData() {
        komoriProductAdapter = new ProductAdapter(getActivity(), komoriProductList);
        mRecyclerView.setAdapter(komoriProductAdapter);
    }

    private void attachFragment() {
        // getSupportFragmentManager().a
    }

    private void showDialge() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.komoriProduct_company_filter:
//                komoriProductList = Weight.GetProductByCompany(getContext(), categoryID);
//                komoriProductAdapter = new KomoriProductAdapter(getActivity(), komoriProductList);
//                mRecyclerView.setAdapter(komoriProductAdapter);
//                komoriProductCompanyFilter.setBackgroundColor(getContext().getResources().getColor(R.color.filterBackgroundPressed));
//                komoriProductPriceFilter.setBackgroundColor(getContext().getResources().getColor(R.color.filterProductBackground));
//                komoriProductWeightFilter.setBackgroundColor(getContext().getResources().getColor(R.color.filterProductBackground));
//                komoriProductAdapter.notifyDataSetChanged();
//                break;
//            case R.id.komoriProduct_price_filter:
//                komoriProductList = Weight.GetProductByPrice(getContext(), categoryID);
//                komoriProductAdapter = new KomoriProductAdapter(getActivity(), komoriProductList);
//                mRecyclerView.setAdapter(komoriProductAdapter);
//                komoriProductPriceFilter.setBackgroundColor(getContext().getResources().getColor(R.color.filterBackgroundPressed));
//                komoriProductCompanyFilter.setBackgroundColor(getContext().getResources().getColor(R.color.filterProductBackground));
//                komoriProductWeightFilter.setBackgroundColor(getContext().getResources().getColor(R.color.filterProductBackground));
//                komoriProductAdapter.notifyDataSetChanged();
//                break;
//            case R.id.komoriProduct_weight_filter:
//                komoriProductList = Weight.GetProductByWeight(getContext(), categoryID);
//                komoriProductAdapter = new KomoriProductAdapter(getActivity(), komoriProductList);
//                mRecyclerView.setAdapter(komoriProductAdapter);
//                komoriProductPriceFilter.setBackgroundColor(getContext().getResources().getColor(R.color.filterProductBackground));
//                komoriProductWeightFilter.setBackgroundColor(getContext().getResources().getColor(R.color.filterBackgroundPressed));
//                komoriProductCompanyFilter.setBackgroundColor(getContext().getResources().getColor(R.color.filterProductBackground));
//                komoriProductAdapter.notifyDataSetChanged();
//                break;
        }
    }

}