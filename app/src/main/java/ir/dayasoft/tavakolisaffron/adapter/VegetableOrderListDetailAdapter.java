package ir.dayasoft.tavakolisaffron.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.core.Core;
import ir.dayasoft.tavakolisaffron.holder.MainViewHolder;
import ir.dayasoft.tavakolisaffron.holder.OrderListDetailHolder;
import ir.dayasoft.tavakolisaffron.model.PreFactor;

/**
 * Created by mohammad on 02/16/2017.
 */

public class VegetableOrderListDetailAdapter extends RecyclerView.Adapter<MainViewHolder> {
    private List<PreFactor> mData;
    private Context mContext;

    public VegetableOrderListDetailAdapter(Context context, List<PreFactor> data) {
        this.mData = data;
        this.mContext = context;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater fInflater = LayoutInflater.from(parent.getContext());
        ViewGroup v = (ViewGroup) fInflater.inflate(R.layout.list_item_orderlistdetail, parent, false);
        OrderListDetailHolder orderListDetailHolder = new OrderListDetailHolder(v);
        return orderListDetailHolder;
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        final OrderListDetailHolder orderListDetailHolder = (OrderListDetailHolder) holder;
        if ((position - 1) % 2 == 0) {
            orderListDetailHolder.getItemLinearLayout().setBackgroundColor(mContext.getResources().getColor(R.color.BackgroundItem));
        } else {
            orderListDetailHolder.getItemLinearLayout().setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
        orderListDetailHolder.getCompanyTextView().setText(mData.get(position).getName());
        orderListDetailHolder.getCountTextView().setText(String.valueOf(mData.get(position).getCount()));
        orderListDetailHolder.getPriceTextView().setText(String.valueOf(Core.Converter.DoubleToString(mData.get(position).getPrice())));
        orderListDetailHolder.getWeightTextView().setText(String.valueOf(mData.get(position).getWeight()));
        orderListDetailHolder.getSumPriceTextView().setText(String.valueOf(Core.Converter.DoubleToString(mData.get(position).getCount() * mData.get(position).getPrice())));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public int getItemViewType(int position) {
        if (position % 2 == 1) {
            return 0;
        } else {
            return 1;
        }
    }
}


