package ir.dayasoft.tavakolisaffron.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;


import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.activity.BaseActivity;
import ir.dayasoft.tavakolisaffron.activity.OrderListDetailActivity;
import ir.dayasoft.tavakolisaffron.core.Core;
import ir.dayasoft.tavakolisaffron.holder.MainViewHolder;
import ir.dayasoft.tavakolisaffron.holder.OrderListHolder;
import ir.dayasoft.tavakolisaffron.model.Invoice;

/**
 * Created by mohammad on 01/31/2017.
 */

public class OrderListAdapter extends RecyclerView.Adapter<MainViewHolder> {
    private List<Invoice> mData;
    private Context mContext;

    public OrderListAdapter(Context context, List<Invoice> data) {
        this.mData = data;
        this.mContext = context;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater fInflater = LayoutInflater.from(parent.getContext());
        ViewGroup v = (ViewGroup) fInflater.inflate(R.layout.list_item_orderlist, parent, false);
        OrderListHolder orderListHolder = new OrderListHolder(v);
        return orderListHolder;

    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, final int position) {
        final OrderListHolder orderListHolder = (OrderListHolder) holder;
        orderListHolder.getInvoiceTextView().setText(String.valueOf(mData.get(position).getInvoiceID()));
        orderListHolder.getPriceTextView().setText(String.valueOf(Core.Converter.DoubleToString(mData.get(position).getPrice())));
        orderListHolder.getCountTextView().setText(String.valueOf(mData.get(position).getCount()));
        List<String> date = Core.Dates.GetShamsDateSeparated(mData.get(position).getCreateDate(), mContext);

        if (date.size() > 0) {
            orderListHolder.getTodayDateTextView().setText((date.get(1) + " " + date.get(0) + " " + date.get(2)));
        } else {
            orderListHolder.getTodayDateTextView().setText(("زمان مشخص نمی باشد"));
        }
        orderListHolder.getItemOrderList().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderListDetailActivity.class);
                intent.putExtra("invoiceID", mData.get(position).getInvoiceID());
                intent.putExtra("invoiceSumPrice", mData.get(position).getPrice());
                intent.putExtra("invoiceSumCount", mData.get(position).getCount());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mContext.startActivity(intent);
                ((BaseActivity) mContext).finish();
            }
        });
        if (mData.get(position).getStatus() == 1) {
            orderListHolder.getCheckLayout().setVisibility(View.VISIBLE);
            orderListHolder.getConfirmLayout().setVisibility(View.GONE);
            orderListHolder.getCompleteLayout().setVisibility(View.GONE);
            orderListHolder.getErrorLayout().setVisibility(View.GONE);
            orderListHolder.getOrderListOnlinePaymentLinearLayout().setVisibility(View.GONE);
            orderListHolder.getOfflinepaymentLinearLayout().setVisibility(View.GONE);
        } else if (mData.get(position).getStatus() == 2) {
            orderListHolder.getErrorLayout().setVisibility(View.VISIBLE);
            orderListHolder.getConfirmLayout().setVisibility(View.GONE);
            orderListHolder.getCheckLayout().setVisibility(View.GONE);
            orderListHolder.getCompleteLayout().setVisibility(View.GONE);
            orderListHolder.getOrderListOnlinePaymentLinearLayout().setVisibility(View.GONE);
            orderListHolder.getOfflinepaymentLinearLayout().setVisibility(View.GONE);
        } else if (mData.get(position).getStatus() == 3) {


            if (mData.get(position).getInvoiceItemPaymentStatus() == 1) {

                orderListHolder.getCheckLayout().setVisibility(View.GONE);
                orderListHolder.getConfirmLayout().setVisibility(View.GONE);
                orderListHolder.getCompleteLayout().setVisibility(View.GONE);
                orderListHolder.getErrorLayout().setVisibility(View.GONE);
                orderListHolder.getOrderListOnlinePaymentLinearLayout().setVisibility(View.GONE);
                orderListHolder.getOfflinepaymentLinearLayout().setVisibility(View.VISIBLE);
            } else if (mData.get(position).getInvoiceItemPaymentStatus() == 2) {


                orderListHolder.getCheckLayout().setVisibility(View.GONE);
                orderListHolder.getConfirmLayout().setVisibility(View.GONE);
                orderListHolder.getCompleteLayout().setVisibility(View.GONE);
                orderListHolder.getErrorLayout().setVisibility(View.GONE);
                orderListHolder.getOrderListOnlinePaymentLinearLayout().setVisibility(View.VISIBLE);
                orderListHolder.getOfflinepaymentLinearLayout().setVisibility(View.GONE);

            } else {

                orderListHolder.getCompleteLayout().setVisibility(View.GONE);
                orderListHolder.getErrorLayout().setVisibility(View.GONE);
                orderListHolder.getConfirmLayout().setVisibility(View.VISIBLE);
                orderListHolder.getCheckLayout().setVisibility(View.GONE);
                orderListHolder.getOrderListOnlinePaymentLinearLayout().setVisibility(View.GONE);
                orderListHolder.getOfflinepaymentLinearLayout().setVisibility(View.GONE);

            }


        } else if (mData.get(position).getStatus() == 4) {
            orderListHolder.getCompleteLayout().setVisibility(View.VISIBLE);
            orderListHolder.getErrorLayout().setVisibility(View.GONE);
            orderListHolder.getConfirmLayout().setVisibility(View.GONE);
            orderListHolder.getCheckLayout().setVisibility(View.GONE);
            orderListHolder.getOrderListOnlinePaymentLinearLayout().setVisibility(View.GONE);
            orderListHolder.getOfflinepaymentLinearLayout().setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
