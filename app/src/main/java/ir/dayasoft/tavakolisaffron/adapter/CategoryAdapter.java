package ir.dayasoft.tavakolisaffron.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.fragment.CategoryFragment;
import ir.dayasoft.tavakolisaffron.holder.MainViewHolder;
import ir.dayasoft.tavakolisaffron.holder.CategoryHolder;
import ir.dayasoft.tavakolisaffron.model.Category;

/**
 * Created by mohammad on 02/18/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<MainViewHolder> {
    private List<Category> mData;
    private Context mContext;
    CategoryFragment fragment;
    private int categoryID;
    private List<Category> categoryList;


    public CategoryAdapter(Context context, List<Category> data, CategoryFragment fragment) {
        this.mData = data;
        this.mContext = context;
        this.fragment = fragment;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater fInflater = LayoutInflater.from(parent.getContext());
        ViewGroup v = (ViewGroup) fInflater.inflate(R.layout.list_item_category, parent, false);
        CategoryHolder subCategoryHolder = new CategoryHolder(v);
        return subCategoryHolder;

    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, final int position) {
        final CategoryHolder subCategoryHolder = (CategoryHolder) holder;
        categoryID = mData.get(position).getCategoryID();
        subCategoryHolder.getProductDetailTextView().setText(mData.get(position).getName());

        if ((position - 1) % 2 == 0) {
            subCategoryHolder.getProductDetailItem().setBackgroundColor(mContext.getResources().getColor(R.color.BackgroundItem));
        } else {
            subCategoryHolder.getProductDetailItem().setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
        subCategoryHolder.getProductDetailItem().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryID = mData.get(position).getCategoryID();
                categoryList = Category.GetCategoryByParent(mContext, categoryID);
                if (categoryList.size() > 0) {
                    fragment.LoadSubData(categoryID);
                    mData = categoryList;
                    notifyDataSetChanged();
                } else {
                    fragment.LoadFragment(categoryID);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public int getItemViewType(int position) {
        if (position % 2 == 1) {
            return 0;
        } else {
            return 1;
        }
    }

}
