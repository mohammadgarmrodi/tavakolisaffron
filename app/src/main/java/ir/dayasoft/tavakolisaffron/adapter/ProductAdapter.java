package ir.dayasoft.tavakolisaffron.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;

import java.util.List;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.activity.ProductDetailActivity;
import ir.dayasoft.tavakolisaffron.core.Core;
import ir.dayasoft.tavakolisaffron.holder.KomoriProductHolder;
import ir.dayasoft.tavakolisaffron.holder.MainViewHolder;
import ir.dayasoft.tavakolisaffron.model.Cart;
import ir.dayasoft.tavakolisaffron.model.KomoriProduct;
import ir.dayasoft.tavakolisaffron.volly.MySingleton;


/**
 * Created by mohammad on 01/18/2017.
 */

public class ProductAdapter extends RecyclerView.Adapter<MainViewHolder> {
    private List<KomoriProduct> mData;
    private Context mContext;
    private String productName, productWeight, productPrice;
    private int komoriProductID;
    private Integer cartNumber, cartPrice;
    private String NAME = "Name";
    private String WEIGHT = "Weight";
    private String PRICE = "Price";
    private String DESCRIPTION = "Description";
    private String IMAGE = "Image";
    private String Count = "Count";
    private ImageLoader imageLoader;


    public ProductAdapter(Context context, List<KomoriProduct> data) {
        this.mContext = context;
        this.mData = data;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater fInflater = LayoutInflater.from(parent.getContext());
        ViewGroup v = (ViewGroup) fInflater.inflate(R.layout.list_item_komoriproduct, parent, false);
        KomoriProductHolder komoriProductHolder = new KomoriProductHolder(v);
        return komoriProductHolder;
    }

    @Override
    public void onBindViewHolder(final MainViewHolder holder, final int position) {
        final KomoriProductHolder komoriProductHolder = (KomoriProductHolder) holder;
        productName = mData.get(position).getName();
        productPrice = String.valueOf(mData.get(position).getPrice());
        productWeight = String.valueOf(mData.get(position).getWeight());
        komoriProductID = mData.get(position).getKomoriProductID();
        cartPrice = mData.get(position).getPrice();
        imageLoader = MySingleton.getInstance(mContext).getImageLoader();
        LoadImage(position, komoriProductHolder);
        cartNumber = Cart.Get(mContext, komoriProductID).getCount();
        komoriProductHolder.getCompanyTextView().setText(productName);
        cartNumber = Cart.Get(mContext, komoriProductID).getCount();
        if(cartNumber==null)
            cartNumber=0;
        if(cartNumber==0){
            komoriProductHolder.getIconRelativeLayout().setVisibility(View.GONE);
        }
        else {
            komoriProductHolder.getIconRelativeLayout().setVisibility(View.VISIBLE);

        }

        komoriProductHolder.getPriceTextView().setText(String.valueOf(Core.Converter.DoubleToString(mData.get(position).getPrice())));

        if (mData.get(position).getShowSubDetail()) {
            komoriProductHolder.getDownShape().setImageResource(R.drawable.upshape);
            komoriProductHolder.getSecondLinearLayout().setVisibility(View.VISIBLE);
            komoriProductHolder.getIconRelativeLayout().setBackgroundColor(mContext.getResources().getColor(R.color.backgroundSecondLinearLayout));
            komoriProductHolder.getMainLinearLayout().setBackgroundColor(mContext.getResources().getColor(R.color.backgroundSecondLinearLayout));
            komoriProductHolder.getSecondLinearLayout().setBackgroundColor(mContext.getResources().getColor(R.color.backgroundSecondLinearLayout));
        } else {
            komoriProductHolder.getDownShape().setImageResource(R.drawable.downshape);
            komoriProductHolder.getSecondLinearLayout().setVisibility(View.GONE);
            komoriProductHolder.getIconRelativeLayout().setBackgroundColor(mContext.getResources().getColor(R.color.BackgroundItem));
            komoriProductHolder.getMainLinearLayout().setBackgroundColor(mContext.getResources().getColor(R.color.BackgroundItem));
            if ((position - 1) % 2 == 0) {
                komoriProductHolder.getIconRelativeLayout().setBackgroundColor(mContext.getResources().getColor(R.color.white));
                komoriProductHolder.getMainLinearLayout().setBackgroundColor(mContext.getResources().getColor(R.color.white));
            } else {
                komoriProductHolder.getIconRelativeLayout().setBackgroundColor(mContext.getResources().getColor(R.color.white));
                komoriProductHolder.getMainLinearLayout().setBackgroundColor(mContext.getResources().getColor(R.color.white));
            }

        }

        if (mData.get(position).getWeight() == null) {
            komoriProductHolder.getWeightTextView().setVisibility(View.GONE);
        } else {
            komoriProductHolder.getWeightTextView().setText(productWeight );

        }

        komoriProductHolder.getNumberTextView().setText(String.valueOf(cartNumber));
        komoriProductHolder.getSumPriceTextView().setText(String.valueOf(Core.Converter.DoubleToString(cartNumber * cartPrice)));

        komoriProductHolder.getKomoriProductPicutreImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductDetailActivity.class);
                intent.putExtra(NAME, mData.get(position).getName());
                intent.putExtra(PRICE, mData.get(position).getPrice());
                intent.putExtra(WEIGHT, mData.get(position).getWeight());
                intent.putExtra(DESCRIPTION, mData.get(position).getDescription());
                intent.putExtra(IMAGE, mData.get(position).getImageUrl());
                intent.putExtra(Count, Integer.parseInt(komoriProductHolder.getNumberTextView().getText().toString()));
                mContext.startActivity(intent);
            }
        });
        komoriProductHolder.getMainLinearLayout().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mData.get(position).getShowSubDetail()) {
                    mData.get(position).setShowSubDetail(true);
                    notifyDataSetChanged();
                }else {
                    mData.get(position).setShowSubDetail(false);
                    notifyDataSetChanged();
                }
                notifyItemChanged(position);

            }
        });
        komoriProductHolder.getPlusTextView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer number = Integer.parseInt(komoriProductHolder.getNumberTextView().getText().toString());
                number++;
                komoriProductHolder.getNumberTextView().setText(String.valueOf(number));
                Cart cart = new Cart();
                komoriProductID = mData.get(position).getKomoriProductID();
                cart.setFK_ProductID(komoriProductID);
                cart.setCount(number);
                cart.InsertCart(mContext, cart);
                productPrice = String.valueOf(mData.get(position).getPrice());
                komoriProductHolder.getSumPriceTextView().setText(String.valueOf(Core.Converter.DoubleToString(number * Integer.parseInt(productPrice))));
                if (Integer.parseInt(komoriProductHolder.getNumberTextView().getText().toString()) == 0) {
                    komoriProductHolder.getChooseIcon().setVisibility(View.GONE);
                } else {
                    komoriProductHolder.getChooseIcon().setVisibility(View.VISIBLE);
                }
            }
        });
        komoriProductHolder.getMinusTextView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int number = Integer.parseInt(komoriProductHolder.getNumberTextView().getText().toString());
                if (number > 0) {
                    number--;
                    Cart cart = new Cart();
                    komoriProductHolder.getNumberTextView().setText(String.valueOf(number));
                    komoriProductID = mData.get(position).getKomoriProductID();
                    cart.setFK_ProductID(komoriProductID);
                    cart.setCount(number);
                    cart.InsertCart(mContext, cart);
                    productPrice = String.valueOf(mData.get(position).getPrice());
                    komoriProductHolder.getSumPriceTextView().setText(String.valueOf(Core.Converter.DoubleToString(number * mData.get(position).getPrice())));
                    if (number == 0) {
                        cart.Delete(mContext, komoriProductID);
                    }
                    if (Integer.parseInt(komoriProductHolder.getNumberTextView().getText().toString()) == 0) {
                        komoriProductHolder.getChooseIcon().setVisibility(View.GONE);
                    } else {
                        komoriProductHolder.getChooseIcon().setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    private void LoadImage(final int position, final KomoriProductHolder holder) {
        ImageLoader imageLoader = MySingleton.getInstance(mContext).getImageLoader();
        String IMAGE_URL = mData.get(position).getImageUrl();
        holder.getKomoriProductPicutreImageView().setDefaultImageResId(R.drawable.noimage);

        if (IMAGE_URL != null) {
            holder.getKomoriProductPicutreImageView().setImageUrl(IMAGE_URL, imageLoader);
        } else {
            IMAGE_URL = "test";
            holder.getKomoriProductPicutreImageView().setImageUrl(IMAGE_URL, imageLoader);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}




