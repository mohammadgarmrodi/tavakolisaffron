package ir.dayasoft.tavakolisaffron.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.holder.DialogHolder;
import ir.dayasoft.tavakolisaffron.holder.KomoriProductHolder;
import ir.dayasoft.tavakolisaffron.holder.MainViewHolder;
import ir.dayasoft.tavakolisaffron.model.Cart;
import ir.dayasoft.tavakolisaffron.model.KomoriProduct;

/**
 * Created by mohammad on 02/20/2017.
 */

public class CheckStockDialogAdapter extends RecyclerView.Adapter<MainViewHolder> {
    private List<String> mData;
    private Context mContext;
    private Dialog mDialog;


    public CheckStockDialogAdapter(Context context, List<String> data, Dialog dialog) {
        this.mContext = context;
        this.mData = data;
        this.mDialog = dialog;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater fInflater = LayoutInflater.from(parent.getContext());
        ViewGroup v = (ViewGroup) fInflater.inflate(R.layout.item_dialog, parent, false);
        DialogHolder dialogHolder = new DialogHolder(v);
        return dialogHolder;

    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, final int position) {
        final DialogHolder dialogHolder = (DialogHolder) holder;
        dialogHolder.getNameTextView().setText(mData.get(position));

    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 1) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}

