package ir.dayasoft.tavakolisaffron.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.core.Core;
import ir.dayasoft.tavakolisaffron.fragment.BuyBasketFragment;
import ir.dayasoft.tavakolisaffron.holder.BuyBasketHolder;
import ir.dayasoft.tavakolisaffron.holder.MainViewHolder;
import ir.dayasoft.tavakolisaffron.model.Cart;

/**
 * Created by mohammad on 01/26/2017.
 */

public class BuyBasketAdapter extends RecyclerView.Adapter<MainViewHolder> {
    private List<Cart> mData;
    BuyBasketFragment buyBasketFragment;
    private Context mContext;

    public BuyBasketAdapter(Context context, List<Cart> data, BuyBasketFragment fragment) {
        this.mData = data;
        this.mContext = context;
        this.buyBasketFragment = fragment;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater fInflater = LayoutInflater.from(parent.getContext());
        ViewGroup v = (ViewGroup) fInflater.inflate(R.layout.list_item_buybasket, parent, false);
        BuyBasketHolder buyBasketHolder = new BuyBasketHolder(v);
        return buyBasketHolder;
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, final int position) {
        final BuyBasketHolder buyBasketHolder = (BuyBasketHolder) holder;
        if ((position - 1) % 2 == 0) {
            buyBasketHolder.getBuyBasketRelativeLayout().setBackgroundColor(mContext.getResources().getColor(R.color.BackgroundItem));
            buyBasketHolder.getSecondLinearLayout().setBackgroundColor(mContext.getResources().getColor(R.color.BackgroundItem));

        } else {
            buyBasketHolder.getBuyBasketRelativeLayout().setBackgroundColor(mContext.getResources().getColor(R.color.white));
            buyBasketHolder.getSecondLinearLayout().setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }

        int number = mData.get(position).getCount();
        int price = mData.get(position).getKomoriProduct().getPrice();
        buyBasketHolder.getCompanyTextView().setText(mData.get(position).getKomoriProduct().getName());
        int categoryID = mData.get(position).getCategory().getCategoryID();
        buyBasketHolder.getCategoryTextView().setText(mData.get(position).getCategory().getName());

        if (mData.get(position).getKomoriProduct().getWeight() == null) {
            buyBasketHolder.getWeightLinearLayout().setVisibility(View.INVISIBLE);

        } else {
            buyBasketHolder.getWeightTextView().setText(String.valueOf(mData.get(position).getKomoriProduct().getWeight()));

        }
        buyBasketHolder.getPriceTextView().setText(String.valueOf(Core.Converter.DoubleToString(mData.get(position).getKomoriProduct().getPrice())));
        buyBasketHolder.getNumberTextView().setText(String.valueOf(mData.get(position).getCount()));
        buyBasketHolder.getSumPriceTextView().setText((String.valueOf(Core.Converter.DoubleToString(number * price))));

        buyBasketHolder.getPlusTextView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer number = Integer.parseInt(buyBasketHolder.getNumberTextView().getText().toString());
                number++;
                buyBasketHolder.getNumberTextView().setText(String.valueOf(number));
                Integer id = mData.get(position).getKomoriProduct().getKomoriProductID();
                Cart cart = new Cart();
                cart.setFK_ProductID(id);
                cart.setCount(number);
                cart.InsertCart(mContext, cart);
                int price = Integer.parseInt(String.valueOf(mData.get(position).getKomoriProduct().getPrice()));
                buyBasketHolder.getSumPriceTextView().setText(String.valueOf(Core.Converter.DoubleToString(price * number)));
                if (Integer.parseInt(buyBasketHolder.getNumberTextView().getText().toString()) > 12) {
                    buyBasketHolder.getWarningRelativeLayout().setVisibility(View.VISIBLE);
                } else {
                    buyBasketHolder.getWarningRelativeLayout().setVisibility(View.GONE);

                }
            }
        });
        buyBasketHolder.getMinusTextView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int number = Integer.parseInt(buyBasketHolder.getNumberTextView().getText().toString());
                if (number > 0) {
                    if (number == 1) {
                        new AlertDialog.Builder(mContext).setTitle("هشدار")
                                .setMessage("آیا میخواهید این محصول را از سبد خرید حذف کنید؟!")
                                .setPositiveButton("بلی", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Cart cart = new Cart();
                                        buyBasketHolder.getNumberTextView().setText(String.valueOf(0));
                                        notifyDataSetChanged();
                                        cart.Delete(mContext, mData.get(position).getKomoriProduct().getKomoriProductID());
                                        mData.remove(position);
                                        notifyItemRemoved(position);
                                        if (mData.size() == 0) {
                                            buyBasketFragment.refresh();
                                        }
                                    }
                                })
                                .setNegativeButton("خیر", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                }).show();
                    } else {

                        number--;
                        buyBasketHolder.getNumberTextView().setText(String.valueOf(number));
                        Integer id = mData.get(position).getKomoriProduct().getKomoriProductID();
                        Cart cart = new Cart();
                        cart.setFK_ProductID(id);
                        cart.setCount(number);
                        cart.InsertCart(mContext, cart);
                        int price = Integer.parseInt(String.valueOf(mData.get(position).getKomoriProduct().getPrice()));
                        buyBasketHolder.getSumPriceTextView().setText(String.valueOf(Core.Converter.DoubleToString(price * number)));
                        if (Integer.parseInt(buyBasketHolder.getNumberTextView().getText().toString()) > 12) {
                            buyBasketHolder.getWarningRelativeLayout().setVisibility(View.VISIBLE);

                        } else {
                            buyBasketHolder.getWarningRelativeLayout().setVisibility(View.GONE);

                        }
                    }
                }
            }
        });

        if (mData.get(position).getCount() > 12) {
            buyBasketHolder.getWarningRelativeLayout().setVisibility(View.VISIBLE);
        } else {
            buyBasketHolder.getWarningRelativeLayout().setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 1) {
            return 0;
        } else {
            return 1;
        }
    }
}
