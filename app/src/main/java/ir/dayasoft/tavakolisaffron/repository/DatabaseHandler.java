package ir.dayasoft.tavakolisaffron.repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by mohammad on 12/16/2016.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    public static final String DatabaseName = "Db";


    public DatabaseHandler(Context context) {
        super(context, DatabaseName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CategoryRepo.CreateTable_category);
        db.execSQL(ImageRepo.CreateTable_Image);
        db.execSQL(InvoiceItemRepo.CreateTable_InvoiceItem);
        db.execSQL(InvoiceRepo.CreateTable_invoice);
        db.execSQL(InvoiceProductRepo.CreateTable_InvoiceProduct);
        db.execSQL(ProductRepo.CreateTable_Product);
        db.execSQL(KomoriProductRepo.CreateTable_KomoriProduct);
        db.execSQL(ProductImageRepo.CreateTable_ProductImage);
        db.execSQL(UserRepo.CreateTable_User);
        db.execSQL(CartRepo.CreateTable_cart);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
