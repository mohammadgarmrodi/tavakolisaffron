package ir.dayasoft.tavakolisaffron.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.model.Product;
import ir.dayasoft.tavakolisaffron.model.ProductImage;

/**
 * Created by mohammad on 01/22/2017.
 */

public class ProductImageRepo {

    public static final String Table_ProductImage = "ProductImage";

    public static final String ProductImageID = "ProductImageID";
    public static final String FK_KomoriProductID = "FK_KomoriProductID";
    public static final String FK_ImageID = "FK_ImageID";
    public static final String Status = "status";
    public static final String Type = "Type";
    public static final String UpdateDate = "UpdateDate";
    public static final String CreateDate = "CreateDate";


    public static final String CreateTable_ProductImage = " create table "
            + Table_ProductImage + " ( "
            + ProductImageID + " integer primary key autoincrement, "
            + FK_KomoriProductID + " integer,"
            + FK_ImageID + " integer,"
            + Status + " integer, "
            + Type + " integer,"
            + UpdateDate + " text,"
            + CreateDate + " text"
            + " ) ;";

    SQLiteDatabase database;
    DatabaseHandler dbHelper;

    public ProductImageRepo(Context context) {

        dbHelper = new DatabaseHandler(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
        database.close();
    }

    public void Delete(int productImageID) {
        database.delete(Table_ProductImage, ProductImageID + " = " + productImageID, null);
    }

    public ProductImage Get(int productImageID) {
        Cursor cursor = database.rawQuery(
                " select * " + " from " + Table_ProductImage + " where " + ProductImageID + " = " + String.valueOf(productImageID), null);
        cursor.moveToFirst();
        ProductImage productimage = new ProductImage();
        if (cursor.getCount() > 0)
            productimage = CursorToProductImage(cursor);
        else
            productimage.setProductImageID((int) -1);
        cursor.close();
        return productimage;
    }

    public List<ProductImage> Get() {
        List<ProductImage> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select * " + " from " + Table_ProductImage, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ProductImage cf = CursorToProductImage(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }

    public void clear() {
        database.delete(Table_ProductImage, null, null);
    }

    public ProductImage CursorToProductImage(Cursor cursor) {
        ProductImage productImage = new ProductImage();

        productImage.setProductImageID(cursor.getInt(cursor.getColumnIndex(ProductImageID)));
        productImage.setFK_KomoriProductID(cursor.getInt(cursor.getColumnIndex(FK_KomoriProductID)));
        productImage.setFK_ImageID(cursor.getInt(cursor.getColumnIndex(FK_ImageID)));
        productImage.setType(cursor.getInt(cursor.getColumnIndex(Type)));
        productImage.setStatus(cursor.getInt(cursor.getColumnIndex(Status)));
        productImage.setUpdateDate(cursor.getString(cursor.getColumnIndex(UpdateDate)));
        productImage.setCreateDate(cursor.getString(cursor.getColumnIndex(CreateDate)));

        return productImage;
    }

    public Boolean IsExist(int productID) {
        Boolean returnValue = false;

        Cursor cursor = database.rawQuery(" select " + ProductImageID + " from " +
                Table_ProductImage + " where " + ProductImageID + " = " + String.valueOf(productID), null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            returnValue = true;
        }

        cursor.close();

        return returnValue;
    }

    public int Update(ProductImage productimage) {
        ContentValues values = new ContentValues();

        if (productimage.getProductImageID() != -1)
            values.put(ProductImageID, productimage.getProductImageID());
        if (productimage.getFK_ImageID() != -1)
            values.put(FK_ImageID, productimage.getFK_ImageID());
        if (productimage.getFK_KomoriProductID() != -1)
            values.put(FK_KomoriProductID, productimage.getFK_KomoriProductID());

        if (productimage.getStatus() != -1)
            values.put(Status, productimage.getStatus());
        if (productimage.getType() != -1)
            values.put(Type, productimage.getType());

        if (productimage.getCreateDate() != null)
            values.put(CreateDate, productimage.getCreateDate());
        if (productimage.getUpdateDate() != null)
            values.put(UpdateDate, productimage.getUpdateDate());


        return database.update(Table_ProductImage, values, ProductImageID + "=?", new String[]{String.valueOf(productimage.getProductImageID())});
    }


    public ProductImage Insert(ProductImage productImage) {
        ContentValues values = new ContentValues();

        values.put(ProductImageID, productImage.getProductImageID());
        values.put(FK_KomoriProductID, productImage.getFK_KomoriProductID());
        values.put(FK_ImageID, productImage.getFK_ImageID());

        values.put(CreateDate, productImage.getCreateDate());
        values.put(UpdateDate, productImage.getUpdateDate());
        values.put(Type, productImage.getType());
        values.put(Status, productImage.getStatus());


        if (productImage.getProductImageID() == -1) {
            int insertID = (int) database.insert(Table_ProductImage, null, values);
            productImage.setProductImageID(insertID);
        } else {
            Boolean flag = IsExist(productImage.getProductImageID());
            if (flag) {
                Update(productImage);
            } else {
                int insertID = (int) database.insert(Table_ProductImage, null, values);
                productImage.setProductImageID(insertID);
            }
        }
        return productImage;


    }

    public Integer insert(List<ProductImage> productImageList) {

        int return_value = 0;
        ContentValues values = new ContentValues();
        for (ProductImage productImage : productImageList) {

            values.put(ProductImageID, productImage.getProductImageID());
            values.put(FK_KomoriProductID, productImage.getFK_KomoriProductID());
            values.put(FK_ImageID, productImage.getFK_ImageID());

            values.put(CreateDate, productImage.getCreateDate());
            values.put(UpdateDate, productImage.getUpdateDate());
            values.put(Type, productImage.getType());
            values.put(Status, productImage.getStatus());


            if (productImage.getProductImageID() == -1) {
                int insertID = (int) database.insert(Table_ProductImage, null, values);
                if (insertID != -1) return_value++;
            } else {
                Boolean flag = IsExist(productImage.getProductImageID());
                if (flag) {
                    int i = Update(productImage);
                    if (i == 1) {
                        return_value++;
                    }
                } else {
                    int insertId = (int) database.insert(Table_ProductImage, null, values);
                    if (insertId != -1)
                        return_value++;
                }
            }
            values.clear();
        }
        return return_value;
    }

//    public Category GetLast() {
//        Cursor cursor = database.rawQuery(" Select * from " +
//                Table_Product +
//                " Order by " +
//                UpdateDate +
//                " desc " + " limit 1", null);
//        cursor.moveToFirst();
//        Category category = new Category();
//        if (cursor.getCountTextView() > 0)
//            category = CursorToCategory(cursor);
//        else {
//            category.setCategoryID(-1);
//            category.setUpdateDate(Constants.DateOfOrigin);
//        }
//        cursor.close();
//        return category;
//    }


}
