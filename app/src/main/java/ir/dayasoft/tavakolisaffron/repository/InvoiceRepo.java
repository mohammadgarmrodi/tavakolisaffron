package ir.dayasoft.tavakolisaffron.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.model.Category;
import ir.dayasoft.tavakolisaffron.model.Invoice;

/**
 * Created by mohammad on 01/22/2017.
 */

public class InvoiceRepo {

    public static final String Table_Invoice = "invoice";
    public static final String InvoiceID = "InvoiceID";
    public static final String Fk_UserID = "FK_UserID";
    public static final String Code = "Code";
    public static final String Price = "Price";
    public static final String DiscountPrice = "DiscountPrice";
    public static final String Description = "Description";
    public static final String Status = "status";
    public static final String Type = "Type";
    public static final String IsDelete = "IsDelete";
    public static final String PaymentDeadline = "PaymentDeadline";
    public static final String PaymentDate = "PaymentDate";
    public static final String UpdateDate = "UpdateDate";
    public static final String CreateDate = "CreateDate";
    public static final String Phone = "Phone";
    public static final String Address = "Address";
    public static final String Count = "Count";
    public static final String Invoice_Item_Status = "InvoiceItemStatus";
    public static final String Invoice_Item_Payment_Status = "InvoiceItemPaymentStatus";


    public static final String CreateTable_invoice = " create table "
            + Table_Invoice + " ( "
            + InvoiceID + " integer primary key autoincrement, "
            + Fk_UserID + " integer,"
            + Code + " text, "
            + Price + " integer,"
            + DiscountPrice + " integer, "
            + Description + " text, "
            + Status + " integer, "
            + Invoice_Item_Status + " integer, "
            + Invoice_Item_Payment_Status + " integer, "
            + Type + " integer,"
            + IsDelete + " blob,"
            + PaymentDeadline + " text,"
            + PaymentDate + " text,"
            + UpdateDate + " text,"
            + Phone + " text,"
            + Address + " text,"
            + Count + " integer,"
            + CreateDate + " text"
            + " ) ;";

    SQLiteDatabase database;
    DatabaseHandler dbHelper;


    public InvoiceRepo(Context context) {

        dbHelper = new DatabaseHandler(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
        database.close();
    }

    public void Delete(int invoiceID) {
        database.delete(Table_Invoice, InvoiceID + " = " + invoiceID, null);
    }

    public Invoice Get(int invoiceID) {
        Cursor cursor = database.rawQuery(
                " select * " + " from " + Table_Invoice + " where " + InvoiceID + " = " + String.valueOf(invoiceID), null);
        cursor.moveToFirst();
        Invoice invoice = new Invoice();
        if (cursor.getCount() > 0)
            invoice = CursorToInvoice(cursor);
        else
            invoice.setInvoiceID((int) -1);
        cursor.close();
        return invoice;
    }

    public List<Invoice> Get() {
        List<Invoice> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select * " + " from " + Table_Invoice + " ORDER BY " + Table_Invoice + " . "  + CreateDate + " desc ", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Invoice cf = CursorToInvoice(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }

    public void clear() {
        database.delete(Table_Invoice, null, null);
    }

    public Invoice CursorToInvoice(Cursor cursor) {
        Invoice invoice = new Invoice();

        invoice.setInvoiceID(cursor.getInt(cursor.getColumnIndex(InvoiceID)));
        invoice.setFk_UserID(cursor.getLong(cursor.getColumnIndex(Fk_UserID)));
        invoice.setCode(cursor.getString(cursor.getColumnIndex(Code)));
        invoice.setCount(cursor.getInt(cursor.getColumnIndex(Count)));
        invoice.setPrice(cursor.getInt(cursor.getColumnIndex(Price)));
        invoice.setDiscountPrice(cursor.getInt(cursor.getColumnIndex(DiscountPrice)));
        invoice.setDescription(cursor.getString(cursor.getColumnIndex(Description)));
        invoice.setType(cursor.getInt(cursor.getColumnIndex(Type)));
        invoice.setStatus(cursor.getInt(cursor.getColumnIndex(Status)));
        invoice.setInvoiceItemStatus(cursor.getInt(cursor.getColumnIndex(Invoice_Item_Status)));
        invoice.setInvoiceItemPaymentStatus(cursor.getInt(cursor.getColumnIndex(Invoice_Item_Payment_Status)));
        invoice.setDelete(cursor.getInt(cursor.getColumnIndex(IsDelete)) > 0);
        invoice.setPaymentDeadline(cursor.getString(cursor.getColumnIndex(PaymentDeadline)));
        invoice.setPaymentDate(cursor.getString(cursor.getColumnIndex(PaymentDate)));
        invoice.setUpdateDate(cursor.getString(cursor.getColumnIndex(UpdateDate)));
        invoice.setCreateDate(cursor.getString(cursor.getColumnIndex(CreateDate)));
        return invoice;
    }

    public Boolean IsExist(int invoiceID) {
        Boolean returnValue = false;

        Cursor cursor = database.rawQuery(" select " + InvoiceID + " from " +
                Table_Invoice + " where " + InvoiceID + " = " + String.valueOf(invoiceID), null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            returnValue = true;
        }

        cursor.close();

        return returnValue;
    }

    public Invoice IsExistByInvoiceID(int invoiceID) {
        Invoice invoice = new Invoice();

        Cursor cursor = database.rawQuery(
                " select * from " +
                        Table_Invoice +
                        " where " + InvoiceID + " = " + String.valueOf(invoiceID), null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            invoice = CursorToInvoice(cursor);
        }
        cursor.close();

        return invoice;
    }

    public int Update(Invoice invoice) {
        ContentValues values = new ContentValues();

        if (invoice.getInvoiceID() != -1)
            values.put(InvoiceID, invoice.getInvoiceID());
        if (invoice.getFk_UserID() != null)
            values.put(Fk_UserID, invoice.getFk_UserID());
        if (invoice.getCode() != null)
            values.put(Code, invoice.getCode());
        if (invoice.getPrice() != -1)
            values.put(Price, invoice.getPrice());
        if (invoice.getDiscountPrice() != -1)
            values.put(DiscountPrice, invoice.getDiscountPrice());
        if (invoice.getDescription() != null)
            values.put(Description, invoice.getDescription());
        if (invoice.getStatus() != -1)
            values.put(Status, invoice.getStatus());
        if (invoice.getType() != -1)
            values.put(Type, invoice.getType());
        if (invoice.getDelete() != null)
            values.put(IsDelete, invoice.getDelete());
        if (invoice.getPaymentDate() != null)
            values.put(PaymentDate, invoice.getPaymentDate());
        if (invoice.getPaymentDeadline() != null)
            values.put(PaymentDate, invoice.getPaymentDeadline());
        if (invoice.getCreateDate() != null)
            values.put(CreateDate, invoice.getCreateDate());
        if (invoice.getUpdateDate() != null)
            values.put(UpdateDate, invoice.getUpdateDate());
        if (invoice.getPhone() != null)
            values.put(Phone, invoice.getPhone());
        if (invoice.getAddress() != null)
            values.put(Address, invoice.getAddress());
        if (invoice.getCount() != -1)
            values.put(Count, invoice.getCount());
        if (invoice.getInvoiceItemStatus() != null)
            values.put(Invoice_Item_Status, invoice.getInvoiceItemStatus());
        if (invoice.getInvoiceItemPaymentStatus() != null)
            values.put(Invoice_Item_Payment_Status, invoice.getInvoiceItemPaymentStatus());


        return database.update(Table_Invoice, values, InvoiceID + "=?", new String[]{String.valueOf(invoice.getInvoiceID())});
    }

    public Invoice Insert(Invoice invoice) {
        ContentValues values = new ContentValues();

        values.put(InvoiceID, invoice.getInvoiceID());
        values.put(Fk_UserID, invoice.getFk_UserID());
        values.put(Code, invoice.getCode());
        values.put(Price, invoice.getPrice());
        values.put(DiscountPrice, invoice.getDiscountPrice());
        values.put(Description, invoice.getDescription());
        values.put(Status, invoice.getStatus());
        values.put(Type, invoice.getType());
        values.put(PaymentDeadline, invoice.getPaymentDeadline());
        values.put(PaymentDate, invoice.getPaymentDate());
        values.put(CreateDate, invoice.getCreateDate());
        values.put(UpdateDate, invoice.getUpdateDate());
        values.put(IsDelete, invoice.getDelete());
        values.put(Phone, invoice.getPhone());
        values.put(Address, invoice.getAddress());
        values.put(Count, invoice.getCount());
        values.put(Invoice_Item_Status, invoice.getInvoiceItemStatus());
        values.put(Invoice_Item_Payment_Status, invoice.getInvoiceItemPaymentStatus());



        if (invoice.getInvoiceID() == -1) {
            int insertID = (int) database.insert(Table_Invoice, null, values);
            invoice.setInvoiceID(insertID);
        } else {
            Boolean flag = IsExist(invoice.getInvoiceID());
            if (flag) {
                Update(invoice);
            } else {
                int insertID = (int) database.insert(Table_Invoice, null, values);
                invoice.setInvoiceID(insertID);
            }
        }
        return invoice;


    }

    public Integer insert(List<Invoice> invoiceList) {

        int return_value = 0;
        ContentValues values = new ContentValues();
        for (Invoice invoice : invoiceList) {

            values.put(InvoiceID, invoice.getInvoiceID());
            values.put(Fk_UserID, invoice.getFk_UserID());
            values.put(Code, invoice.getCode());
            values.put(Price, invoice.getPrice());
            values.put(DiscountPrice, invoice.getDiscountPrice());
            values.put(Description, invoice.getDescription());
            values.put(Status, invoice.getStatus());
            values.put(Type, invoice.getType());
            values.put(PaymentDeadline, invoice.getPaymentDeadline());
            values.put(PaymentDate, invoice.getPaymentDate());
            values.put(CreateDate, invoice.getCreateDate());
            values.put(UpdateDate, invoice.getUpdateDate());
            values.put(IsDelete, invoice.getDelete());
            values.put(Phone, invoice.getPhone());
            values.put(Address, invoice.getAddress());
            values.put(Count, invoice.getCount());
            values.put(Invoice_Item_Status, invoice.getInvoiceItem().getStatus());
            values.put(Invoice_Item_Payment_Status, invoice.getInvoiceItem().getPaymentStatus());



            if (invoice.getInvoiceID() == -1) {
                int insertID = (int) database.insert(Table_Invoice, null, values);
                if (insertID != -1) return_value++;
            } else {
                Boolean flag = IsExist(invoice.getInvoiceID());
                if (flag) {
                    int i = Update(invoice);
                    if (i == 1) {
                        return_value++;
                    }
                } else {
                    int insertId = (int) database.insert(Table_Invoice, null, values);
                    if (insertId != -1)
                        return_value++;
                }
            }
            values.clear();
        }
        return return_value;
    }

    public Invoice GetLast() {
        Cursor cursor = database.rawQuery(" Select * from " +
                Table_Invoice +
                " Order by " +
                UpdateDate +
                " desc " + " limit 1", null);
        cursor.moveToFirst();
        Invoice invoice = new Invoice();
        if (cursor.getCount() > 0)
            invoice = CursorToInvoice(cursor);
        else {
            invoice.setInvoiceID(-1);
            invoice.setUpdateDate(Constants.DateOfOrigin);
        }
        cursor.close();
        return invoice;
    }
}
