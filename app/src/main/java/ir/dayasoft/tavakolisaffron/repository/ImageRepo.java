package ir.dayasoft.tavakolisaffron.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.model.Image;
import ir.dayasoft.tavakolisaffron.model.InvoiceItem;

/**
 * Created by mohammad on 01/22/2017.
 */

public class ImageRepo {

    public static final String Table_Image = "Image";

    public static final String ImageID = "ImageID";
    public static final String Url = "Url";
    public static final String ThumbUrl = "ThumbUrl";
    public static final String Description = "Description";
    public static final String Alt = "Alt";
    public static final String Orders = "Orders";
    public static final String Status = "status";
    public static final String Type = "Type";
    public static final String IsDelete = "IsDelete";
    public static final String ImageType = "ImageType";
    public static final String UpdateDate = "UpdateDate";
    public static final String CreateDate = "CreateDate";


    public static final String CreateTable_Image = " create table "
            + Table_Image + " ( "
            + ImageID + " integer primary key autoincrement, "
            + Url + " text, "
            + ThumbUrl + " text, "
            + Description + " text, "
            + Alt + " text, "
            + Orders + " text, "
            + Status + " integer, "
            + Type + " integer,"
            + IsDelete + " blob,"
            + ImageType + " text, "
            + UpdateDate + " text,"
            + CreateDate + " text"
            + " ) ;";

    SQLiteDatabase database;
    DatabaseHandler dbHelper;

    public ImageRepo(Context context) {

        dbHelper = new DatabaseHandler(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
        database.close();
    }

    public void Delete(int imageID) {
        database.delete(Table_Image, ImageID + " = " + imageID, null);
    }

    public Image Get(int imageID) {
        Cursor cursor = database.rawQuery(
                " select * " + " from " + Table_Image + " where " + ImageID + " = " + String.valueOf(imageID), null);
        cursor.moveToFirst();
        Image image = new Image();
        if (cursor.getCount() > 0)
            image = CursorToImage(cursor);
        else
            image.setImageID((int) -1);
        cursor.close();
        return image;
    }

    public List<Image> Get() {
        List<Image> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select * " + " from " + Table_Image, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Image cf = CursorToImage(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }

    public void clear() {
        database.delete(Table_Image, null, null);
    }

    public Image CursorToImage(Cursor cursor) {
        Image image = new Image();

        image.setImageID(cursor.getInt(cursor.getColumnIndex(ImageID)));
        image.setUrl(cursor.getString(cursor.getColumnIndex(Url)));
        image.setThumbUrl(cursor.getString(cursor.getColumnIndex(ThumbUrl)));
        image.setDescription(cursor.getString(cursor.getColumnIndex(Description)));
        image.setAlt(cursor.getString(cursor.getColumnIndex(Alt)));
        image.setOrders(cursor.getInt(cursor.getColumnIndex(Orders)));
        image.setType(cursor.getInt(cursor.getColumnIndex(Type)));
        image.setStatus(cursor.getInt(cursor.getColumnIndex(Status)));
        image.setDelete(cursor.getInt(cursor.getColumnIndex(IsDelete)) > 0);
        image.setImageType(cursor.getString(cursor.getColumnIndex(ImageType)));
        image.setUpdateDate(cursor.getString(cursor.getColumnIndex(UpdateDate)));
        image.setCreateDate(cursor.getString(cursor.getColumnIndex(CreateDate)));
        return image;
    }

    public Boolean IsExist(int imageID) {
        Boolean returnValue = false;

        Cursor cursor = database.rawQuery(" select " + ImageID + " from " +
                Table_Image + " where " + ImageID + " = " + String.valueOf(imageID), null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            returnValue = true;
        }

        cursor.close();

        return returnValue;
    }

    public int Update(Image image) {
        ContentValues values = new ContentValues();

        if (image.getImageID() != -1)
            values.put(ImageID, image.getImageID());
        if (image.getUrl() != null)
            values.put(Url, image.getUrl());
        if (image.getStatus() != -1)
            values.put(Status, image.getStatus());
        if (image.getType() != -1)
            values.put(Type, image.getType());
        if (image.getThumbUrl() != null)
            values.put(ThumbUrl, image.getThumbUrl());
        if (image.getAlt() != null)
            values.put(Alt, image.getAlt());
        if (image.getOrders() != -1)
            values.put(Orders, image.getOrders());
        if (image.getDescription() != null)
            values.put(Description, image.getDescription());
        if (image.getDelete() != null)
            values.put(IsDelete, image.getDelete());
        if (image.getImageType() != null)
            values.put(ImageType, image.getImageType());
        if (image.getCreateDate() != null)
            values.put(CreateDate, image.getCreateDate());
        if (image.getUpdateDate() != null)
            values.put(UpdateDate, image.getUpdateDate());


        return database.update(Table_Image, values, ImageID + "=?", new String[]{String.valueOf(image.getImageID())});
    }


    public Image Insert(Image image) {
        ContentValues values = new ContentValues();

        values.put(ImageID, image.getImageID());
        values.put(Url, image.getUrl());
        values.put(ThumbUrl, image.getImageID());
        values.put(Description, image.getDescription());
        values.put(Alt, image.getAlt());
        values.put(Orders, image.getOrders());
        values.put(Status, image.getStatus());
        values.put(Type, image.getType());
        values.put(ImageType, image.getType());
        values.put(CreateDate, image.getCreateDate());
        values.put(UpdateDate, image.getUpdateDate());
        values.put(IsDelete, image.getDelete());


        if (image.getImageID() == -1) {
            int insertID = (int) database.insert(Table_Image, null, values);
            image.setImageID(insertID);
        } else {
            Boolean flag = IsExist(image.getImageID());
            if (flag) {
                Update(image);
            } else {
                int insertID = (int) database.insert(Table_Image, null, values);
                image.setImageID(insertID);
            }
        }
        return image;


    }

    public Integer insert(List<Image> imageList) {

        int return_value = 0;
        ContentValues values = new ContentValues();
        for (Image image : imageList) {

            values.put(ImageID, image.getImageID());
            values.put(Url, image.getUrl());
            values.put(ThumbUrl, image.getThumbUrl());
            values.put(Description, image.getDescription());
            values.put(Alt, image.getAlt());
            values.put(Orders, image.getOrders());
            values.put(Status, image.getStatus());
            values.put(Type, image.getType());
            values.put(ImageType, image.getType());
            values.put(CreateDate, image.getCreateDate());
            values.put(UpdateDate, image.getUpdateDate());
            values.put(IsDelete, image.getDelete());


            if (image.getImageID() == -1) {
                int insertID = (int) database.insert(Table_Image, null, values);
                if (insertID != -1) return_value++;
            } else {
                Boolean flag = IsExist(image.getImageID());
                if (flag) {
                    int i = Update(image);
                    if (i == 1) {
                        return_value++;
                    }
                } else {
                    int insertId = (int) database.insert(Table_Image, null, values);
                    if (insertId != -1)
                        return_value++;
                }
            }
            values.clear();
        }
        return return_value;
    }

    public Image GetLast() {
        Cursor cursor = database.rawQuery(" Select * from " +
                Table_Image +
                " Order by " +
                UpdateDate +
                " desc " + " limit 1", null);
        cursor.moveToFirst();
        Image image = new Image();
        if (cursor.getCount() > 0)
            image = CursorToImage(cursor);
        else {
            image.setImageID(-1);
            image.setUpdateDate(Constants.DateOfOrigin);
        }
        cursor.close();
        return image;
    }

}
