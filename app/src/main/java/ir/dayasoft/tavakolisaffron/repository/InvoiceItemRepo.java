package ir.dayasoft.tavakolisaffron.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.model.InvoiceItem;
import ir.dayasoft.tavakolisaffron.model.InvoiceProduct;

/**
 * Created by mohammad on 01/22/2017.
 */

public class InvoiceItemRepo {

    public static final String Table_InvoiceItem = "InvoiceItem";

    public static final String InvoiceItemID = "InvoiceItemID";
    public static final String FK_InvoiceID = "FK_InvoiceID";
    public static final String Code = "Code";
    public static final String Price = "Price";
    public static final String BankName = "BankName";
    public static final String PaymentStatus = "PaymentStatus";
    public static final String Description = "Description";
    public static final String Status = "status";
    public static final String Type = "Type";
    public static final String IsDelete = "IsDelete";
    public static final String PaymentDate = "PaymentDate";
    public static final String UpdateDate = "UpdateDate";
    public static final String CreateDate = "CreateDate";


    public static final String CreateTable_InvoiceItem = " create table "
            + Table_InvoiceItem + " ( "
            + InvoiceItemID + " integer primary key autoincrement, "
            + FK_InvoiceID + " integer,"
            + Price + " integer,"
            + Code + " text, "
            + BankName + " text, "
            + PaymentStatus + " integer, "
            + Description + " text, "
            + Status + " integer, "
            + Type + " integer,"
            + IsDelete + " blob,"
            + PaymentDate + " text,"
            + UpdateDate + " text,"
            + CreateDate + " text"
            + " ) ;";

    SQLiteDatabase database;
    DatabaseHandler dbHelper;


    public InvoiceItemRepo(Context context) {

        dbHelper = new DatabaseHandler(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
        database.close();
    }

    public void Delete(int invoiceItemID) {
        database.delete(Table_InvoiceItem, InvoiceItemID + " = " + invoiceItemID, null);
    }

    public InvoiceItem Get(int invoiceItemID) {
        Cursor cursor = database.rawQuery(
                " select * " + " from " + Table_InvoiceItem + " where " + InvoiceItemID + " = " + String.valueOf(invoiceItemID), null);
        cursor.moveToFirst();
        InvoiceItem invoiceItem = new InvoiceItem();
        if (cursor.getCount() > 0)
            invoiceItem = CursorToInvoiceItem(cursor);
        else
            invoiceItem.setInvoiceItemID((int) -1);
        cursor.close();
        return invoiceItem;
    }

    public List<InvoiceItem> Get() {
        List<InvoiceItem> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select * " + " from " + Table_InvoiceItem, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            InvoiceItem cf = CursorToInvoiceItem(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }

    public void clear() {
        database.delete(Table_InvoiceItem, null, null);
    }

    public InvoiceItem CursorToInvoiceItem(Cursor cursor) {
        InvoiceItem invoiceItem = new InvoiceItem();

        invoiceItem.setInvoiceItemID(cursor.getInt(cursor.getColumnIndex(InvoiceItemID)));
        invoiceItem.setFK_InvoiceID(cursor.getInt(cursor.getColumnIndex(FK_InvoiceID)));
        invoiceItem.setPrice(cursor.getInt(cursor.getColumnIndex(Price)));
        invoiceItem.setCode(cursor.getString(cursor.getColumnIndex(Code)));
        invoiceItem.setBankName(cursor.getString(cursor.getColumnIndex(BankName)));
        invoiceItem.setPaymentStatus(cursor.getInt(cursor.getColumnIndex(PaymentStatus)));
        invoiceItem.setDescription(cursor.getString(cursor.getColumnIndex(Description)));
        invoiceItem.setType(cursor.getInt(cursor.getColumnIndex(Type)));
        invoiceItem.setStatus(cursor.getInt(cursor.getColumnIndex(Status)));
        invoiceItem.setDelete(cursor.getInt(cursor.getColumnIndex(IsDelete)) > 0);
        invoiceItem.setPaymentDate(cursor.getString(cursor.getColumnIndex(PaymentDate)));
        invoiceItem.setUpdateDate(cursor.getString(cursor.getColumnIndex(UpdateDate)));
        invoiceItem.setCreateDate(cursor.getString(cursor.getColumnIndex(CreateDate)));
        return invoiceItem;
    }

    public Boolean IsExist(int invoiceItemID) {
        Boolean returnValue = false;

        Cursor cursor = database.rawQuery(" select " + InvoiceItemID + " from " +
                Table_InvoiceItem + " where " + InvoiceItemID + " = " + String.valueOf(invoiceItemID), null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            returnValue = true;
        }

        cursor.close();

        return returnValue;
    }

    public int Update(InvoiceItem invoiceItem) {
        ContentValues values = new ContentValues();

        if (invoiceItem.getInvoiceItemID() != -1)
            values.put(InvoiceItemID, invoiceItem.getInvoiceItemID());
        if (invoiceItem.getFK_InvoiceID() != -1)
            values.put(FK_InvoiceID, invoiceItem.getFK_InvoiceID());
        if (invoiceItem.getCode() != null)
            values.put(Code, invoiceItem.getCode());
        if (invoiceItem.getPrice() != -1)
            values.put(Price, invoiceItem.getPrice());
        if (invoiceItem.getBankName() != null)
            values.put(BankName, invoiceItem.getBankName());
        if (invoiceItem.getPaymentStatus() != -1)
            values.put(PaymentStatus, invoiceItem.getPaymentStatus());
        if (invoiceItem.getDescription() != null)
            values.put(Description, invoiceItem.getDescription());
        if (invoiceItem.getStatus() != -1)
            values.put(Status, invoiceItem.getStatus());
        if (invoiceItem.getType() != -1)
            values.put(Type, invoiceItem.getType());
        if (invoiceItem.getDelete() != null)
            values.put(IsDelete, invoiceItem.getDelete());
        if (invoiceItem.getPaymentDate() != null)
            values.put(PaymentDate, invoiceItem.getPaymentDate());
        if (invoiceItem.getCreateDate() != null)
            values.put(CreateDate, invoiceItem.getCreateDate());
        if (invoiceItem.getUpdateDate() != null)
            values.put(UpdateDate, invoiceItem.getUpdateDate());


        return database.update(Table_InvoiceItem, values, InvoiceItemID + "=?", new String[]{String.valueOf(invoiceItem.getInvoiceItemID())});
    }

    public InvoiceItem Insert(InvoiceItem invoiceItem) {
        ContentValues values = new ContentValues();

        values.put(InvoiceItemID, invoiceItem.getInvoiceItemID());
        values.put(FK_InvoiceID, invoiceItem.getFK_InvoiceID());
        values.put(Code, invoiceItem.getCode());
        values.put(Price, invoiceItem.getPrice());
        values.put(BankName, invoiceItem.getBankName());
        values.put(Description, invoiceItem.getDescription());
        values.put(Status, invoiceItem.getStatus());
        values.put(Type, invoiceItem.getType());
        values.put(PaymentStatus, invoiceItem.getPaymentStatus());
        values.put(PaymentDate, invoiceItem.getPaymentDate());
        values.put(CreateDate, invoiceItem.getCreateDate());
        values.put(UpdateDate, invoiceItem.getUpdateDate());
        values.put(IsDelete, invoiceItem.getDelete());


        if (invoiceItem.getInvoiceItemID() == -1) {
            int insertID = (int) database.insert(Table_InvoiceItem, null, values);
            invoiceItem.setInvoiceItemID(insertID);
        } else {
            Boolean flag = IsExist(invoiceItem.getInvoiceItemID());
            if (flag) {
                Update(invoiceItem);
            } else {
                int insertID = (int) database.insert(Table_InvoiceItem, null, values);
                invoiceItem.setInvoiceItemID(insertID);
            }
        }
        return invoiceItem;


    }

    public Integer insert(List<InvoiceItem> invoiceItemList) {

        int return_value = 0;
        ContentValues values = new ContentValues();
        for (InvoiceItem invoiceItem : invoiceItemList) {

            values.put(InvoiceItemID, invoiceItem.getInvoiceItemID());
            values.put(FK_InvoiceID, invoiceItem.getFK_InvoiceID());
            values.put(Code, invoiceItem.getCode());
            values.put(Price, invoiceItem.getPrice());
            values.put(BankName, invoiceItem.getBankName());
            values.put(Description, invoiceItem.getDescription());
            values.put(Status, invoiceItem.getStatus());
            values.put(Type, invoiceItem.getType());
            values.put(PaymentStatus, invoiceItem.getPaymentStatus());
            values.put(PaymentDate, invoiceItem.getPaymentDate());
            values.put(CreateDate, invoiceItem.getCreateDate());
            values.put(UpdateDate, invoiceItem.getUpdateDate());
            values.put(IsDelete, invoiceItem.getDelete());


            if (invoiceItem.getInvoiceItemID() == -1) {
                int insertID = (int) database.insert(Table_InvoiceItem, null, values);
                if (insertID != -1) return_value++;
            } else {
                Boolean flag = IsExist(invoiceItem.getInvoiceItemID());
                if (flag) {
                    int i = Update(invoiceItem);
                    if (i == 1) {
                        return_value++;
                    }
                } else {
                    int insertId = (int) database.insert(Table_InvoiceItem, null, values);
                    if (insertId != -1)
                        return_value++;
                }
            }
            values.clear();
        }
        return return_value;
    }

    public InvoiceItem GetLast() {
        Cursor cursor = database.rawQuery(" Select * from " +
                Table_InvoiceItem +
                " Order by " +
                UpdateDate +
                " desc " + " limit 1", null);
        cursor.moveToFirst();
        InvoiceItem invoiceItem = new InvoiceItem();
        if (cursor.getCount() > 0)
            invoiceItem = CursorToInvoiceItem(cursor);
        else {
            invoiceItem.setInvoiceItemID(-1);
            invoiceItem.setUpdateDate(Constants.DateOfOrigin);
        }
        cursor.close();
        return invoiceItem;
    }

}
