package ir.dayasoft.tavakolisaffron.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.model.InvoiceProduct;
import ir.dayasoft.tavakolisaffron.model.PreFactor;

/**
 * Created by mohammad on 01/22/2017.
 */

public class InvoiceProductRepo {

    public static final String Table_InvoiceProduct = "InvoiceProduct";

    public static final String InvoiceProductID = "InvoiceProductID";
    public static final String FK_InvoiceID = "FK_InvoiceID";
    public static final String FK_ProductID = "FK_ProductID";
    public static final String Count = "Count";
    public static final String Description = "Description";
    public static final String Status = "status";
    public static final String Type = "Type";
    public static final String IsDelete = "IsDelete";
    public static final String UpdateDate = "UpdateDate";
    public static final String CreateDate = "CreateDate";


    public static final String CreateTable_InvoiceProduct = " create table "
            + Table_InvoiceProduct + " ( "
            + InvoiceProductID + " integer primary key autoincrement, "
            + FK_InvoiceID + " integer,"
            + FK_ProductID + " integer,"
            + Count + " integer,"
            + Description + " text, "
            + Status + " integer, "
            + Type + " integer,"
            + IsDelete + " blob,"
            + UpdateDate + " text,"
            + CreateDate + " text"
            + " ) ;";

    SQLiteDatabase database;
    DatabaseHandler dbHelper;


    public InvoiceProductRepo(Context context) {

        dbHelper = new DatabaseHandler(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
        database.close();
    }

    public void Delete(int invoiceProductID) {
        database.delete(Table_InvoiceProduct, InvoiceProductID + " = " + invoiceProductID, null);
    }

    public List<InvoiceProduct> GetListInvoice(int invoiceProductID) {
        List<InvoiceProduct> AllApp = new ArrayList<>();
        Cursor cursor = database.rawQuery(
                " select * " + " from " + Table_InvoiceProduct + " where " + FK_InvoiceID + " = " + String.valueOf(invoiceProductID), null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            InvoiceProduct cf = CursorToInvoiceProduct(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }

    public InvoiceProduct Get(int invoiceProductID) {
        Cursor cursor = database.rawQuery(
                " select * " + " from " + Table_InvoiceProduct + " where " + FK_InvoiceID + " = " + String.valueOf(invoiceProductID), null);
        cursor.moveToFirst();
        InvoiceProduct invoiceProduct = new InvoiceProduct();
        if (cursor.getCount() > 0)
            invoiceProduct = CursorToInvoiceProduct(cursor);
        else
            invoiceProduct.setInvoiceProductID((int) -1);
        cursor.close();
        return invoiceProduct;
    }

    public List<PreFactor> GetVegetableInvoiceProduct(int invoiceProductID) {
        List<PreFactor> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select " +
                KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Name + " , " +
                Table_InvoiceProduct + "." + Count + " , " +
                KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Weight + " , " +
                KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Price +
                " From " +
                KomoriProductRepo.Table_KomoriProduct +
                " INNER JOIN " +
                Table_InvoiceProduct +
                " ON " +
                KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.FK_ProductID + " = " +
                Table_InvoiceProduct + "." + FK_ProductID +
                " WHERE " + Table_InvoiceProduct + "." + FK_InvoiceID +
                " = " +
                String.valueOf(invoiceProductID) + " and " +
                KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.FK_CategoryID +
                " in ( 4,7,8 )", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PreFactor cf = CursorToPreFactor(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }

    public List<PreFactor> GetAnimalInvoiceProduct(int invoiceProductID) {
        List<PreFactor> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select " +
                KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Name + " , " +
                Table_InvoiceProduct + "." + Count + " , " +
                KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Weight + " , " +
                KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Price +
                " From " +
                KomoriProductRepo.Table_KomoriProduct +
                " INNER JOIN " +
                 Table_InvoiceProduct +
                " ON " +
                KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.FK_ProductID + " = " +
                Table_InvoiceProduct + "." + FK_ProductID +
                " WHERE " + Table_InvoiceProduct + "." + FK_InvoiceID +
                " = " +
                String.valueOf(invoiceProductID) , null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PreFactor cf = CursorToPreFactor(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }

    public void clear() {
        database.delete(Table_InvoiceProduct, null, null);
    }

    public InvoiceProduct CursorToInvoiceProduct(Cursor cursor) {
        InvoiceProduct invoiceProduct = new InvoiceProduct();

        invoiceProduct.setInvoiceProductID(cursor.getInt(cursor.getColumnIndex(InvoiceProductID)));
        invoiceProduct.setFK_InvoiceID(cursor.getInt(cursor.getColumnIndex(FK_InvoiceID)));
        invoiceProduct.setFK_ProductID(cursor.getInt(cursor.getColumnIndex(FK_ProductID)));
        invoiceProduct.setCount(cursor.getInt(cursor.getColumnIndex(Count)));
        invoiceProduct.setDescription(cursor.getString(cursor.getColumnIndex(Description)));
        invoiceProduct.setType(cursor.getInt(cursor.getColumnIndex(Type)));
        invoiceProduct.setStatus(cursor.getInt(cursor.getColumnIndex(Status)));
        invoiceProduct.setDelete(cursor.getInt(cursor.getColumnIndex(IsDelete)) > 0);
        invoiceProduct.setUpdateDate(cursor.getString(cursor.getColumnIndex(UpdateDate)));
        invoiceProduct.setCreateDate(cursor.getString(cursor.getColumnIndex(CreateDate)));
        return invoiceProduct;
    }

    public Boolean IsExist(int invoiceProductID) {
        Boolean returnValue = false;

        Cursor cursor = database.rawQuery(" select " + InvoiceProductID + " from " +
                Table_InvoiceProduct + " where " + InvoiceProductID + " = " + String.valueOf(invoiceProductID), null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            returnValue = true;
        }

        cursor.close();

        return returnValue;
    }

    public int Update(InvoiceProduct invoiceProduct) {
        ContentValues values = new ContentValues();

        if (invoiceProduct.getInvoiceProductID() != -1)
            values.put(InvoiceProductID, invoiceProduct.getInvoiceProductID());
        if (invoiceProduct.getFK_InvoiceID() != -1)
            values.put(FK_InvoiceID, invoiceProduct.getFK_InvoiceID());
        if (invoiceProduct.getFK_ProductID() != -1)
            values.put(FK_ProductID, invoiceProduct.getFK_ProductID());
        if (invoiceProduct.getCount() != -1)
            values.put(Count, invoiceProduct.getCount());
        values.put(Description, invoiceProduct.getDescription());
        if (invoiceProduct.getStatus() != -1)
            values.put(Status, invoiceProduct.getStatus());
        if (invoiceProduct.getType() != -1)
            values.put(Type, invoiceProduct.getType());
        if (invoiceProduct.getDelete() != null)
            values.put(IsDelete, invoiceProduct.getDelete());
        if (invoiceProduct.getCreateDate() != null)
            values.put(CreateDate, invoiceProduct.getCreateDate());
        if (invoiceProduct.getUpdateDate() != null)
            values.put(UpdateDate, invoiceProduct.getUpdateDate());


        return database.update(Table_InvoiceProduct, values, InvoiceProductID + "=?", new String[]{String.valueOf(invoiceProduct.getInvoiceProductID())});
    }

    public InvoiceProduct Insert(InvoiceProduct invoiceProduct) {
        ContentValues values = new ContentValues();

        values.put(InvoiceProductID, invoiceProduct.getInvoiceProductID());
        values.put(FK_InvoiceID, invoiceProduct.getFK_InvoiceID());
        values.put(FK_ProductID, invoiceProduct.getFK_ProductID());
        values.put(Count, invoiceProduct.getCount());
        values.put(Description, invoiceProduct.getDescription());
        values.put(Status, invoiceProduct.getStatus());
        values.put(Type, invoiceProduct.getType());
        values.put(CreateDate, invoiceProduct.getCreateDate());
        values.put(UpdateDate, invoiceProduct.getUpdateDate());
        values.put(IsDelete, invoiceProduct.getDelete());


        if (invoiceProduct.getInvoiceProductID() == -1) {
            int insertID = (int) database.insert(Table_InvoiceProduct, null, values);
            invoiceProduct.setInvoiceProductID(insertID);
        } else {
            Boolean flag = IsExist(invoiceProduct.getInvoiceProductID());
            if (flag) {
                Update(invoiceProduct);
            } else {
                int insertID = (int) database.insert(Table_InvoiceProduct, null, values);
                invoiceProduct.setInvoiceProductID(insertID);
            }
        }
        return invoiceProduct;


    }

    public Integer insert(List<InvoiceProduct> invoiceProductList) {

        int return_value = 0;
        ContentValues values = new ContentValues();
        for (InvoiceProduct invoiceProduct : invoiceProductList) {

            values.put(InvoiceProductID, invoiceProduct.getInvoiceProductID());
            values.put(FK_InvoiceID, invoiceProduct.getFK_InvoiceID());
            values.put(FK_ProductID, invoiceProduct.getFK_ProductID());
            values.put(Count, invoiceProduct.getCount());
            values.put(Description, invoiceProduct.getDescription());
            values.put(Status, invoiceProduct.getStatus());
            values.put(Type, invoiceProduct.getType());
            values.put(CreateDate, invoiceProduct.getCreateDate());
            values.put(UpdateDate, invoiceProduct.getUpdateDate());
            values.put(IsDelete, invoiceProduct.getDelete());

            if (invoiceProduct.getInvoiceProductID() == -1) {
                int insertID = (int) database.insert(Table_InvoiceProduct, null, values);
                if (insertID != -1) return_value++;
            } else {
                Boolean flag = IsExist(invoiceProduct.getInvoiceProductID());
                if (flag) {
                    int i = Update(invoiceProduct);
                    if (i == 1) {
                        return_value++;
                    }
                } else {
                    int insertId = (int) database.insert(Table_InvoiceProduct, null, values);
                    if (insertId != -1)
                        return_value++;
                }
            }
            values.clear();
        }
        return return_value;
    }

    public List<InvoiceProduct> Get() {
        List<InvoiceProduct> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select " + " " + " from " + Table_InvoiceProduct, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            InvoiceProduct cf = CursorToInvoiceProduct(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }


    public PreFactor GetSum(int invoiceID) {
        Cursor cursor = database.rawQuery(
                " Select sum( " + KomoriProductRepo.Price + " * " +  Table_InvoiceProduct  + "." + Count + " )  as Price"
                        + " From "
                        + Table_InvoiceProduct
                        + " INNER join "
                        + KomoriProductRepo.Table_KomoriProduct
                        + " on "
                        + Table_InvoiceProduct + " . " + FK_ProductID
                        + " = "
                        + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.FK_ProductID +
                " WHERE " + Table_InvoiceProduct + "." + FK_InvoiceID + " = " + String.valueOf(invoiceID), null);
        cursor.moveToFirst();
        PreFactor preFactor = new PreFactor();
        if (cursor.getCount() > 0) {
            preFactor = CursorToSumPreFactor(cursor);
        }
        cursor.close();
        return preFactor;


    }

    public PreFactor CursorToPreFactor(Cursor cursor) {
        PreFactor preFactor = new PreFactor();

        preFactor.setName(cursor.getString(cursor.getColumnIndex(KomoriProductRepo.Name)));
        preFactor.setCount(cursor.getInt(cursor.getColumnIndex(String.valueOf(InvoiceProductRepo.Count))));
        preFactor.setPrice(cursor.getInt(cursor.getColumnIndex(String.valueOf(KomoriProductRepo.Price))));
        preFactor.setWeight(cursor.getInt(cursor.getColumnIndex(KomoriProductRepo.Weight)));


        return preFactor;
    }


    public PreFactor CursorToSumPreFactor(Cursor cursor) {
        PreFactor preFactor = new PreFactor();

        preFactor.setPrice(cursor.getInt(cursor.getColumnIndex(String.valueOf(KomoriProductRepo.Price))));


        return preFactor;
    }

    public InvoiceProduct GetLast() {
        Cursor cursor = database.rawQuery(" Select * from " +
                Table_InvoiceProduct +
                " Order by " +
                UpdateDate +
                " desc " + " limit 1", null);
        cursor.moveToFirst();
        InvoiceProduct invoiceProduct = new InvoiceProduct();
        if (cursor.getCount() > 0)
            invoiceProduct = CursorToInvoiceProduct(cursor);
        else {
            invoiceProduct.setInvoiceProductID(-1);
            invoiceProduct.setUpdateDate(Constants.DateOfOrigin);
        }
        cursor.close();
        return invoiceProduct;
    }

}