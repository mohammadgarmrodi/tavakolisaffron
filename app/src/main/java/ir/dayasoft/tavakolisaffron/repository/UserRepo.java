package ir.dayasoft.tavakolisaffron.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.model.Product;
import ir.dayasoft.tavakolisaffron.model.User;

/**
 * Created by mohammad on 01/22/2017.
 */

public class UserRepo {

    public static final String Table_User = "User";

    public static final String UserID = "UserID";
    public static final String FK_RollID = "FK_RollID";
    public static final String FK_ProfileImageID = "FK_ProfileImageID";
    public static final String FirstName = "FirstName";
    public static final String LastName = "LastName";
    public static final String NationalCode = "NationalCode";
    public static final String IsConfirmed = "IsConfirmed";
    public static final String IsSuspended = "IsSuspended";
    public static final String UserName = "UserName";
    public static final String Email = "Email";
    public static final String Phone1 = "Phone1";
    public static final String Phone2 = "Phone2";
    public static final String CellPhone = "CellPhone";
    public static final String Password = "Password";
    public static final String FirstLogin = "FirstLogin";
    public static final String LastLogin = "LastLogin";
    public static final String LastLoginIp = "LastLoginIp";
    public static final String WebKey = "WebKey";
    public static final String AllowReceiveMessage = "AllowReceiveMessage";
    public static final String WantReceiveMessage = "WantReceiveMessage";
    public static final String AllowSendMessage = "AllowSendMessage";
    public static final String WantSendMessage = "WantSendMessage";
    public static final String IsOnline = "IsOnline";
    public static final String Address = "Address";
    public static final String Description = "Description";
    public static final String TokenExpireDate = "TokenExpireDate";
    public static final String Token = "Token";
    public static final String BirthDay = "BirthDay";
    public static final String Status = "Status";
    public static final String Type = "Type";
    public static final String IsDeleted = "IsDeleted";
    public static final String CreateDate = "CreateDate";
    public static final String UpdateDate = "UpdateDate";
    public static final String FcmToken = "FckToken";

    public static final String CreateTable_User = " create table "
            + Table_User + " ( "
            + UserID + " integer primary key autoincrement, "
            + FK_RollID + " integer, "
            + FK_ProfileImageID + " integer,"
            + FirstName + " text, "
            + LastName + " text, "
            + NationalCode + " integer, "
            + IsConfirmed + " blob,"
            + IsSuspended + " blob,"
            + UserName + " text,"
            + Email + " text,"
            + Phone1 + " text,"
            + Phone2 + " text,"
            + CellPhone + " text,"
            + Password + " text,"
            + FirstLogin + " text,"
            + LastLogin + " text,"
            + LastLoginIp + " text,"
            + WebKey + " text,"
            + AllowReceiveMessage + " blob,"
            + WantReceiveMessage + " blob,"
            + AllowSendMessage + " blob,"
            + WantSendMessage + " blob,"
            + IsOnline + " blob,"
            + Address + " text,"
            + Description + " text,"
            + TokenExpireDate + " text,"
            + Token + " text,"
            + BirthDay + " text,"
            + Status + " integer,"
            + Type + " integer,"
            + IsDeleted + " integer,"
            + CreateDate + " text,"
            + UpdateDate + " text,"
            + FcmToken + " text"
            + " ) ;";

    SQLiteDatabase database;
    DatabaseHandler dbHelper;

    public UserRepo(Context context) {

        dbHelper = new DatabaseHandler(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
        database.close();
    }

    public void Delete(Long userID) {
        database.delete(Table_User, UserID + " = " + userID, null);
    }

    public User Get(Long userID) {
        Cursor cursor = database.rawQuery(
                " select * " + " from " + Table_User + " where " + UserID + " = " + String.valueOf(userID), null);
        cursor.moveToFirst();
        User user = new User();
        if (cursor.getCount() > 0)
            user = CursorToUser(cursor);
        else
            user.setUserID((long) -1);
        cursor.close();
        return user;
    }

    public List<User> Get() {
        List<User> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select * " + " from " + Table_User, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            User cf = CursorToUser(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }


    public User GetUser() {
        Cursor cursor = database.rawQuery(
                " select * " + " from " + Table_User , null);
        cursor.moveToFirst();
        User user = new User();
        if (cursor.getCount() > 0)
            user = CursorToUser(cursor);
        else
            user.setUserID((long) -1);
        cursor.close();
        return user;
    }

    public void clear() {
        database.delete(Table_User, null, null);
    }

    public User CursorToUser(Cursor cursor) {
        User user = new User();

        user.setUserID(cursor.getLong(cursor.getColumnIndex(UserID)));
        user.setFK_RollID(cursor.getInt(cursor.getColumnIndex(FK_RollID)));
        user.setFK_ProfileImageID(cursor.getInt(cursor.getColumnIndex(FK_ProfileImageID)));
        user.setUpdateDate(cursor.getString(cursor.getColumnIndex(UpdateDate)));
        user.setDescription(cursor.getString(cursor.getColumnIndex(Description)));
        user.setUserName(cursor.getString(cursor.getColumnIndex(UserName)));
        user.setPassword(cursor.getString(cursor.getColumnIndex(Password)));
        user.setAllowReceiveMessage(cursor.getInt(cursor.getColumnIndex(AllowReceiveMessage)) > 0);
        user.setAllowSendMessage(cursor.getInt(cursor.getColumnIndex(AllowSendMessage)) > 0);
        user.setCellPhone(cursor.getString(cursor.getColumnIndex(CellPhone)));
        user.setIsDeleted(cursor.getInt(cursor.getColumnIndex(IsDeleted)));
        user.setConfirmed(cursor.getInt(cursor.getColumnIndex(IsConfirmed)) > 0);
        user.setSuspended(cursor.getInt(cursor.getColumnIndex(IsSuspended)) > 0);
        user.setLastLoginIp(cursor.getString(cursor.getColumnIndex(LastLoginIp)));
        user.setType(cursor.getInt(cursor.getColumnIndex(Type)));
        user.setStatus(cursor.getInt(cursor.getColumnIndex(Status)));
        user.setUpdateDate(cursor.getString(cursor.getColumnIndex(UpdateDate)));
        user.setCreateDate(cursor.getString(cursor.getColumnIndex(CreateDate)));
        user.setAddress(cursor.getString(cursor.getColumnIndex(Address)));
        user.setLastLogin(cursor.getString(cursor.getColumnIndex(LastLogin)));
        user.setFirstLogin(cursor.getString(cursor.getColumnIndex(FirstLogin)));
        user.setFirstName(cursor.getString(cursor.getColumnIndex(FirstName)));
        user.setLastName(cursor.getString(cursor.getColumnIndex(LastName)));
        user.setPhone2(cursor.getString(cursor.getColumnIndex(Phone2)));
        user.setPhone1(cursor.getString(cursor.getColumnIndex(Phone1)));
        user.setOnline(cursor.getInt(cursor.getColumnIndex(IsOnline)) > 0);
        user.setWantReceiveMessage(cursor.getInt(cursor.getColumnIndex(WantReceiveMessage)) > 0);
        user.setWantSendMessage(cursor.getInt(cursor.getColumnIndex(WantSendMessage)) > 0);
        user.setWebKey(cursor.getString(cursor.getColumnIndex(WebKey)));
        user.setTokenExpireDate(cursor.getString(cursor.getColumnIndex(Token)));
        user.setTokenExpireDate(cursor.getString(cursor.getColumnIndex(TokenExpireDate)));
        user.setBirthDay(cursor.getString(cursor.getColumnIndex(BirthDay)));
        user.setNationalCode(cursor.getString(cursor.getColumnIndex(NationalCode)));
        user.setFcmToken(cursor.getString(cursor.getColumnIndex(FcmToken)));


        return user;
    }

    public Boolean IsExist(long userID) {
        Boolean returnValue = false;

        Cursor cursor = database.rawQuery(" select " + UserID + " from " +
                Table_User + " where " + UserID + " = " + String.valueOf(userID), null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            returnValue = true;
        }

        cursor.close();

        return returnValue;
    }

    public int Update(User user) {
        ContentValues values = new ContentValues();

        if (user.getUserID() != -1)
            values.put(UserID, user.getUserID());
        if (user.getFK_RollID() != -1)
            values.put(FK_RollID, user.getFK_RollID());
        if (user.getFK_ProfileImageID() != -1)
            values.put(FK_ProfileImageID, user.getFK_ProfileImageID());
        if (user.getDescription() != null)
            values.put(Description, user.getDescription());
        if (user.getStatus() != -1)
            values.put(Status, user.getStatus());
        if (user.getAddress() != null)
            values.put(Address, user.getAddress());
        if (user.getUserName() != null)
            values.put(UserName, user.getUserName());
        if (user.getPassword() != null)
            values.put(Password, user.getPassword());
        if (user.getAllowReceiveMessage() != null)
            values.put(AllowReceiveMessage, user.getAllowReceiveMessage());
        if (user.getToken() != null)
            values.put(Token, user.getToken());
        if (user.getType() != -1)
            values.put(Type, user.getType());
        if (user.getTokenExpireDate() != null)
            values.put(TokenExpireDate, user.getTokenExpireDate());
        if (user.getConfirmed() != null)
            values.put(IsConfirmed, user.getConfirmed());
        if (user.getLastLogin() != null)
            values.put(LastLogin, user.getLastLogin());
        if (user.getFirstLogin() != null)
            values.put(FirstLogin, user.getFirstLogin());
        if (user.getWantReceiveMessage() != null)
            values.put(WantReceiveMessage, user.getWantReceiveMessage());
        if (user.getWantSendMessage() != null)
            values.put(WantSendMessage, user.getWantSendMessage());
        if (user.getAllowSendMessage() != null)
            values.put(AllowSendMessage, user.getAllowSendMessage());
        if (user.getBirthDay() != null)
            values.put(BirthDay, user.getBirthDay());
        if (user.getPhone1() != null)
            values.put(Phone1, user.getPhone1());
        if (user.getPhone2() != null)
            values.put(Phone2, user.getPhone2());
        if (user.getCellPhone() != null)
            values.put(CellPhone, user.getCellPhone());
        if (user.getCreateDate() != null)
            values.put(CreateDate, user.getCreateDate());
        if (user.getUpdateDate() != null)
            values.put(UpdateDate, user.getUpdateDate());
        if (user.getTokenExpireDate() != null)
            values.put(TokenExpireDate, user.getTokenExpireDate());
        if (user.getTokenExpireDate() != null)
            values.put(TokenExpireDate, user.getTokenExpireDate());
        if (user.getTokenExpireDate() != null)
            values.put(TokenExpireDate, user.getTokenExpireDate());
        if (user.getWebKey() != null)
            values.put(WebKey, user.getWebKey());
        if (user.getNationalCode() != null)
            values.put(NationalCode, user.getNationalCode());
        if (user.getSuspended() != null)
            values.put(IsSuspended, user.getSuspended());
        if (user.getEmail() != null)
            values.put(Email, user.getEmail());
        if (user.getLastLoginIp() != null)
            values.put(LastLoginIp, user.getLastLoginIp());
        if (user.getFcmToken() != null)
            values.put(FcmToken, user.getFcmToken());


        return database.update(Table_User, values, UserID + "=?", new String[]{String.valueOf(user.getUserID())});
    }

    public User Insert(User user) {
        ContentValues values = new ContentValues();

        values.put(UserID, user.getUserID());
        values.put(FK_RollID, user.getFK_RollID());
        values.put(FK_ProfileImageID, user.getFK_ProfileImageID());
        values.put(Description, user.getDescription());
        values.put(Status, user.getStatus());
        values.put(Type, user.getType());
        values.put(UserName, user.getUserName());
        values.put(FirstLogin, user.getFirstLogin());
        values.put(LastName, user.getLastName());
        values.put(CreateDate, user.getCreateDate());
        values.put(UpdateDate, user.getUpdateDate());
        values.put(IsDeleted, user.getIsDeleted());
        values.put(IsSuspended, user.getSuspended());
        values.put(AllowSendMessage, user.getAllowSendMessage());
        values.put(AllowReceiveMessage, user.getAllowReceiveMessage());
        values.put(Phone1, user.getPhone1());
        values.put(Phone2, user.getPhone2());
        values.put(CellPhone, user.getCellPhone());
        values.put(LastLoginIp, user.getLastLoginIp());
        values.put(WantSendMessage, user.getWantSendMessage());
        values.put(WantReceiveMessage, user.getWantReceiveMessage());
        values.put(FirstLogin, user.getFirstName());
        values.put(Password, user.getPassword());
        values.put(Address, user.getAddress());
        values.put(FcmToken, user.getFcmToken());
        values.put(IsOnline, user.getOnline());
        values.put(BirthDay, user.getBirthDay());
        values.put(Token, user.getToken());
        values.put(TokenExpireDate, user.getTokenExpireDate());
        values.put(WebKey, user.getWebKey());
        values.put(Email, user.getEmail());
        values.put(IsConfirmed, user.getConfirmed());
        values.put(NationalCode, user.getNationalCode());

        if (user.getUserID() == -1) {
            long insertID = (int) database.insert(Table_User, null, values);
            user.setUserID(insertID);
        } else {
            Boolean flag = IsExist(user.getUserID());
            if (flag) {
                Update(user);
            } else {
                long insertID = (int) database.insert(Table_User, null, values);
                user.setUserID(insertID);
            }
        }
        return user;


    }

    public Integer insert(List<User> userList) {

        int return_value = 0;
        ContentValues values = new ContentValues();
        for (User user : userList) {

            values.put(UserID, user.getUserID());
            values.put(FK_RollID, user.getFK_RollID());
            values.put(FK_ProfileImageID, user.getFK_ProfileImageID());
            values.put(Description, user.getDescription());
            values.put(Status, user.getStatus());
            values.put(Type, user.getType());
            values.put(UserName, user.getUserName());
            values.put(FirstLogin, user.getFirstLogin());
            values.put(LastName, user.getLastName());
            values.put(FcmToken, user.getFcmToken());
            values.put(CreateDate, user.getCreateDate());
            values.put(UpdateDate, user.getUpdateDate());
            values.put(IsDeleted, user.getIsDeleted());
            values.put(IsSuspended, user.getSuspended());
            values.put(AllowSendMessage, user.getAllowSendMessage());
            values.put(AllowReceiveMessage, user.getAllowReceiveMessage());
            values.put(Phone1, user.getPhone1());
            values.put(Phone2, user.getPhone2());
            values.put(CellPhone, user.getCellPhone());
            values.put(LastLoginIp, user.getLastLoginIp());
            values.put(WantSendMessage, user.getWantSendMessage());
            values.put(WantReceiveMessage, user.getWantReceiveMessage());
            values.put(FirstLogin, user.getFirstName());
            values.put(Password, user.getPassword());
            values.put(Address, user.getAddress());
            values.put(IsOnline, user.getIsDeleted());
            values.put(BirthDay, user.getBirthDay());
            values.put(Token, user.getToken());
            values.put(TokenExpireDate, user.getTokenExpireDate());
            values.put(WebKey, user.getWebKey());
            values.put(Email, user.getEmail());
            values.put(IsConfirmed, user.getConfirmed());
            values.put(NationalCode, user.getNationalCode());


            if (user.getUserID() == -1) {
                int insertID = (int) database.insert(Table_User, null, values);
                if (insertID != -1) return_value++;
            } else {
                Boolean flag = IsExist(user.getUserID());
                if (flag) {
                    int i = Update(user);
                    if (i == 1) {
                        return_value++;
                    }
                } else {
                    int insertId = (int) database.insert(Table_User, null, values);
                    if (insertId != -1)
                        return_value++;
                }
            }
            values.clear();
        }
        return return_value;
    }

//    public Category GetLast() {
//        Cursor cursor = database.rawQuery(" Select * from " +
//                Table_Product +
//                " Order by " +
//                UpdateDate +
//                " desc " + " limit 1", null);
//        cursor.moveToFirst();
//        Category category = new Category();
//        if (cursor.getCountTextView() > 0)
//            category = CursorToCategory(cursor);
//        else {
//            category.setCategoryID(-1);
//            category.setUpdateDate(Constants.DateOfOrigin);
//        }
//        cursor.close();
//        return category;
//    }


}


