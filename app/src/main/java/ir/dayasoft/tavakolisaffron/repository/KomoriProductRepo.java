package ir.dayasoft.tavakolisaffron.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.model.KomoriProduct;

/**
 * Created by mohammad on 01/22/2017.
 */

public class KomoriProductRepo {

    public static final String Table_KomoriProduct = "KomoriProduct";

    public static final String KomoriProductID = "KomoriProductID";
    public static final String FK_ProductID = "FK_ProductID";
    public static final String FK_CategoryID = "FK_CategoryID";
    public static final String Name = "Name";
    public static final String Price = "Price";
    public static final String ProductInventory = "ProductInventory";
    public static final String Description = "Description";
    public static final String Status = "status";
    public static final String Weight = "Weight";
    public static final String DiscountPrice = "DiscountPrice";
    public static final String Type = "Type";
    public static final String PrePayment = "PrePayment";
    public static final String IsDelete = "IsDelete";
    public static final String UpdateDate = "UpdateDate";
    public static final String CreateDate = "CreateDate";
    public static final String ImageUrl = "ImageUrl";
    public static final String ImageThumbUrl = "ImageThumbUrl";


    public static final String CreateTable_KomoriProduct = " create table "
            + Table_KomoriProduct + " ( "
            + KomoriProductID + " integer primary key autoincrement, "
            + FK_ProductID + " integer, "
            + FK_CategoryID + " integer,"
            + Name + " text, "
            + Price + " integer, "
            + Description + " text, "
            + ProductInventory + " integer, "
            + Status + " integer, "
            + ImageUrl + " integer, "
            + ImageThumbUrl + " integer, "
            + Weight + " integer, "
            + DiscountPrice + " integer, "
            + Type + " integer, "
            + PrePayment + " integer,"
            + IsDelete + " blob,"
            + UpdateDate + " text,"
            + CreateDate + " text"
            + " ) ;";

    SQLiteDatabase database;
    DatabaseHandler dbHelper;

    public KomoriProductRepo(Context context) {

        dbHelper = new DatabaseHandler(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
        database.close();
    }

    public void Delete(int komoriProductID) {
        database.delete(Table_KomoriProduct, KomoriProductID + " = " + komoriProductID, null);
    }

    public KomoriProduct Get(int komoriProductID) {
        Cursor cursor = database.rawQuery(
                " select * " + " from " + Table_KomoriProduct + " where " + KomoriProductID + " = " + String.valueOf(komoriProductID), null);
        cursor.moveToFirst();
        KomoriProduct komoriProduct = new KomoriProduct();
        if (cursor.getCount() > 0)
            komoriProduct = CursorToKomoriProduct(cursor);
        else
            komoriProduct.setKomoriProductID((int) -1);
        cursor.close();
        return komoriProduct;
    }


    public List<KomoriProduct> GetProductByCategory(int categoryID) {
        List<KomoriProduct> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select * from " + Table_KomoriProduct + " where " +
                FK_CategoryID + " = " + String.valueOf(categoryID) + " and " + Status + " =  0 and " + IsDelete + " = 0"  , null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            KomoriProduct cf = CursorToKomoriProduct(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }

    public List<KomoriProduct> Get() {
        List<KomoriProduct> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select * " + " from " + Table_KomoriProduct, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            KomoriProduct cf = CursorToKomoriProduct(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }

    public List<KomoriProduct> GetProductByPrice(int categoryID) {
        List<KomoriProduct> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select  *  from " + Table_KomoriProduct + " Where " +
                FK_CategoryID + " = " + String.valueOf(categoryID) + " and  " + Status + " = 0 ORDER BY " + Price + " DESC ", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            KomoriProduct cf = CursorToKomoriProduct(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }

    public List<KomoriProduct> GetProductByWeight(int categoryID) {
        List<KomoriProduct> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select  *  from " + Table_KomoriProduct + " Where " +
                FK_CategoryID + " = " + String.valueOf(categoryID) + " and  " + Status + " = 0 ORDER BY " + Weight + " DESC ", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            KomoriProduct cf = CursorToKomoriProduct(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }

    public List<KomoriProduct> GetProductByCompany(int categoryID) {
        List<KomoriProduct> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select  *  from " + Table_KomoriProduct + " Where " +
                FK_CategoryID + " = " + String.valueOf(categoryID) + " and  " + Status + " = 0 ORDER BY " + Name, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            KomoriProduct cf = CursorToKomoriProduct(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }




    public void clear() {
        database.delete(Table_KomoriProduct, null, null);
    }

    public KomoriProduct CursorToKomoriProduct(Cursor cursor) {
        KomoriProduct komoriProduct = new KomoriProduct();

        komoriProduct.setKomoriProductID(cursor.getInt(cursor.getColumnIndex(KomoriProductID)));
        komoriProduct.setFK_ProductID(cursor.getInt(cursor.getColumnIndex(FK_ProductID)));
        komoriProduct.setFK_CategoryID(cursor.getInt(cursor.getColumnIndex(FK_CategoryID)));
        komoriProduct.setName(cursor.getString(cursor.getColumnIndex(Name)));
        komoriProduct.setPrice(cursor.getInt(cursor.getColumnIndex(Price)));
        komoriProduct.setDescription(cursor.getString(cursor.getColumnIndex(Description)));
        komoriProduct.setProductInventory(cursor.getInt(cursor.getColumnIndex(ProductInventory)));
        komoriProduct.setStatus(cursor.getInt(cursor.getColumnIndex(Status)));
        komoriProduct.setWeight(cursor.getString(cursor.getColumnIndex(Weight)));
        komoriProduct.setDiscountPrice(cursor.getInt(cursor.getColumnIndex(DiscountPrice)));
        komoriProduct.setType(cursor.getInt(cursor.getColumnIndex(Type)));
        komoriProduct.setPrePayment(cursor.getInt(cursor.getColumnIndex(PrePayment)));
        komoriProduct.setDelete(cursor.getInt(cursor.getColumnIndex(IsDelete)) > 0);
        komoriProduct.setUpdateDate(cursor.getString(cursor.getColumnIndex(UpdateDate)));
        komoriProduct.setCreateDate(cursor.getString(cursor.getColumnIndex(CreateDate)));
        komoriProduct.setImageUrl(cursor.getString(cursor.getColumnIndex(ImageUrl)));
        komoriProduct.setImageThumbUrl(cursor.getString(cursor.getColumnIndex(ImageThumbUrl)));

        return komoriProduct;
    }

    public Boolean IsExist(int komoriProductID) {
        Boolean returnValue = false;

        Cursor cursor = database.rawQuery(" select " + KomoriProductID + " from " +
                Table_KomoriProduct + " where " + KomoriProductID + " = " + String.valueOf(komoriProductID), null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            returnValue = true;
        }

        cursor.close();

        return returnValue;
    }

    public int Update(KomoriProduct komoriProduct) {
        ContentValues values = new ContentValues();

        if (komoriProduct.getKomoriProductID() != -1)
            values.put(KomoriProductID, komoriProduct.getKomoriProductID());
        if (komoriProduct.getFK_CategoryID() != -1)
            values.put(FK_CategoryID, komoriProduct.getFK_CategoryID());
        if (komoriProduct.getFK_ProductID() != -1)
            values.put(FK_ProductID, komoriProduct.getFK_ProductID());
        if (komoriProduct.getName() != null)
            values.put(Name, komoriProduct.getName());
        if (komoriProduct.getPrice() != -1)
            values.put(Price, komoriProduct.getPrice());
        if (komoriProduct.getDescription() != null)
            values.put(Description, komoriProduct.getDescription());
        if (komoriProduct.getProductInventory() != -1)
            values.put(ProductInventory, komoriProduct.getProductInventory());
        if (komoriProduct.getStatus() != -1)
            values.put(Status, komoriProduct.getStatus());
        if (komoriProduct.getWeight() != null)
            values.put(Weight, komoriProduct.getWeight());
        if (komoriProduct.getDiscountPrice() != -1)
            values.put(DiscountPrice, komoriProduct.getDiscountPrice());
        if (komoriProduct.getType() != -1)
            values.put(Type, komoriProduct.getType());
        if (komoriProduct.getPrePayment() != -1)
            values.put(PrePayment, komoriProduct.getPrePayment());
        if (komoriProduct.getDelete() != null)
            values.put(IsDelete, komoriProduct.getDelete());
        if (komoriProduct.getCreateDate() != null)
            values.put(CreateDate, komoriProduct.getCreateDate());
        if (komoriProduct.getUpdateDate() != null)
            values.put(UpdateDate, komoriProduct.getUpdateDate());
        if (komoriProduct.getImageThumbUrl() != null)
            values.put(ImageThumbUrl, komoriProduct.getImageThumbUrl());
        if (komoriProduct.getImageUrl() != null)
            values.put(ImageUrl, komoriProduct.getImageUrl());


        return database.update(Table_KomoriProduct, values, KomoriProductID + "=?", new String[]{String.valueOf(komoriProduct.getKomoriProductID())});
    }


    public KomoriProduct Insert(KomoriProduct komoriProduct) {
        ContentValues values = new ContentValues();

        values.put(KomoriProductID, komoriProduct.getKomoriProductID());
        values.put(FK_CategoryID, komoriProduct.getFK_CategoryID());
        values.put(FK_ProductID, komoriProduct.getFK_ProductID());
        values.put(Name, komoriProduct.getName());
        values.put(Price, komoriProduct.getPrice());
        values.put(Description, komoriProduct.getDescription());
        values.put(ProductInventory, komoriProduct.getProductInventory());
        values.put(Status, komoriProduct.getStatus());
        values.put(Weight, komoriProduct.getWeight());
        values.put(DiscountPrice, komoriProduct.getDiscountPrice());
        values.put(Type, komoriProduct.getType());
        values.put(PrePayment, komoriProduct.getPrePayment());
        values.put(CreateDate, komoriProduct.getCreateDate());
        values.put(UpdateDate, komoriProduct.getUpdateDate());
        values.put(IsDelete, komoriProduct.getDelete());
        values.put(ImageUrl,komoriProduct.getImageUrl());
        values.put(ImageThumbUrl,komoriProduct.getImageThumbUrl());


        if (komoriProduct.getKomoriProductID() == -1) {
            int insertID = (int) database.insert(Table_KomoriProduct, null, values);
            komoriProduct.setKomoriProductID(insertID);
        } else {
            Boolean flag = IsExist(komoriProduct.getKomoriProductID());
            if (flag) {
                Update(komoriProduct);
            } else {
                int insertID = (int) database.insert(Table_KomoriProduct, null, values);
                komoriProduct.setKomoriProductID(insertID);
            }
        }
        return komoriProduct;


    }

    public Integer insert(List<KomoriProduct> komoriProductList) {

        int return_value = 0;
        ContentValues values = new ContentValues();
        for (KomoriProduct komoriProduct : komoriProductList) {

            values.put(KomoriProductID, komoriProduct.getKomoriProductID());
            values.put(FK_CategoryID, komoriProduct.getFK_CategoryID());
            values.put(FK_ProductID, komoriProduct.getFK_ProductID());
            values.put(Name, komoriProduct.getName());
            values.put(Price, komoriProduct.getPrice());
            values.put(Description, komoriProduct.getDescription());
            values.put(ProductInventory, komoriProduct.getProductInventory());
            values.put(Status, komoriProduct.getStatus());
            values.put(Weight, komoriProduct.getWeight());
            values.put(DiscountPrice, komoriProduct.getDiscountPrice());
            values.put(Type, komoriProduct.getType());
            values.put(PrePayment, komoriProduct.getPrePayment());
            values.put(CreateDate, komoriProduct.getCreateDate());
            values.put(UpdateDate, komoriProduct.getUpdateDate());
            values.put(IsDelete, komoriProduct.getDelete());
            values.put(ImageUrl,komoriProduct.getImageUrl());
            values.put(ImageThumbUrl,komoriProduct.getImageThumbUrl());


            if (komoriProduct.getKomoriProductID() == -1) {
                int insertID = (int) database.insert(Table_KomoriProduct, null, values);
                if (insertID != -1) return_value++;
            } else {
                Boolean flag = IsExist(komoriProduct.getKomoriProductID());
                if (flag) {
                    int i = Update(komoriProduct);
                    if (i == 1) {
                        return_value++;
                    }
                } else {
                    int insertId = (int) database.insert(Table_KomoriProduct, null, values);
                    if (insertId != -1)
                        return_value++;
                }
            }
            values.clear();
        }
        return return_value;
    }


    public KomoriProduct GetLast() {
        Cursor cursor = database.rawQuery(" Select * from " +
                Table_KomoriProduct +
                " Order by " +
                UpdateDate +
                " desc " + " limit 1", null);
        cursor.moveToFirst();
        KomoriProduct komoriProduct = new KomoriProduct();
        if (cursor.getCount() > 0)
            komoriProduct = CursorToKomoriProduct(cursor);
        else {
            komoriProduct.setKomoriProductID(-1);
            komoriProduct.setUpdateDate(Constants.DateOfOrigin);
        }
        cursor.close();
        return komoriProduct;
    }

}



