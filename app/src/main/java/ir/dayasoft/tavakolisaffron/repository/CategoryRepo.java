package ir.dayasoft.tavakolisaffron.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.model.Category;

/**
 * Created by mohammad on 01/22/2017.
 */

public class CategoryRepo {


    public static final String Table_Category = "Category";

    public static final String CategoryID = "CategoryID";
    public static final String Name = "Name";
    public static final String ParentID = "ParentID";
    public static final String UrlPicture = "UrlPicture";
    public static final String IsActive = "IsActive";
    public static final String Status = "Status";
    public static final String Type = "Type";
    public static final String IsDelete = "IsDelete";
    public static final String Orders = "Orders";
    public static final String UpdateDate = "UpdateDate";
    public static final String CreateDate = "CreateDate";


    public static final String CreateTable_category = " create table "
            + Table_Category + " ( "
            + CategoryID + " integer primary key autoincrement, "
            + Name + " text, "
            + ParentID + " integer,"
            + UrlPicture + " text, "
            + IsActive + " blob, "
            + Status + " integer, "
            + Type + " integer,"
            + IsDelete + " blob,"
            + UpdateDate + " text,"
            + Orders + " integer,"
            + CreateDate + " text"
            + " ) ;";

    SQLiteDatabase database;
    DatabaseHandler dbHelper;

    public CategoryRepo(Context context) {

        dbHelper = new DatabaseHandler(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
        database.close();
    }

    public void Delete(int categoryID) {
        database.delete(Table_Category, CategoryID + " = " + categoryID, null);
    }

    public Category Get(int categoryID) {
        Cursor cursor = database.rawQuery(
                " select * " + " from " + Table_Category + " where " + CategoryID + " = " + String.valueOf(categoryID), null);
        cursor.moveToFirst();
        Category category = new Category();
        if (cursor.getCount() > 0)
            category = CursorToCategory(cursor);
        else
            category.setCategoryID((int) -1);
        cursor.close();
        return category;
    }

    public List<Category> Get() {
        List<Category> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select * " + " from " + Table_Category, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Category cf = CursorToCategory(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }

    public void clear() {
        database.delete(Table_Category, null, null);
    }

    public Category CursorToCategory(Cursor cursor) {
        Category category = new Category();

        category.setCategoryID(cursor.getInt(cursor.getColumnIndex(CategoryID)));
        category.setName(cursor.getString(cursor.getColumnIndex(Name)));
        category.setParentID(cursor.getInt(cursor.getColumnIndex(ParentID)));
        category.setUrlPicture(cursor.getString(cursor.getColumnIndex(UrlPicture)));
        category.setActive(cursor.getInt(cursor.getColumnIndex(IsActive)) > 0);
        category.setDelete(cursor.getInt(cursor.getColumnIndex(IsDelete)) > 0);
        category.setType(cursor.getInt(cursor.getColumnIndex(Type)));
        category.setStatus(cursor.getInt(cursor.getColumnIndex(Status)));
        category.setOrders(cursor.getInt(cursor.getColumnIndex(Orders)));
        category.setUpdateDate(cursor.getString(cursor.getColumnIndex(UpdateDate)));
        category.setCreateDate(cursor.getString(cursor.getColumnIndex(CreateDate)));

        return category;
    }

    public Boolean IsExist(int categoryID) {
        Boolean returnValue = false;

        Cursor cursor = database.rawQuery(" select " + CategoryID + " from " +
                Table_Category + " where " + CategoryID + " = " + String.valueOf(categoryID), null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            returnValue = true;
        }

        cursor.close();

        return returnValue;
    }

    public int Update(Category category) {
        ContentValues values = new ContentValues();

        if (category.getCategoryID() != -1)
            values.put(CategoryID, category.getCategoryID());
        if (category.getName() != null)
            values.put(Name, category.getName());
        if (category.getStatus() != -1)
            values.put(Status, category.getStatus());
        if (category.getType() != -1)
            values.put(Type, category.getType());
        if (category.getParentID() != -1)
            values.put(ParentID, category.getParentID());
        if (category.getUrlPicture() != null)
            values.put(UrlPicture, category.getUrlPicture());
        if (category.getDelete() != null)
            values.put(IsDelete, category.getDelete());
        if (category.getActive() != null)
            values.put(IsActive, category.getActive());
        if (category.getCreateDate() != null)
            values.put(CreateDate, category.getCreateDate());
        if (category.getUpdateDate() != null)
            values.put(UpdateDate, category.getUpdateDate());
        if (category.getOrders() != null)
            values.put(Orders, category.getOrders());

        return database.update(Table_Category, values, CategoryID + "=?", new String[]{String.valueOf(category.getCategoryID())});
    }

    public List<Category> GetCategoryByParent(int id) {
        List<Category> categoryList = new ArrayList<>();
        Cursor cursor = database.rawQuery("select * from "
                + Table_Category + " where " + ParentID + " = " + String.valueOf(id) + " and  IsDelete  = " + 0
                + " order by " + Orders , null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            {
                Category category = CursorToCategory(cursor);
                categoryList.add(category);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return categoryList;
    }

    public Category Insert(Category category) {
        ContentValues values = new ContentValues();

        values.put(CategoryID, category.getCategoryID());
        values.put(Name, category.getName());
        values.put(ParentID, category.getParentID());
        values.put(UrlPicture, category.getUrlPicture());
        values.put(CreateDate, category.getCreateDate());
        values.put(UpdateDate, category.getUpdateDate());
        values.put(IsActive, category.getActive());
        values.put(IsDelete, category.getDelete());
        values.put(Type, category.getType());
        values.put(Status, category.getStatus());
        values.put(Orders, category.getOrders());


        if (category.getCategoryID() == -1) {
            int insertID = (int) database.insert(Table_Category, null, values);
            category.setCategoryID(insertID);
        } else {
            Boolean flag = IsExist(category.getCategoryID());
            if (flag) {
                Update(category);
            } else {
                int insertID = (int) database.insert(Table_Category, null, values);
                category.setCategoryID(insertID);
            }
        }
        return category;


    }

    public Integer insert(List<Category> categoryList) {

        int return_value = 0;
        ContentValues values = new ContentValues();
        for (Category category : categoryList) {

            values.put(CategoryID, category.getCategoryID());
            values.put(Name, category.getName());
            values.put(ParentID, category.getParentID());
            values.put(UrlPicture, category.getUrlPicture());
            values.put(CreateDate, category.getCreateDate());
            values.put(UpdateDate, category.getUpdateDate());
            values.put(IsActive, category.getActive());
            values.put(IsDelete, category.getDelete());
            values.put(Type, category.getType());
            values.put(Status, category.getStatus());
            values.put(Orders, category.getOrders());


            if (category.getCategoryID() == -1) {
                int insertID = (int) database.insert(Table_Category, null, values);
                if (insertID != -1) return_value++;
            } else {
                Boolean flag = IsExist(category.getCategoryID());
                if (flag) {
                    int i = Update(category);
                    if (i == 1) {
                        return_value++;
                    }
                } else {
                    int insertId = (int) database.insert(Table_Category, null, values);
                    if (insertId != -1)
                        return_value++;
                }
            }
            values.clear();
        }
        return return_value;
    }

    public Category GetLast() {
        Cursor cursor = database.rawQuery(" Select * from " +
                Table_Category +
                " Order by " +
                UpdateDate +
                " desc " + " limit 1", null);
        cursor.moveToFirst();
        Category category = new Category();
        if (cursor.getCount() > 0)
            category = CursorToCategory(cursor);
        else {
            category.setCategoryID(-1);
            category.setUpdateDate(Constants.DateOfOrigin);
        }
        cursor.close();
        return category;
    }


}
