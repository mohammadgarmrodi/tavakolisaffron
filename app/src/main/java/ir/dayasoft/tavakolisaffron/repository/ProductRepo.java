package ir.dayasoft.tavakolisaffron.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.model.Category;
import ir.dayasoft.tavakolisaffron.model.Product;

/**
 * Created by mohammad on 01/22/2017.
 */

public class ProductRepo {


    public static final String Table_Product = "Product";

    public static final String ProductID = "ProductID";
    public static final String FK_CategoryID = "FK_CategoryID";
    public static final String Name = "Name";
    public static final String Description = "Description";
    public static final String Price = "Price";
    public static final String ManufactureDate = "ManufactureDate";
    public static final String ExpirationDate = "ExpirationDate";

    public static final String SerialNumber = "SerialNumber";

    public static final String DiscountPrice = "DiscountPrice";

    public static final String Status = "status";
    public static final String Type = "Type";
    public static final String IsDelete = "IsDelete";
    public static final String UpdateDate = "UpdateDate";
    public static final String CreateDate = "CreateDate";


    public static final String CreateTable_Product = " create table "
            + Table_Product + " ( "
            + ProductID + " integer primary key autoincrement, "
            + FK_CategoryID + " integer,"
            + Name + " text, "
            + Description + " text, "
            + Price + " integer,"
            + DiscountPrice + " integer, "
            + ManufactureDate + " text, "
            + ExpirationDate + " text, "
            + SerialNumber + " text, "
            + IsDelete + " blob,"
            + Status + " integer, "
            + Type + " integer,"
            + UpdateDate + " text,"
            + CreateDate + " text"
            + " ) ;";

    SQLiteDatabase database;
    DatabaseHandler dbHelper;

    public ProductRepo(Context context) {

        dbHelper = new DatabaseHandler(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
        database.close();
    }

    public void Delete(int productID) {
        database.delete(Table_Product, ProductID + " = " + productID, null);
    }

    public Product Get(int productID) {
        Cursor cursor = database.rawQuery(
                " select * " + " from " + Table_Product + " where " + ProductID + " = " + String.valueOf(productID), null);
        cursor.moveToFirst();
        Product product = new Product();
        if (cursor.getCount() > 0)
            product = CursorToProduct(cursor);
        else
            product.setProductID((int) -1);
        cursor.close();
        return product;
    }

    public List<Product> Get() {
        List<Product> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select * " + " from " + Table_Product, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Product cf = CursorToProduct(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }

    public void clear() {
        database.delete(Table_Product, null, null);
    }

    public Product CursorToProduct(Cursor cursor) {
        Product product = new Product();

        product.setProductID(cursor.getInt(cursor.getColumnIndex(ProductID)));
        product.setFk_CategoryID(cursor.getInt(cursor.getColumnIndex(FK_CategoryID)));
        product.setName(cursor.getString(cursor.getColumnIndex(Name)));
        product.setDescription(cursor.getString(cursor.getColumnIndex(Description)));
        product.setPrice(cursor.getInt(cursor.getColumnIndex(Price)));
        product.setDiscountPrice(cursor.getInt(cursor.getColumnIndex(DiscountPrice)));
        product.setManufactureDate(cursor.getString(cursor.getColumnIndex(ManufactureDate)));
        product.setExpirationDate(cursor.getString(cursor.getColumnIndex(ExpirationDate)));
        product.setSerialNumber(cursor.getString(cursor.getColumnIndex(SerialNumber)));

        product.setDelete(cursor.getInt(cursor.getColumnIndex(IsDelete)) > 0);
        product.setType(cursor.getInt(cursor.getColumnIndex(Type)));
        product.setStatus(cursor.getInt(cursor.getColumnIndex(Status)));
        product.setUpdateDate(cursor.getString(cursor.getColumnIndex(UpdateDate)));
        product.setCreateDate(cursor.getString(cursor.getColumnIndex(CreateDate)));

        return product;
    }

    public Boolean IsExist(int productID) {
        Boolean returnValue = false;

        Cursor cursor = database.rawQuery(" select " + ProductID + " from " +
                Table_Product + " where " + ProductID + " = " + String.valueOf(productID), null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            returnValue = true;
        }

        cursor.close();

        return returnValue;
    }

    public int Update(Product product) {
        ContentValues values = new ContentValues();

        if (product.getProductID() != -1)
            values.put(ProductID, product.getProductID());
        if (product.getFk_CategoryID() != -1)
            values.put(FK_CategoryID, product.getFk_CategoryID());
        if (product.getName() != null)
            values.put(Name, product.getName());
        if (product.getDescription() != null)
            values.put(Description, product.getDescription());
        if (product.getPrice() != -1)
            values.put(Price, product.getPrice());
        if (product.getDiscountPrice() != -1)
            values.put(DiscountPrice, product.getDiscountPrice());
        if (product.getManufactureDate() != null)
            values.put(ManufactureDate, product.getManufactureDate());
        if (product.getExpirationDate() != null)
            values.put(ExpirationDate, product.getExpirationDate());
        if (product.getSerialNumber() != null)
            values.put(SerialNumber, product.getSerialNumber());
        if (product.getStatus() != -1)
            values.put(Status, product.getStatus());
        if (product.getType() != -1)
            values.put(Type, product.getType());
        if (product.getDelete() != null)
            values.put(IsDelete, product.getDelete());
        if (product.getCreateDate() != null)
            values.put(CreateDate, product.getCreateDate());
        if (product.getUpdateDate() != null)
            values.put(UpdateDate, product.getUpdateDate());


        return database.update(Table_Product, values, ProductID + "=?", new String[]{String.valueOf(product.getProductID())});
    }


    public Product Insert(Product product) {
        ContentValues values = new ContentValues();

        values.put(ProductID, product.getProductID());
        values.put(FK_CategoryID, product.getFk_CategoryID());
        values.put(Name, product.getName());
        values.put(Description, product.getDescription());
        values.put(Price, product.getPrice());
        values.put(DiscountPrice, product.getDiscountPrice());
        values.put(ManufactureDate, product.getManufactureDate());
        values.put(ExpirationDate, product.getExpirationDate());
        values.put(SerialNumber, product.getSerialNumber());
        values.put(CreateDate, product.getCreateDate());
        values.put(UpdateDate, product.getUpdateDate());
        values.put(IsDelete, product.getDelete());
        values.put(Type, product.getType());
        values.put(Status, product.getStatus());


        if (product.getProductID() == -1) {
            int insertID = (int) database.insert(Table_Product, null, values);
            product.setProductID(insertID);
        } else {
            Boolean flag = IsExist(product.getProductID());
            if (flag) {
                Update(product);
            } else {
                int insertID = (int) database.insert(Table_Product, null, values);
                product.setProductID(insertID);
            }
        }
        return product;


    }

    public Integer insert(List<Product> productList) {

        int return_value = 0;
        ContentValues values = new ContentValues();
        for (Product product : productList) {

            values.put(ProductID, product.getProductID());
            values.put(FK_CategoryID, product.getFk_CategoryID());
            values.put(Name, product.getName());
            values.put(Description, product.getDescription());
            values.put(Price, product.getPrice());
            values.put(DiscountPrice, product.getDiscountPrice());
            values.put(ManufactureDate, product.getManufactureDate());
            values.put(ExpirationDate, product.getExpirationDate());
            values.put(SerialNumber, product.getSerialNumber());
            values.put(CreateDate, product.getCreateDate());
            values.put(UpdateDate, product.getUpdateDate());
            values.put(IsDelete, product.getDelete());
            values.put(Type, product.getType());
            values.put(Status, product.getStatus());


            if (product.getProductID() == -1) {
                int insertID = (int) database.insert(Table_Product, null, values);
                if (insertID != -1) return_value++;
            } else {
                Boolean flag = IsExist(product.getProductID());
                if (flag) {
                    int i = Update(product);
                    if (i == 1) {
                        return_value++;
                    }
                } else {
                    int insertId = (int) database.insert(Table_Product, null, values);
                    if (insertId != -1)
                        return_value++;
                }
            }
            values.clear();
        }
        return return_value;
    }

    public Product GetLast() {
        Cursor cursor = database.rawQuery(" Select * from " +
                Table_Product +
                " Order by " +
                UpdateDate +
                " desc " + " limit 1", null);
        cursor.moveToFirst();
        Product product = new Product();
        if (cursor.getCount() > 0)
            product = CursorToProduct(cursor);
        else {
            product.setProductID(-1);
            product.setUpdateDate(Constants.DateOfOrigin);
        }
        cursor.close();
        return product;
    }


}
