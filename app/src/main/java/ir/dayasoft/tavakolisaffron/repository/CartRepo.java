package ir.dayasoft.tavakolisaffron.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.model.Cart;
import ir.dayasoft.tavakolisaffron.model.Category;
import ir.dayasoft.tavakolisaffron.model.KomoriProduct;

/**
 * Created by mohammad on 01/23/2017.
 */

public class CartRepo {
    public static final String Table_Cart = "Cart";

    public static final String CartID = "CartID";
    public static final String FK_ProductID = "FK_ProductID";
    public static final String Count = "Count";


    SQLiteDatabase database;
    DatabaseHandler dbHelper;


    public static final String CreateTable_cart = " create table "
            + Table_Cart + " ( "
            + CartID + " integer primary key autoincrement, "
            + FK_ProductID + " integer, "
            + Count + " integer"
            + " ) ;";


    public CartRepo(Context context) {

        dbHelper = new DatabaseHandler(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
        database.close();
    }

    public void Delete(int cartID) {
        database.delete(Table_Cart, FK_ProductID + " = " + cartID, null);
    }

    public Cart Get(int ProductID) {
        Cursor cursor = database.rawQuery(
                " select * " + " from " + Table_Cart + " where " + FK_ProductID + " = " + String.valueOf(ProductID), null);
        cursor.moveToFirst();
        Cart cart = new Cart();
        if (cursor.getCount() > 0)
            cart = CursorToCart(cursor);
        else
            cart.setCartID((int) -1);
        cursor.close();
        return cart;
    }

    public List<Cart> Get() {
        List<Cart> AllApp = new ArrayList<>();

        Cursor cursor = database.rawQuery(" select * " + " from " + Table_Cart, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Cart cf = CursorToCart(cursor);
            AllApp.add(cf);
            cursor.moveToNext();
        }
        cursor.close();
        return AllApp;
    }

    public void clear() {
        database.delete(Table_Cart, Count, null);
    }

    public Cart CursorToCart(Cursor cursor) {
        Cart cart = new Cart();

        cart.setCartID(cursor.getInt(cursor.getColumnIndex(CartID)));
        cart.setFK_ProductID(cursor.getInt(cursor.getColumnIndex(FK_ProductID)));
        cart.setCount(cursor.getInt(cursor.getColumnIndex(Count)));


        return cart;
    }

    public Boolean IsExist(int cartID) {
        Boolean returnValue = false;

        Cursor cursor = database.rawQuery(" select " + CartID + " from " +
                Table_Cart + " where " + CartID + " = " + String.valueOf(cartID), null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            returnValue = true;
        }

        cursor.close();

        return returnValue;
    }

    public int Update(Cart cart) {
        ContentValues values = new ContentValues();

        if (cart.getCartID() != -1)
            values.put(CartID, cart.getCartID());
        if (cart.getFK_ProductID() != -1)
            values.put(FK_ProductID, cart.getFK_ProductID());
        if (cart.getCount() != -1)
            values.put(Count, cart.getCount());


        return database.update(Table_Cart, values, CartID + "=?", new String[]{String.valueOf(cart.getCartID())});
    }


    public Cart Insert(Cart cart) {
        ContentValues values = new ContentValues();

        values.put(CartID, cart.getCartID());
        values.put(FK_ProductID, cart.getFK_ProductID());
        values.put(Count, cart.getCount());

        if (cart.getCartID() == -1) {
            int insertID = (int) database.insert(Table_Cart, null, values);
            cart.setCartID(insertID);
        } else {
            Boolean flag = IsExist(cart.getCartID());
            if (flag) {
                Update(cart);
            } else {
                int insertID = (int) database.insert(Table_Cart, null, values);
                cart.setCartID(insertID);
            }
        }
        return cart;


    }

    public Integer insert(List<Cart> cartList) {

        int return_value = 0;
        ContentValues values = new ContentValues();
        for (Cart cart : cartList) {

            values.put(CartID, cart.getCartID());
            values.put(FK_ProductID, cart.getFK_ProductID());
            values.put(Count, cart.getCount());


            if (cart.getCartID() == -1) {
                int insertID = (int) database.insert(Table_Cart, null, values);
                if (insertID != -1) return_value++;
            } else {
                Boolean flag = IsExist(cart.getCartID());
                if (flag) {
                    int i = Update(cart);
                    if (i == 1) {
                        return_value++;
                    }
                } else {
                    int insertId = (int) database.insert(Table_Cart, null, values);
                    if (insertId != -1)
                        return_value++;
                }
            }
            values.clear();
        }
        return return_value;
    }


    public Cart GetCart() {
        Cursor cursor = database.rawQuery(
                " Select "
                        + CategoryRepo.Table_Category + "." + CategoryRepo.Name + " as kName , "
                        + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Name + " , "
                        + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Price + " , "
                        + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Weight + " , "
                        + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.KomoriProductID + " , "
                        + CategoryRepo.Table_Category + "." + CategoryRepo.CategoryID + " , "
                        + Table_Cart + "." + Count
                        + " From "
                        + Table_Cart
                        + " INNER join "
                        + KomoriProductRepo.Table_KomoriProduct
                        + " on "
                        + Table_Cart + "." + FK_ProductID
                        + " = "
                        + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.KomoriProductID
                        + " INNER join "
                        + CategoryRepo.Table_Category
                        + " on "
                        + CategoryRepo.Table_Category + "." + CategoryRepo.CategoryID
                        + " = "
                        + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.FK_CategoryID
                , null);
        cursor.moveToFirst();
        Cart cart = new Cart();
        if (cursor.getCount() > 0)
            cart = CursorCartJoinKomoriProductJoinCategory(cursor);
        else
            cart.setCartID(-1);
        cursor.close();
        return cart;
    }

    public Cart GetSum() {
        Cursor cursor = database.rawQuery(
                " Select sum(" + Table_Cart + " . " + Count + " ) as Count,sum( " + KomoriProductRepo.Price + " * " + Table_Cart + " . " + Count + ") as Price"
                        + " From "
                        + Table_Cart
                        + " INNER join "
                        + KomoriProductRepo.Table_KomoriProduct
                        + " on "
                        + Table_Cart + " . " + FK_ProductID
                        + " = "
                        + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.KomoriProductID, null);
        cursor.moveToFirst();
        Cart cart = new Cart();
        if (cursor.getCount() > 0)
            cart = CursorCartJoinKomoriProductJoinCategorySum(cursor);
        else
            cart.setCartID(-1);
        cursor.close();
        return cart;
    }

    public List<Cart> GetCartList() {
        List<Cart> AllFood = new ArrayList<>();
        String[] selectionArgs = {String.valueOf(1)};

        Cursor cursor = database.rawQuery(" Select "
                + CategoryRepo.Table_Category + "." + CategoryRepo.Name + " , "
                + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Name + " as kName , "
                + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Price + " , "
                + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Price + " , "
                + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Weight + " , "
                + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.KomoriProductID + " , "
                + CategoryRepo.Table_Category + "." + CategoryRepo.CategoryID + " , "
                + Table_Cart + "." + Count
                + " From "
                + Table_Cart
                + " INNER join "
                + KomoriProductRepo.Table_KomoriProduct
                + " on "
                + Table_Cart + "." + FK_ProductID
                + " = "
                + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.KomoriProductID
                + " INNER join "
                + CategoryRepo.Table_Category
                + " on "
                + CategoryRepo.Table_Category + "." + CategoryRepo.CategoryID
                + " = "
                + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.FK_CategoryID, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Cart cart = CursorCartJoinKomoriProductJoinCategory(cursor);
            AllFood.add(cart);
            cursor.moveToNext();
        }
        cursor.close();
        return AllFood;
    }

    public Cart CursorCartJoinKomoriProductJoinCategory(Cursor cursor) {
        Cart cart = new Cart();
        cart.setCount(cursor.getInt(cursor.getColumnIndex(Count)));

        Category category = new Category();
        category.setCategoryID(cursor.getInt(cursor.getColumnIndex(CategoryRepo.CategoryID)));
        category.setName(cursor.getString(cursor.getColumnIndex(CategoryRepo.Name)));
        cart.setCategory(category);

        KomoriProduct komoriProduct = new KomoriProduct();
        komoriProduct.setName(cursor.getString(cursor.getColumnIndex("kName")));
        komoriProduct.setPrice(cursor.getInt(cursor.getColumnIndex(String.valueOf(KomoriProductRepo.Price))));
        komoriProduct.setKomoriProductID(cursor.getInt(cursor.getColumnIndex(String.valueOf(KomoriProductRepo.KomoriProductID))));
        komoriProduct.setWeight(cursor.getString(cursor.getColumnIndex(KomoriProductRepo.Weight)));

        cart.setKomoriProduct(komoriProduct);

        return cart;
    }

    public Cart CursorCartJoinKomoriProductJoinCategorySum(Cursor cursor) {
        Cart cart = new Cart();
        cart.setCount(cursor.getInt(cursor.getColumnIndex(String.valueOf(Count))));

        KomoriProduct komoriProduct = new KomoriProduct();
        komoriProduct.setPrice(cursor.getInt(cursor.getColumnIndex(String.valueOf(KomoriProductRepo.Price))));

        cart.setKomoriProduct(komoriProduct);

        return cart;
    }


    public Integer InsertCart(Cart cart) {
        ContentValues values = new ContentValues();
        Integer result = 0;

        values.put(Count, cart.getCount());
        values.put(FK_ProductID, cart.getFK_ProductID());
        values.put(CartID, cart.getCartID());


        if (isExist(cart.getFK_ProductID())) {
            result = UpdateByCartNumber(cart);
        } else {

            int insertID = (int) database.insert(Table_Cart, null, values);
            cart.setCartID(insertID);


        }

        return result;
    }

    public boolean isExist(Integer fk_productID) {
        boolean returnValue = false;
        Cursor cursor = database.rawQuery(
                " select "
                        + FK_ProductID
                        + " from "
                        + Table_Cart
                        + " where "
                        + FK_ProductID
                        + " = "
                        + String.valueOf(fk_productID), null);

        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            returnValue = true;
        }
        cursor.close();
        return returnValue;
    }

    public int UpdateByCartNumber(Cart cart) {
        ContentValues values = new ContentValues();

        if (cart.getCartID() != null)
            values.put(CartID, cart.getCartID());
        if (cart.getFK_ProductID() != null)
            values.put(FK_ProductID, cart.getFK_ProductID());
        if (cart.getCount() != null)
            values.put(Count, cart.getCount());


        return database.update(Table_Cart, values, FK_ProductID + "=?", new String[]{String.valueOf(cart.getFK_ProductID())});
    }


    public List<Cart> GetCartListByCategory(int categoryID) {
        List<Cart> cartList = new ArrayList<>();
        String[] selectionArgs = {String.valueOf(1)};

        Cursor cursor = database.rawQuery(" Select "
                + CategoryRepo.Table_Category + "." + CategoryRepo.Name + " , "
                + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Name + " as kName , "
                + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Price + " , "
                + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.Weight + " , "
                + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.KomoriProductID + " , "
                + CategoryRepo.Table_Category + "." + CategoryRepo.CategoryID + " , "
                + Table_Cart + "." + Count
                + " From "
                + Table_Cart
                + " INNER join "
                + KomoriProductRepo.Table_KomoriProduct
                + " on "
                + Table_Cart + "." + FK_ProductID
                + " = "
                + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.KomoriProductID
                + " INNER join "
                + CategoryRepo.Table_Category
                + " on "
                + CategoryRepo.Table_Category + "." + CategoryRepo.CategoryID
                + " = "
                + KomoriProductRepo.Table_KomoriProduct + "." + KomoriProductRepo.FK_CategoryID + " where "
                + CategoryRepo.Table_Category + " . " + CategoryRepo.CategoryID + " = " + String.valueOf(categoryID), null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Cart cart = CursorCartJoinKomoriProductJoinCategory(cursor);
            cartList.add(cart);
            cursor.moveToNext();
        }
        cursor.close();
        return cartList;
    }




}


