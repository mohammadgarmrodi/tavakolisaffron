package ir.dayasoft.tavakolisaffron.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ir.dayasoft.tavakolisaffron.R;

/**
 * Created by mohammad on 02/06/2017.
 */

public class OrderListDetailHolder extends MainViewHolder {
    private LinearLayout itemLinearLayout;
    private TextView companyTextView, weightTextView, priceTextView, countTextView, sumPriceTextView;
    private TextView geramTextView;
    public TextView getCompanyTextView() {
        return companyTextView;
    }

    public LinearLayout getItemLinearLayout() {
        return itemLinearLayout;
    }

    public void setItemLinearLayout(LinearLayout itemLinearLayout) {
        this.itemLinearLayout = itemLinearLayout;
    }

    public void setCompanyTextView(TextView companyTextView) {
        this.companyTextView = companyTextView;

    }

    public TextView getWeightTextView() {
        return weightTextView;
    }

    public void setWeightTextView(TextView weightTextView) {
        this.weightTextView = weightTextView;
    }

    public TextView getPriceTextView() {
        return priceTextView;
    }

    public void setPriceTextView(TextView priceTextView) {
        this.priceTextView = priceTextView;
    }

    public TextView getCountTextView() {
        return countTextView;
    }

    public void setCountTextView(TextView countTextView) {
        this.countTextView = countTextView;
    }

    public TextView getSumPriceTextView() {
        return sumPriceTextView;
    }

    public TextView getGeramTextView() {
        return geramTextView;
    }

    public void setSumPriceTextView(TextView sumPriceTextView) {
        this.sumPriceTextView = sumPriceTextView;
    }

    public OrderListDetailHolder(View itemView) {
        super(itemView);
        companyTextView = (TextView) itemView.findViewById(R.id.orderListDetail_companyName_textView);
        weightTextView = (TextView) itemView.findViewById(R.id.orderListDetail_weight_textView);
        priceTextView = (TextView) itemView.findViewById(R.id.orderListDetail_price_textView);
        sumPriceTextView = (TextView) itemView.findViewById(R.id.orderListDetail_sum_textView);
        countTextView = (TextView) itemView.findViewById(R.id.orderListDetail_number_textView);
        itemLinearLayout=(LinearLayout) itemView.findViewById(R.id.orderListDetail_item_linearLayout);
        geramTextView=(TextView)itemView.findViewById(R.id.orderListDetail_geram_textView);
    }


}
