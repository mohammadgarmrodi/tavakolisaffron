package ir.dayasoft.tavakolisaffron.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ir.dayasoft.tavakolisaffron.R;

/**
 * Created by mohammad on 01/26/2017.
 */

public class BuyBasketHolder extends MainViewHolder {
    private TextView categoryTextView;
    private TextView companyTextView;
    private RelativeLayout buyBasketSecondRelativeLayout,warningRelativeLayout;
    private TextView priceTextView;
    private LinearLayout buyBasketRelativeLayout;
    private TextView weightTextView;
    private TextView numberTextView;
    private TextView sumPriceTextView;
    private TextView plusTextView;
    private TextView minusTextView;
    private LinearLayout weightLinearLayout;
    private LinearLayout secondLinearLayout;

    public TextView getCategoryTextView() {
        return categoryTextView;
    }

    public LinearLayout getBuyBasketRelativeLayout() {
        return buyBasketRelativeLayout;
    }

    public RelativeLayout getWarningRelativeLayout() {
        return warningRelativeLayout;
    }

    public void setWarningRelativeLayout(RelativeLayout warningRelativeLayout) {
        this.warningRelativeLayout = warningRelativeLayout;
    }

    public void setBuyBasketRelativeLayout(LinearLayout buyBasketRelativeLayout) {
        this.buyBasketRelativeLayout = buyBasketRelativeLayout;
    }

    public LinearLayout getWeightLinearLayout() {
        return weightLinearLayout;
    }

    public void setCategoryTextView(TextView categoryTextView) {


        this.categoryTextView = categoryTextView;
    }

    public TextView getCompanyTextView() {
        return companyTextView;
    }

    public void setCompanyTextView(TextView companyTextView) {
        this.companyTextView = companyTextView;
    }

    public RelativeLayout getBuyBasketSecondRelativeLayout() {
        return buyBasketSecondRelativeLayout;
    }

    public void setBuyBasketSecondRelativeLayout(RelativeLayout buyBasketSecondRelativeLayout) {
        this.buyBasketSecondRelativeLayout = buyBasketSecondRelativeLayout;
    }

    public TextView getPriceTextView() {
        return priceTextView;
    }

    public void setPriceTextView(TextView priceTextView) {
        this.priceTextView = priceTextView;
    }

    public TextView getWeightTextView() {
        return weightTextView;
    }

    public void setWeightTextView(TextView weightTextView) {
        this.weightTextView = weightTextView;
    }

    public TextView getNumberTextView() {
        return numberTextView;
    }

    public void setNumberTextView(TextView numberTextView) {
        this.numberTextView = numberTextView;
    }

    public TextView getSumPriceTextView() {
        return sumPriceTextView;
    }

    public LinearLayout getSecondLinearLayout() {
        return secondLinearLayout;
    }

    public void setSecondLinearLayout(LinearLayout secondLinearLayout) {
        this.secondLinearLayout = secondLinearLayout;
    }

    public void setSumPriceTextView(TextView sumPriceTextView) {
        this.sumPriceTextView = sumPriceTextView;

    }

    public TextView getPlusTextView() {
        return plusTextView;
    }

    public void setWeightLinearLayout(LinearLayout weightLinearLayout) {
        this.weightLinearLayout = weightLinearLayout;
    }

    public void setPlusTextView(TextView plusTextView) {
        this.plusTextView = plusTextView;
    }

    public TextView getMinusTextView() {
        return minusTextView;
    }

    public void setMinusTextView(TextView minusTextView) {
        this.minusTextView = minusTextView;
    }



    public BuyBasketHolder(View itemView) {
        super(itemView);
        priceTextView = (TextView) itemView.findViewById(R.id.buyBasket_price_textView);
        weightTextView = (TextView) itemView.findViewById(R.id.buyBasket_weight_textView);
        numberTextView = (TextView) itemView.findViewById(R.id.buyBasket_count_textView);
        sumPriceTextView = (TextView) itemView.findViewById(R.id.buyBasket_sumPrice_textView);
        plusTextView = (TextView) itemView.findViewById(R.id.buyBasket_plus_textView);
        minusTextView = (TextView) itemView.findViewById(R.id.buyBasket_minus_textView);
        categoryTextView = (TextView) itemView.findViewById(R.id.buyBasket_category_textView);
        companyTextView = (TextView) itemView.findViewById(R.id.buyBasket_company_textView);
        buyBasketRelativeLayout = (LinearLayout) itemView.findViewById(R.id.buyBasket_item_relativeLayout);
        warningRelativeLayout=(RelativeLayout)itemView.findViewById(R.id.buyBasketItem_warning_relativeLayout);
        secondLinearLayout=(LinearLayout) itemView.findViewById(R.id.buyBasket_secondLinearLayout);
        weightLinearLayout=(LinearLayout)itemView.findViewById(R.id.weight_linearLayout);


    }
}
