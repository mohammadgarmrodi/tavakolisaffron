package ir.dayasoft.tavakolisaffron.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ir.dayasoft.tavakolisaffron.R;

/**
 * Created by mohammad on 02/20/2017.
 */

public class DialogHolder extends MainViewHolder {
    private TextView nameTextView,weightTextView,priceTextView;
    private LinearLayout mainLinearLayout;

    public TextView getNameTextView() {
        return nameTextView;
    }

    public void setNameTextView(TextView nameTextView) {
        this.nameTextView = nameTextView;
    }

    public TextView getWeightTextView() {
        return weightTextView;
    }

    public void setWeightTextView(TextView weightTextView) {
        this.weightTextView = weightTextView;
    }

    public TextView getPriceTextView() {
        return priceTextView;
    }

    public LinearLayout getMainLinearLayout() {
        return mainLinearLayout;
    }

    public void setMainLinearLayout(LinearLayout mainLinearLayout) {
        this.mainLinearLayout = mainLinearLayout;
    }

    public void setPriceTextView(TextView priceTextView) {
        this.priceTextView = priceTextView;
    }

    public DialogHolder(View itemView) {
        super(itemView);
        nameTextView=(TextView)itemView.findViewById(R.id.item_name_textView);
//        priceTextView=(TextView)itemView.findViewById(R.id.dialog_price_textView);
//        weightTextView=(TextView)itemView.findViewById(R.id.dialog_weight_textView);
//        mainLinearLayout=(LinearLayout)itemView.findViewById(R.id.main_linearLayout);

    }
}
