package ir.dayasoft.tavakolisaffron.holder;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.core.NormalTextView;


/**
 * Created by mohammad on 01/31/2017.
 */

public class OrderListHolder extends MainViewHolder {
    protected View rootView;
    protected NormalTextView orderListNumberTextView;
    protected NormalTextView orderListInvoiceTextView;
    protected RelativeLayout orderListInvoiceLinearLayout;
    protected LinearLayout offlinepaymentLinearLayout;
    protected NormalTextView orderListTodayDateTextView;
    protected NormalTextView orderListPriceTextView;
    protected NormalTextView orderListCountTextView;
    protected FrameLayout orderListDetailLinearLayout;
    protected NormalTextView orderListDateTextView;
    protected NormalTextView orderListReceiveDateTextView;
    protected LinearLayout orderListCompleteLinearLayout;
    protected LinearLayout orderListConfirmLinearLayout;
    protected LinearLayout orderListCheckLinearLayout;
    protected LinearLayout orderListErrorLinearLayout;
    protected LinearLayout orderListOfflinePayedLinearLayout;
    protected LinearLayout orderListItemLinearLayout;
    protected LinearLayout orderListOnlinePaymentLinearLayout;
    private TextView todayDateTextView, receiveDateTextView, countTextView, priceTextView, invoiceTextView;
    private LinearLayout completeLayout, errorLayout, checkLayout, confirmLayout, itemOrderList;

    public TextView getTodayDateTextView() {
        return todayDateTextView;
    }

    public void setTodayDateTextView(TextView todayDateTextView) {
        this.todayDateTextView = todayDateTextView;
    }

    public LinearLayout getItemOrderList() {
        return itemOrderList;
    }

    public void setItemOrderList(LinearLayout itemOrderList) {
        this.itemOrderList = itemOrderList;
    }

    public TextView getReceiveDateTextView() {
        return receiveDateTextView;
    }

    public void setReceiveDateTextView(TextView receiveDateTextView) {
        this.receiveDateTextView = receiveDateTextView;
    }

    public LinearLayout getConfirmLayout() {
        return confirmLayout;
    }

    public void setConfirmLayout(LinearLayout confirmLayout) {
        this.confirmLayout = confirmLayout;
    }

    public TextView getCountTextView() {
        return countTextView;
    }

    public void setCountTextView(TextView countTextView) {
        this.countTextView = countTextView;
    }

    public TextView getPriceTextView() {
        return priceTextView;
    }

    public void setPriceTextView(TextView priceTextView) {
        this.priceTextView = priceTextView;
    }

    public TextView getInvoiceTextView() {
        return invoiceTextView;
    }

    public void setInvoiceTextView(TextView invoiceTextView) {
        this.invoiceTextView = invoiceTextView;
    }

    public LinearLayout getCompleteLayout() {
        return completeLayout;
    }

    public void setCompleteLayout(LinearLayout completeLayout) {
        this.completeLayout = completeLayout;
    }

    public LinearLayout getErrorLayout() {
        return errorLayout;
    }

    public void setErrorLayout(LinearLayout errorLayout) {
        this.errorLayout = errorLayout;
    }

    public LinearLayout getCheckLayout() {
        return checkLayout;
    }

    public View getRootView() {
        return rootView;
    }

    public NormalTextView getOrderListNumberTextView() {
        return orderListNumberTextView;
    }

    public NormalTextView getOrderListInvoiceTextView() {
        return orderListInvoiceTextView;
    }

    public NormalTextView getOrderListTodayDateTextView() {
        return orderListTodayDateTextView;
    }

    public NormalTextView getOrderListPriceTextView() {
        return orderListPriceTextView;
    }

    public NormalTextView getOrderListCountTextView() {
        return orderListCountTextView;
    }

    public FrameLayout getOrderListDetailLinearLayout() {
        return orderListDetailLinearLayout;
    }

    public NormalTextView getOrderListDateTextView() {
        return orderListDateTextView;
    }

    public NormalTextView getOrderListReceiveDateTextView() {
        return orderListReceiveDateTextView;
    }

    public LinearLayout getOrderListCompleteLinearLayout() {
        return orderListCompleteLinearLayout;
    }

    public LinearLayout getOrderListConfirmLinearLayout() {
        return orderListConfirmLinearLayout;
    }

    public LinearLayout getOrderListCheckLinearLayout() {
        return orderListCheckLinearLayout;
    }

    public LinearLayout getOrderListErrorLinearLayout() {
        return orderListErrorLinearLayout;
    }

    public LinearLayout getOrderListOfflinePayedLinearLayout() {
        return orderListOfflinePayedLinearLayout;
    }


    public void setRootView(View rootView) {
        this.rootView = rootView;
    }

    public void setOrderListNumberTextView(NormalTextView orderListNumberTextView) {
        this.orderListNumberTextView = orderListNumberTextView;
    }

    public void setOrderListInvoiceTextView(NormalTextView orderListInvoiceTextView) {
        this.orderListInvoiceTextView = orderListInvoiceTextView;
    }

    public void setOrderListInvoiceLinearLayout(RelativeLayout orderListInvoiceLinearLayout) {
        this.orderListInvoiceLinearLayout = orderListInvoiceLinearLayout;
    }

    public void setOfflinepaymentLinearLayout(LinearLayout offlinepaymentLinearLayout) {
        this.offlinepaymentLinearLayout = offlinepaymentLinearLayout;
    }

    public void setOrderListTodayDateTextView(NormalTextView orderListTodayDateTextView) {
        this.orderListTodayDateTextView = orderListTodayDateTextView;
    }

    public void setOrderListPriceTextView(NormalTextView orderListPriceTextView) {
        this.orderListPriceTextView = orderListPriceTextView;
    }

    public void setOrderListCountTextView(NormalTextView orderListCountTextView) {
        this.orderListCountTextView = orderListCountTextView;
    }

    public void setOrderListDetailLinearLayout(FrameLayout orderListDetailLinearLayout) {
        this.orderListDetailLinearLayout = orderListDetailLinearLayout;
    }

    public void setOrderListDateTextView(NormalTextView orderListDateTextView) {
        this.orderListDateTextView = orderListDateTextView;
    }

    public void setOrderListReceiveDateTextView(NormalTextView orderListReceiveDateTextView) {
        this.orderListReceiveDateTextView = orderListReceiveDateTextView;
    }

    public void setOrderListCompleteLinearLayout(LinearLayout orderListCompleteLinearLayout) {
        this.orderListCompleteLinearLayout = orderListCompleteLinearLayout;
    }

    public void setOrderListConfirmLinearLayout(LinearLayout orderListConfirmLinearLayout) {
        this.orderListConfirmLinearLayout = orderListConfirmLinearLayout;
    }

    public LinearLayout getOrderListOnlinePaymentLinearLayout() {
        return orderListOnlinePaymentLinearLayout;
    }

    public void setOrderListOnlinePaymentLinearLayout(LinearLayout orderListOnlinePaymentLinearLayout) {
        this.orderListOnlinePaymentLinearLayout = orderListOnlinePaymentLinearLayout;
    }

    public void setOrderListCheckLinearLayout(LinearLayout orderListCheckLinearLayout) {
        this.orderListCheckLinearLayout = orderListCheckLinearLayout;

    }

    public void setOrderListErrorLinearLayout(LinearLayout orderListErrorLinearLayout) {
        this.orderListErrorLinearLayout = orderListErrorLinearLayout;
    }

    public void setOrderListOfflinePayedLinearLayout(LinearLayout orderListOfflinePayedLinearLayout) {
        this.orderListOfflinePayedLinearLayout = orderListOfflinePayedLinearLayout;
    }


    public void setOrderListItemLinearLayout(LinearLayout orderListItemLinearLayout) {
        this.orderListItemLinearLayout = orderListItemLinearLayout;
    }

    public LinearLayout getOrderListItemLinearLayout() {
        return orderListItemLinearLayout;
    }

    public RelativeLayout getOrderListInvoiceLinearLayout() {
        return orderListInvoiceLinearLayout;
    }

    public LinearLayout getOfflinepaymentLinearLayout() {
        return offlinepaymentLinearLayout;
    }

    public void setCheckLayout(LinearLayout checkLayout) {
        this.checkLayout = checkLayout;
    }

    public OrderListHolder(View itemView) {
        super(itemView);
        todayDateTextView = (TextView) itemView.findViewById(R.id.orderList_TodayDate_TextView);
        receiveDateTextView = (TextView) itemView.findViewById(R.id.orderList_receiveDate_textView);
        priceTextView = (TextView) itemView.findViewById(R.id.orderList_price_textView);
        countTextView = (TextView) itemView.findViewById(R.id.orderList_count_textView);
        invoiceTextView = (TextView) itemView.findViewById(R.id.orderList_invoice_textView);
        completeLayout = (LinearLayout) itemView.findViewById(R.id.orderList_complete_linearLayout);
        errorLayout = (LinearLayout) itemView.findViewById(R.id.orderList_error_linearLayout);
        checkLayout = (LinearLayout) itemView.findViewById(R.id.orderList_check_linearLayout);
        confirmLayout = (LinearLayout) itemView.findViewById(R.id.orderList_confirm_linearLayout);
        itemOrderList = (LinearLayout) itemView.findViewById(R.id.orderList_item_linearLayout);
        initView(itemView);


    }

    private void initView(View rootView) {
        orderListNumberTextView = (NormalTextView) rootView.findViewById(R.id.orderList_number_textView);
        orderListInvoiceTextView = (NormalTextView) rootView.findViewById(R.id.orderList_invoice_textView);
        orderListInvoiceLinearLayout = (RelativeLayout) rootView.findViewById(R.id.orderList_invoice_linearLayout);
        offlinepaymentLinearLayout = (LinearLayout) rootView.findViewById(R.id.orderList_offline_payment_linearLayout);
        orderListTodayDateTextView = (NormalTextView) rootView.findViewById(R.id.orderList_TodayDate_TextView);
        orderListPriceTextView = (NormalTextView) rootView.findViewById(R.id.orderList_price_textView);
        orderListCountTextView = (NormalTextView) rootView.findViewById(R.id.orderList_count_textView);
        orderListDetailLinearLayout = (FrameLayout) rootView.findViewById(R.id.orderList_detail_linearLayout);
        orderListDateTextView = (NormalTextView) rootView.findViewById(R.id.orderList_date_TextView);
        orderListReceiveDateTextView = (NormalTextView) rootView.findViewById(R.id.orderList_receiveDate_textView);
        orderListCompleteLinearLayout = (LinearLayout) rootView.findViewById(R.id.orderList_complete_linearLayout);
        orderListConfirmLinearLayout = (LinearLayout) rootView.findViewById(R.id.orderList_confirm_linearLayout);
        orderListCheckLinearLayout = (LinearLayout) rootView.findViewById(R.id.orderList_check_linearLayout);
        orderListErrorLinearLayout = (LinearLayout) rootView.findViewById(R.id.orderList_error_linearLayout);
        orderListItemLinearLayout = (LinearLayout) rootView.findViewById(R.id.orderList_item_linearLayout);
        orderListOnlinePaymentLinearLayout = (LinearLayout) rootView.findViewById(R.id.orderList_online_payment_linearLayout);
    }
}
