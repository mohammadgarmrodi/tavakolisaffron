package ir.dayasoft.tavakolisaffron.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import ir.dayasoft.tavakolisaffron.R;

/**
 * Created by mohammad on 01/23/2017.
 */

public class KomoriProductHolder extends MainViewHolder {
    protected View rootView;
    protected NetworkImageView komoriProductPicutreImageView;
    private LinearLayout weightLinearLayout, chooseIconLinearLayout;
    private ImageView downShape, chooseIcon;
    private ProgressBar progressBar;
    RelativeLayout iconRelativeLayout;
    private TextView toomanTextView, priceTextView, companyTextView, weightTextView, numberTextView, plusTextView, minusTextView, sumPriceTextView;

    public LinearLayout getSecondLinearLayout() {
        return secondLinearLayout;
    }

    public void setSecondLinearLayout(LinearLayout secondLinearLayout) {
        this.secondLinearLayout = secondLinearLayout;
    }

    public ImageView getChooseIcon() {
        return chooseIcon;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public void setChooseIcon(ImageView chooseIcon) {
        this.chooseIcon = chooseIcon;

    }

    private LinearLayout secondLinearLayout;

    public RelativeLayout getIconRelativeLayout() {
        return iconRelativeLayout;
    }

    public void setIconRelativeLayout(RelativeLayout iconRelativeLayout) {
        this.iconRelativeLayout = iconRelativeLayout;
    }

    LinearLayout mainLinearLayout;

    public LinearLayout getMainLinearLayout() {
        return mainLinearLayout;
    }

    public TextView getToomanTextView() {
        return toomanTextView;
    }

    public void setToomanTextView(TextView toomanTextView) {
        this.toomanTextView = toomanTextView;
    }

    public void setMainLinearLayout(LinearLayout mainLinearLayout) {
        this.mainLinearLayout = mainLinearLayout;
    }

    public ImageView getDownShape() {
        return downShape;
    }

    public LinearLayout getWeightLinearLayout() {
        return weightLinearLayout;
    }

    public void setWeightLinearLayout(LinearLayout weightLinearLayout) {
        this.weightLinearLayout = weightLinearLayout;
    }

    public void setDownShape(ImageView downShape) {
        this.downShape = downShape;
    }

    public LinearLayout getChooseIconLinearLayout() {
        return chooseIconLinearLayout;
    }

    public void setChooseIconLinearLayout(LinearLayout chooseIconLinearLayout) {
        this.chooseIconLinearLayout = chooseIconLinearLayout;
    }

    public TextView getPriceTextView() {
        return priceTextView;
    }

    public void setPriceTextView(TextView priceTextView) {
        this.priceTextView = priceTextView;
    }

    public TextView getCompanyTextView() {
        return companyTextView;
    }

    public void setCompanyTextView(TextView companyTextView) {
        this.companyTextView = companyTextView;
    }

    public TextView getWeightTextView() {
        return weightTextView;
    }

    public void setWeightTextView(TextView weightTextView) {
        this.weightTextView = weightTextView;
    }

    public TextView getNumberTextView() {
        return numberTextView;
    }

    public void setNumberTextView(TextView numberTextView) {
        this.numberTextView = numberTextView;
    }

    public TextView getPlusTextView() {
        return plusTextView;
    }

    public void setPlusTextView(TextView plusTextView) {
        this.plusTextView = plusTextView;
    }

    public TextView getMinusTextView() {
        return minusTextView;
    }

    public void setMinusTextView(TextView minusTextView) {
        this.minusTextView = minusTextView;
    }

    public TextView getSumPriceTextView() {
        return sumPriceTextView;
    }

    public void setSumPriceTextView(TextView sumPriceTextView) {
        this.sumPriceTextView = sumPriceTextView;
    }

    public NetworkImageView getKomoriProductPicutreImageView() {
        return komoriProductPicutreImageView;
    }

    public void setKomoriProductPicutreImageView(NetworkImageView komoriProductPicutreImageView) {
        this.komoriProductPicutreImageView = komoriProductPicutreImageView;
    }

    public KomoriProductHolder(View itemView) {
        super(itemView);
        initView(itemView);
        iconRelativeLayout=(RelativeLayout)itemView.findViewById(R.id.chooseProduct_relativeLayout);
        downShape = (ImageView) itemView.findViewById(R.id.komoriProduct_downShape_imageView);
        chooseIcon = (ImageView) itemView.findViewById(R.id.komoriProduct_chooseIcon_imageView);
        priceTextView = (TextView) itemView.findViewById(R.id.komoriProduct_price_textView);
        companyTextView = (TextView) itemView.findViewById(R.id.komoriProduct_name_textView);
        weightTextView = (TextView) itemView.findViewById(R.id.komoriProduct_weight_textView);
        secondLinearLayout = (LinearLayout) itemView.findViewById(R.id.komoriProduct_secondLinearLayout);
        sumPriceTextView = (TextView) itemView.findViewById(R.id.komoriProduct_sumPrice_textView);
        minusTextView = (TextView) itemView.findViewById(R.id.komoriProduct_minus_textView);
        plusTextView = (TextView) itemView.findViewById(R.id.komoriProduct_plus_textView);
        mainLinearLayout = (LinearLayout) itemView.findViewById(R.id.firstLinearLayout);
        numberTextView = (TextView) itemView.findViewById(R.id.komoriProduct_count_textView);


    }

    private void initView(View rootView) {
        komoriProductPicutreImageView = (NetworkImageView) rootView.findViewById(R.id.komoriProduct_picutre_imageView);
    }
}
