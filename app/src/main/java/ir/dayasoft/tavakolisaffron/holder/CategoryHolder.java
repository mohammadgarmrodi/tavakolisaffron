package ir.dayasoft.tavakolisaffron.holder;

import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ir.dayasoft.tavakolisaffron.R;

/**
 * Created by mohammad on 02/18/2017.
 */

public class CategoryHolder extends MainViewHolder {
    private TextView productDetailTextView;
    private CardView productDetailItem;

    public TextView getProductDetailTextView() {
        return productDetailTextView;
    }

    public void setProductDetailTextView(TextView productDetailTextView) {
        this.productDetailTextView = productDetailTextView;
    }

    public CardView getProductDetailItem() {
        return productDetailItem;
    }

    public void setProductDetailItem(CardView productDetailItem) {
        this.productDetailItem = productDetailItem;
    }

    public CategoryHolder(View itemView) {
        super(itemView);
        productDetailTextView = (TextView) itemView.findViewById(R.id.subCategory_productDetail_textView);
        productDetailItem = (CardView) itemView.findViewById(R.id.item_productDetail);

    }
}
