package ir.dayasoft.tavakolisaffron.utility;

/**
 * Created by navid on 19/04/2016.
 */
public class ContactManagement {

    static public Boolean IsValidMobileNumber (String MobileNumber)
    {
        String res="0";

        String validChar="0123456789+";


        if (MobileNumber.substring(1).contains("+"))
            return false;
        for (char c : MobileNumber.toCharArray())
        {
            if (!validChar.contains(String.valueOf(c)))
                return false;

        }

        return true;

    }

    static  public String ValidationMobileNumber (String MobileNumber)
    {
        String res="0";

        MobileNumber=MobileNumber.replace(" ","");
        MobileNumber=MobileNumber.replace("-","");
        MobileNumber=MobileNumber.replace("(","");
        MobileNumber=MobileNumber.replace(")","");

        if (MobileNumber.length()>=11)
        {
            res= "0" + MobileNumber.substring(MobileNumber.length()-10);
        }
        else if (MobileNumber.length()==10)
        {
            res= "0" + MobileNumber;
        }

        return res;

    }
}
