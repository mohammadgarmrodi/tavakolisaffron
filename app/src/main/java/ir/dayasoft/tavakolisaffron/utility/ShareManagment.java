package ir.dayasoft.tavakolisaffron.utility;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.util.ArrayList;


public class ShareManagment {


    static public void sendTo(Context context,String text ,String chooserTitle)
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent,chooserTitle));
    }

    static public void sendTo(Context context,String text ,String title ,String chooserTitle)
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
        sendIntent.putExtra(Intent.EXTRA_TITLE, title);
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent,chooserTitle));
    }

    static public void sendTo(Context context, Uri uriToImage , String chooserTitle)
    {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uriToImage);
        shareIntent.setType("image/jpeg");
        context.startActivity(Intent.createChooser(shareIntent, chooserTitle));
    }


    static public void sendTo(Context context,  ArrayList<Uri> imageUris , String chooserTitle)
    {

        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
        shareIntent.setType("image/*");
        context.startActivity(Intent.createChooser(shareIntent, chooserTitle));
    }

    static public void sendEmailTo(Context context,String email ,String subject , String text ,String chooserTitle)
    {
        Intent sendIntent = new Intent();
        sendIntent.setType("message/rfc822");
        sendIntent.putExtra(Intent.EXTRA_EMAIL  , new String[]{email});
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sendIntent.putExtra(Intent.EXTRA_TEXT   , text );
        context.startActivity(Intent.createChooser(sendIntent,chooserTitle));
    }

    static public void sendEmailTo(Context context,String[] emails ,String subject , String text ,String chooserTitle)
    {
        Intent sendIntent = new Intent();
        sendIntent.setType("message/rfc822");
        sendIntent.putExtra(Intent.EXTRA_EMAIL  , emails );
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sendIntent.putExtra(Intent.EXTRA_TEXT   , text );
        context.startActivity(Intent.createChooser(sendIntent,chooserTitle));
    }

    static public void sendEmailTo(Context context,String[] emails ,String[] cc,String[] bcc,String subject , String text ,String chooserTitle)
     {
        Intent sendIntent = new Intent();
        sendIntent.setType("message/rfc822");
        sendIntent.putExtra(Intent.EXTRA_EMAIL  , emails );
        if (cc!=null)
          sendIntent.putExtra(Intent.EXTRA_CC  , cc );
        if (bcc!=null)
            sendIntent.putExtra(Intent.EXTRA_BCC  , bcc );
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sendIntent.putExtra(Intent.EXTRA_TEXT   , text );
        context.startActivity(Intent.createChooser(sendIntent,chooserTitle));
     }

}
