package ir.dayasoft.tavakolisaffron.api;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;
import java.util.List;

import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.core.SharePref;
import ir.dayasoft.tavakolisaffron.model.Category;
import ir.dayasoft.tavakolisaffron.model.KomoriProduct;
import ir.dayasoft.tavakolisaffron.model.Result;
import ir.dayasoft.tavakolisaffron.model.User;
import ir.dayasoft.tavakolisaffron.volly.CustomStringRequest;
import ir.dayasoft.tavakolisaffron.volly.MySingleton;

/**
 * Created by mohammad on 01/26/2017.
 */

public class CategoryApi {

    static final String REST_RESOURCE_NAME = "Category";
    static final String REST_Category = REST_RESOURCE_NAME + "/Get?offset=%s&pageSize=%s&updateDate=%s";
    String restApiPostfixUrl;
    RestApiLifeCycle restApiLifeCycle;
    Integer offset;
    Integer pageSize;
    String updateDate;
    private String userID;


    public CategoryApi(RestApiLifeCycle restApiLifeCycle, Context context) {
        this.restApiLifeCycle = restApiLifeCycle;
        this.restApiPostfixUrl = Constants.RestAPIPostfix;
        userID = String.valueOf(SharePref.getUserId(context));

    }

    public static String getRestResourceName() {
        return REST_RESOURCE_NAME;
    }

    public String getRestApiPostfixUrl() {
        return restApiPostfixUrl;
    }

    public void setRestApiPostfixUrl(String restApiPostfixUrl) {
        this.restApiPostfixUrl = restApiPostfixUrl;
    }

    public static String getREST_Category() {
        return REST_Category;
    }

    public RestApiLifeCycle getRestApiLifeCycle() {
        return restApiLifeCycle;
    }

    public void setRestApiLifeCycle(RestApiLifeCycle restApiLifeCycle) {
        this.restApiLifeCycle = restApiLifeCycle;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate.replace(" ", "T");
    }

    static public Category jsonToCategoryObject(String response) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return Arrays.asList(gson.fromJson(response, Category.class)).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public List<Category> jsonToCategoryObjectArray(String response) {
        try {

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.serializeNulls().create();
            return Arrays.asList(gson.fromJson(response, Category[].class));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int GetCategory(final Context context) {//getAll

        String url = restApiPostfixUrl + REST_Category;
        url = String.format(url, offset, pageSize, updateDate);
        RequestCategory(context, url);

        return 1;
    }

    private void RequestCategory(final Context context, final String url) {
        CustomStringRequest customRequest = new CustomStringRequest(Request.Method.GET, url, userID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Result result = Result.jsonToObject(response.toString());
                    if (!result.getError()) {
                        List<Category> categoryList = jsonToCategoryObjectArray(result.getValue());
                        Category category = new Category();
                        category.Insert(context, categoryList);
                        if (categoryList.size() > 0 && categoryList.size() >= pageSize) {
                            offset += pageSize;
                            GetCategory(context);
                        } else {
                            restApiLifeCycle.feedback(RestApiLifeCycle.STEP_COMPELETE, "", RestApiLifeCycle.STEP_COMPLETE_CATEGORY);
                        }
                    } else {
                        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", RestApiLifeCycle.STEP_ERROR_CATEGORY);
                    }
                } catch (Exception error) {

                    restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", RestApiLifeCycle.STEP_ERROR_CATEGORY);
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", RestApiLifeCycle.STEP_ERROR_CATEGORY);

            }
        });
        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ADD_QUEUE, "", RestApiLifeCycle.STEP_ADD_QUEUE_CATEGORY);
        MySingleton.getInstance(context).addToRequestQueue(customRequest);

    }

}
