package ir.dayasoft.tavakolisaffron.api;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;
import java.util.List;

import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.core.SharePref;
import ir.dayasoft.tavakolisaffron.model.Product;
import ir.dayasoft.tavakolisaffron.model.Result;
import ir.dayasoft.tavakolisaffron.volly.CustomStringRequest;
import ir.dayasoft.tavakolisaffron.volly.MySingleton;

/**
 * Created by mohammad on 02/20/2017.
 */

public class PaymentMethodApi {
    static final String REST_RESOURCE_NAME = "PaymentMethod";
    static final String REST_PaymentMethod = REST_RESOURCE_NAME + "/Get?updateDate=%s";
    String restApiPostfixUrl;
    RestApiLifeCycle restApiLifeCycle;
    String updateDate;
    private String userID;

    public PaymentMethodApi(RestApiLifeCycle restApiLifeCycle, Context context) {
        this.restApiLifeCycle = restApiLifeCycle;
        this.restApiPostfixUrl = Constants.RestAPIPostfix;
        userID = String.valueOf(SharePref.getUserId(context));

    }

    public static String getRestResourceName() {
        return REST_RESOURCE_NAME;
    }

    public static String getREST_PaymentMethod() {
        return REST_PaymentMethod;
    }

    public String getRestApiPostfixUrl() {
        return restApiPostfixUrl;
    }

    public void setRestApiPostfixUrl(String restApiPostfixUrl) {
        this.restApiPostfixUrl = restApiPostfixUrl;
    }

    public RestApiLifeCycle getRestApiLifeCycle() {
        return restApiLifeCycle;
    }

    public void setRestApiLifeCycle(RestApiLifeCycle restApiLifeCycle) {
        this.restApiLifeCycle = restApiLifeCycle;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate.replace(" ", "T");
    }

    public int GetPaymentMethod(final Context context) {//getAll

        String url = restApiPostfixUrl + REST_PaymentMethod;
        url = String.format(url, updateDate);
        RequestProduct(context, url);

        return 1;
    }

    private void RequestProduct(final Context context, final String url) {
        CustomStringRequest customRequest = new CustomStringRequest(Request.Method.GET, url, userID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Result result = Result.jsonToObject(response.toString());
                    if (!result.getError()) {
                        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_COMPELETE, result.getValue(), 1);
                    }
                    else {
                        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
                    }
                } catch (Exception error) {

                    restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);

            }
        });
        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ADD_QUEUE, "", 1);
        MySingleton.getInstance(context).addToRequestQueue(customRequest);

    }


}
