package ir.dayasoft.tavakolisaffron.api;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.core.SharePref;
import ir.dayasoft.tavakolisaffron.model.Invoice;
import ir.dayasoft.tavakolisaffron.model.InvoiceItem;
import ir.dayasoft.tavakolisaffron.model.Result;
import ir.dayasoft.tavakolisaffron.model.SyncObject;
import ir.dayasoft.tavakolisaffron.volly.CustomJsonObjectRequest;
import ir.dayasoft.tavakolisaffron.volly.CustomStringRequest;
import ir.dayasoft.tavakolisaffron.volly.MySingleton;

/**
 * Created by mohammad on 01/26/2017.
 */

public class InvoiceApi {


    static final String REST_RESOURCE_NAME = "Invoice";
    static final String REST_Invoice = REST_RESOURCE_NAME + "/Get?offset=%s&pageSize=%s&updateDate=%s";
    static final String REST_POST = REST_RESOURCE_NAME + "/Add";
    String restApiPostfixUrl;
    RestApiLifeCycle restApiLifeCycle;
    Integer offset;
    Integer pageSize;
    String updateDate;
    private String userID;

    public static final Integer STEP_Invoice_COMPLETE = 11;


    public InvoiceApi(RestApiLifeCycle restApiLifeCycle, Context context) {
        this.restApiLifeCycle = restApiLifeCycle;
        this.restApiPostfixUrl = Constants.RestAPIPostfix;
        userID = String.valueOf(SharePref.getUserId(context));
    }

    public static String getRestResourceName() {
        return REST_RESOURCE_NAME;
    }

    public String getRestApiPostfixUrl() {
        return restApiPostfixUrl;
    }

    public void setRestApiPostfixUrl(String restApiPostfixUrl) {
        this.restApiPostfixUrl = restApiPostfixUrl;
    }

    public static String getREST_Invoice() {
        return REST_Invoice;
    }

    public RestApiLifeCycle getRestApiLifeCycle() {
        return restApiLifeCycle;
    }

    public void setRestApiLifeCycle(RestApiLifeCycle restApiLifeCycle) {
        this.restApiLifeCycle = restApiLifeCycle;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate.replace(" ", "T");
    }

    static public Invoice jsonToInvoiceObject(String response) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return Arrays.asList(gson.fromJson(response, Invoice.class)).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public SyncObject jsonToSyncObject(String response) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return Arrays.asList(gson.fromJson(response, SyncObject.class)).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public List<Invoice> jsonToInvoiceObjectArray(String response) {
        try {

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.serializeNulls().create();
            return Arrays.asList(gson.fromJson(response, Invoice[].class));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int GetInvoice(final Context context) {//getAll

        String url = restApiPostfixUrl + REST_Invoice;
        url = String.format(url, offset, pageSize, updateDate);
        RequestInvoice(context, url);

        return 1;
    }

    public int PostInvoice(final Context context, Invoice invoice) {//postAll

        String url = restApiPostfixUrl + REST_POST;
        url = String.format(url);
        RequestPostAttendance(context, url, invoice);

        return 1;
    }

    static public String InvoiceObjectToJson(Invoice invoice) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return gson.toJson(invoice);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void RequestInvoice(final Context context, final String url) {
        CustomStringRequest customRequest = new CustomStringRequest(Request.Method.GET, url, userID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Result result = Result.jsonToObject(response.toString());
                    if (!result.getError()) {
                        List<Invoice> invoiceList = jsonToInvoiceObjectArray(result.getValue());
                        Invoice invoice = new Invoice();


                        for(Invoice invoice1 : invoiceList){
                            if(invoice1.getInvoiceItem()!=null) {

                                invoice1.setInvoiceItemPaymentStatus(invoice1.getInvoiceItem().getPaymentStatus());
                                invoice1.setInvoiceItemStatus(invoice1.getInvoiceItem().getStatus());


                            }

                            invoice.Insert(context,invoice1);

                        }


                        if (invoiceList.size() > 0 && invoiceList.size() >= pageSize) {

                            offset += pageSize;
                            GetInvoice(context);

                        } else {
                             restApiLifeCycle.feedback(RestApiLifeCycle.STEP_COMPELETE, "", RestApiLifeCycle.STEP_COMPLETE_INVOICE);

                        }
                    }
                    else {
                        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", RestApiLifeCycle.STEP_ERROR_INVOICE);
                    }
                } catch (Exception error) {

                    restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", RestApiLifeCycle.STEP_ERROR_INVOICE);
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", RestApiLifeCycle.STEP_ERROR_INVOICE);

            }
        });
        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ADD_QUEUE, "", 1);
        MySingleton.getInstance(context).addToRequestQueue(customRequest);

    }

    private void RequestPostAttendance(final Context context, final String url, Invoice invoice) {

        CustomJsonObjectRequest customRequest = new CustomJsonObjectRequest(Request.Method.POST, url, userID, InvoiceObjectToJson(invoice), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    Result result = Result.jsonToObject(response.toString());
                    if (!result.getError()) {
                        SyncObject syncObject = jsonToSyncObject(result.getValue());
                        restApiLifeCycle.passSyncObject(RestApiLifeCycle.STEP_COMPELETE, syncObject, RestApiLifeCycle.STEP_COMPLETE_INVOICE);

                    } else {
                        if(result.getCode()==0){
                            restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR,"" , RestApiLifeCycle.STEP_ERROR_INVOICE);
                        }
                        else{
                            Type type = new TypeToken<List<String>>(){}.getType();
                            Gson gsonReceiver = new Gson();
                            List<String> obj = gsonReceiver.fromJson(result.getValue(),type);
                            restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, obj, RestApiLifeCycle.STEP_ERROR_CHECKSTOCK);
                        }
                    }

                } catch (Exception error) {

                    restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
            }
        });
        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ADD_QUEUE, "", 1);
        MySingleton.getInstance(context).addToRequestQueue(customRequest);
    }


}
