package ir.dayasoft.tavakolisaffron.api;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.core.SharePref;
import ir.dayasoft.tavakolisaffron.model.Invoice;
import ir.dayasoft.tavakolisaffron.model.InvoiceItem;
import ir.dayasoft.tavakolisaffron.model.Result;
import ir.dayasoft.tavakolisaffron.model.SyncObject;
import ir.dayasoft.tavakolisaffron.model.User;
import ir.dayasoft.tavakolisaffron.volly.CustomJsonObjectRequest;
import ir.dayasoft.tavakolisaffron.volly.CustomStringRequest;
import ir.dayasoft.tavakolisaffron.volly.MySingleton;

/**
 * Created by mohammad on 02/07/2017.
 */

public class InvoiceItemApi {
    static final String REST_RESOURCE_NAME = "InvoiceItem";
    static final String REST_InvoiceItem = REST_RESOURCE_NAME + "/Get?offset=%s&pageSize=%s&updateDate=%s";
    static final String REST_POST = REST_RESOURCE_NAME + "/Add";

    String restApiPostfixUrl;
    RestApiLifeCycle restApiLifeCycle;
    Integer offset;
    Integer pageSize;
    String updateDate;
    private String userID;

    public InvoiceItemApi(RestApiLifeCycle restApiLifeCycle, Context context) {
        this.restApiLifeCycle = restApiLifeCycle;
        this.restApiPostfixUrl = Constants.RestAPIPostfix;
        userID = String.valueOf(SharePref.getUserId(context));

    }

    public static String getRestResourceName() {
        return REST_RESOURCE_NAME;
    }

    public static String getREST_InvoiceItem() {
        return REST_InvoiceItem;
    }

    public String getRestApiPostfixUrl() {
        return restApiPostfixUrl;
    }

    public void setRestApiPostfixUrl(String restApiPostfixUrl) {
        this.restApiPostfixUrl = restApiPostfixUrl;
    }

    public RestApiLifeCycle getRestApiLifeCycle() {
        return restApiLifeCycle;
    }

    public void setRestApiLifeCycle(RestApiLifeCycle restApiLifeCycle) {
        this.restApiLifeCycle = restApiLifeCycle;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate.replace(" ", "T");
    }

    static public InvoiceItem jsonToInvoiceItemObject(String response) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return Arrays.asList(gson.fromJson(response, InvoiceItem.class)).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public SyncObject jsonToSyncObject(String response) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return Arrays.asList(gson.fromJson(response, SyncObject.class)).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public List<InvoiceItem> jsonToInvoiceItemObjectArray(String response) {
        try {

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.serializeNulls().create();
            return Arrays.asList(gson.fromJson(response, InvoiceItem[].class));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int GetInvoiceItem(final Context context) {//getAll

        String url = restApiPostfixUrl + REST_InvoiceItem;
        url = String.format(url, offset, pageSize, updateDate);
        RequestInvoiceItem(context, url);

        return 1;
    }

    public int PostIInvoiceItem(final Context context, InvoiceItem invoiceItem) {//postAll

        String url = restApiPostfixUrl + REST_POST;
        url = String.format(url);
        RequestPostAttendance(context, url, invoiceItem);

        return 1;
    }

    static public String InvoiceItemObjectToJson(Invoice invoice) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return gson.toJson(invoice);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public String InvoiceObjectToJson(InvoiceItem invoiceItem) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return gson.toJson(invoiceItem);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void RequestInvoiceItem(final Context context, final String url) {
        CustomStringRequest customRequest = new CustomStringRequest(Request.Method.GET, url, userID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Result result = Result.jsonToObject(response.toString());
                    if (!result.getError()) {
                        List<InvoiceItem> invoiceItemList = jsonToInvoiceItemObjectArray(result.getValue());
                        InvoiceItem invoiceItem = new InvoiceItem();
                        invoiceItem.Insert(context, invoiceItemList);

                        if (invoiceItemList.size() > 0 && invoiceItemList.size() >= pageSize) {

                            offset += pageSize;
                            GetInvoiceItem(context);

                            return;
                        } else {

                            restApiLifeCycle.feedback(RestApiLifeCycle.STEP_COMPELETE, "", 1);
                            return;
                        }
                    }
                } catch (Exception error) {

                    restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);

            }
        });
        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ADD_QUEUE, "", 1);
        MySingleton.getInstance(context).addToRequestQueue(customRequest);

    }

    private void RequestPostAttendance(final Context context, final String url, InvoiceItem invoiceItem) {

        CustomJsonObjectRequest customRequest = new CustomJsonObjectRequest(Request.Method.POST, url,userID, InvoiceObjectToJson(invoiceItem), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    Result result = Result.jsonToObject(response.toString());
                    if (!result.getError()) {
                        SyncObject syncObject = jsonToSyncObject(result.getValue());
                        restApiLifeCycle.passSyncObject(RestApiLifeCycle.STEP_COMPELETE, syncObject, RestApiLifeCycle.STEP_COMPLETE_INVOICE_ITEM);

                    } else {
                        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", RestApiLifeCycle.STEP_ERROR_INVOICE_ITEM);
                    }

                } catch (Exception error) {

                    restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
            }
        });
        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ADD_QUEUE, "", 1);
        MySingleton.getInstance(context).addToRequestQueue(customRequest);
    }


}
