package ir.dayasoft.tavakolisaffron.api;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.core.SharePref;
import ir.dayasoft.tavakolisaffron.model.Category;
import ir.dayasoft.tavakolisaffron.model.Invoice;
import ir.dayasoft.tavakolisaffron.model.Product;
import ir.dayasoft.tavakolisaffron.model.Result;
import ir.dayasoft.tavakolisaffron.model.User;
import ir.dayasoft.tavakolisaffron.volly.CustomJsonObjectRequest;
import ir.dayasoft.tavakolisaffron.volly.CustomStringRequest;
import ir.dayasoft.tavakolisaffron.volly.MySingleton;

/**
 * Created by mohammad on 02/02/2017.
 */

public class UserApi {
    static final String REST_RESOURCE_NAME = "User";
    static final String REST_Login = REST_RESOURCE_NAME + "/Login?username=%s&password=%s";
    static final String REST_CheckToken = REST_RESOURCE_NAME + "/CheckToken?userID=%s&token=%s";
    static final String REST_GET = REST_RESOURCE_NAME + "/Get?username=%s&password=%s";
    static final String REST_POST = REST_RESOURCE_NAME + "/add";
    String restApiPostfixUrl;
    RestApiLifeCycle restApiLifeCycle;
    String user, password, key, token, userID;


    public UserApi(RestApiLifeCycle restApiLifeCycle, Context context) {
        this.restApiLifeCycle = restApiLifeCycle;
        this.restApiPostfixUrl = Constants.RestAPIPostfix;
        userID = String.valueOf(SharePref.getUserId(context));

    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public static String getREST_Login() {
        return REST_Login;
    }

    public static String getRestGet() {
        return REST_GET;
    }

    public static String getREST_CheckToken() {
        return REST_CheckToken;
    }

    public static String getRestPost() {
        return REST_POST;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


    public static String getRestResourceName() {
        return REST_RESOURCE_NAME;
    }

    public static String getREST_Get() {
        return REST_GET;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRestApiPostfixUrl() {
        return restApiPostfixUrl;
    }

    public void setRestApiPostfixUrl(String restApiPostfixUrl) {
        this.restApiPostfixUrl = restApiPostfixUrl;
    }

    public RestApiLifeCycle getRestApiLifeCycle() {
        return restApiLifeCycle;
    }

    public void setRestApiLifeCycle(RestApiLifeCycle restApiLifeCycle) {
        this.restApiLifeCycle = restApiLifeCycle;
    }

    static public User jsonToUserObject(String response) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return Arrays.asList(gson.fromJson(response, User.class)).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public List<User> jsonToUserObjectArray(String response) {
        try {

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.serializeNulls().create();
            return Arrays.asList(gson.fromJson(response, User[].class));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int GetUser(final Context context) {//getAll

        String url = restApiPostfixUrl + REST_GET;
        url = String.format(url, user, password);
        RequestUser(context, url);

        return 1;
    }

    public int GetLogin(final Context context) {//getAll

        String url = restApiPostfixUrl + REST_Login;
        url = String.format(url, user, password);
        RequestLogin(context, url);

        return 1;
    }

    public int GetCheckToken(final Context context) {//getAll

        String url = restApiPostfixUrl + REST_CheckToken;
        url = String.format(url, userID, token);
        RequestCheckToken(context, url);

        return 1;
    }

    public int PostUser(final Context context, User user) {//postAll

        String url = restApiPostfixUrl + REST_POST;
        url = String.format(url);
        RequestPostAttendance(context, url, user);

        return 1;
    }

    static public String UserObjectToJson(User user) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return gson.toJson(user);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void RequestUser(final Context context, final String url) {
        CustomStringRequest customRequest = new CustomStringRequest(Request.Method.GET, url, userID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Result result = Result.jsonToObject(response.toString());
                    if (!result.getError()) {
                    }
                } catch (Exception error) {

                    restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);

            }
        });
        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ADD_QUEUE, "", 1);
        MySingleton.getInstance(context).addToRequestQueue(customRequest);

    }

    private void RequestCheckToken(final Context context, final String url) {
        CustomStringRequest customRequest = new CustomStringRequest(Request.Method.GET, url, userID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            Result result = Result.jsonToObject(response.toString());
                            if (!result.getError()) {
                                User user = jsonToUserObject(result.getValue());
                                Object object = new Object();
                                restApiLifeCycle.passUser(RestApiLifeCycle.STEP_COMPELETE, user, 1);

                            }
                        } catch (Exception error) {

                            restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
                            Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);

            }
        });
        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ADD_QUEUE, "", 1);
        MySingleton.getInstance(context).addToRequestQueue(customRequest);

    }

    private void RequestLogin(final Context context, final String url) {
        CustomStringRequest customRequest = new CustomStringRequest(Request.Method.GET, url, userID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Result result = Result.jsonToObject(response.toString());
                    if (!result.getError()) {

                        User user = jsonToUserObject(result.getValue());
                        user.Insert(context, user);
                        restApiLifeCycle.passUser(RestApiLifeCycle.STEP_COMPELETE, user, RestApiLifeCycle.STEP_COMPLETE_LOGIN);
                        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_COMPELETE, result.getMessage(), RestApiLifeCycle.STEP_COMPLETE_LOGIN);
                    } else {
                        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, result.getCode(), RestApiLifeCycle.STEP_ERROR_LOGIN);
                    }


                } catch (Exception error) {

                    restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);

            }
        });
        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ADD_QUEUE, "", 1);
        MySingleton.getInstance(context).addToRequestQueue(customRequest);

    }

    private void RequestPostAttendance(final Context context, final String url, final User user) {

        CustomJsonObjectRequest customRequest = new CustomJsonObjectRequest(Request.Method.POST, url, userID, UserObjectToJson(user), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    Result result = Result.jsonToObject(response.toString());
                    if (!result.getError()) {
                        User user = jsonToUserObject(result.getValue());
                        user.Insert(context, user);
                        SharePref.setLoginStep(context, User.STEP_FINISH_LOGIN);
                        restApiLifeCycle.passUser(RestApiLifeCycle.STEP_COMPELETE,user, 1);
                    } else {
                        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, result.getCode(), 1);
                    }

                } catch (Exception error) {

                    restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
            }
        });
        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ADD_QUEUE, "", 1);
        MySingleton.getInstance(context).addToRequestQueue(customRequest);
    }


}
