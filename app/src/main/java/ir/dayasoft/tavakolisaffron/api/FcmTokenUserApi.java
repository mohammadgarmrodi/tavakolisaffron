package ir.dayasoft.tavakolisaffron.api;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.core.SharePref;
import ir.dayasoft.tavakolisaffron.model.FcmTokenUser;
import ir.dayasoft.tavakolisaffron.model.Result;
import ir.dayasoft.tavakolisaffron.model.SyncObject;
import ir.dayasoft.tavakolisaffron.volly.CustomJsonObjectRequest;
import ir.dayasoft.tavakolisaffron.volly.MySingleton;

/**
 * Created by mohammad on 02/18/2017.
 */

public class FcmTokenUserApi {
    static final String REST_RESOURCE_NAME = "FcmToken";
    static final String REST_FcmTokenUser = REST_RESOURCE_NAME + "/Add";
    String restApiPostfixUrl;
    RestApiLifeCycle restApiLifeCycle;
    private String userID;


    public FcmTokenUserApi(RestApiLifeCycle restApiLifeCycle, Context context) {
        this.restApiLifeCycle = restApiLifeCycle;
        this.restApiPostfixUrl = Constants.RestAPIPostfix;
        userID = String.valueOf(SharePref.getUserId(context));

    }

    public static String getRestResourceName() {
        return REST_RESOURCE_NAME;
    }

    public String getRestApiPostfixUrl() {
        return restApiPostfixUrl;
    }

    public void setRestApiPostfixUrl(String restApiPostfixUrl) {
        this.restApiPostfixUrl = restApiPostfixUrl;
    }

    public static String getREST_FcmToken() {
        return REST_FcmTokenUser;
    }

    public RestApiLifeCycle getRestApiLifeCycle() {
        return restApiLifeCycle;
    }

    public void setRestApiLifeCycle(RestApiLifeCycle restApiLifeCycle) {
        this.restApiLifeCycle = restApiLifeCycle;
    }

    static public FcmTokenUser jsonToFcmTokenUserObject(String response) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return Arrays.asList(gson.fromJson(response, FcmTokenUser.class)).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public List<FcmTokenUser> jsonToFcmTokenUserObjectArray(String response) {
        try {

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.serializeNulls().create();
            return Arrays.asList(gson.fromJson(response, FcmTokenUser[].class));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int PostFcmTokenUser(final Context context, FcmTokenUser fcmTokenUser) {//postAll

        String url = restApiPostfixUrl + REST_FcmTokenUser;
        url = String.format(url);
        RequestPostAttendance(context, url, fcmTokenUser);

        return 1;
    }

    static public String FcmTokenUserObjectToJson(FcmTokenUser fcmTokenUser) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return gson.toJson(fcmTokenUser);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public SyncObject jsonToSyncObject(String response) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return Arrays.asList(gson.fromJson(response, SyncObject.class)).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void RequestPostAttendance(final Context context, final String url, final FcmTokenUser fcmTokenUser) {

        CustomJsonObjectRequest customRequest = new CustomJsonObjectRequest(Request.Method.POST, url, userID, FcmTokenUserObjectToJson(fcmTokenUser), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    Result result = Result.jsonToObject(response.toString());
                    if (!result.getError()) {
                        SyncObject syncObject = jsonToSyncObject(result.getValue());
                        restApiLifeCycle.passSyncObject(RestApiLifeCycle.STEP_COMPELETE, syncObject,RestApiLifeCycle.STEP_COMPLETE_FCMTOKEN);
                    } else {
                        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, result.getCode(), RestApiLifeCycle.STEP_COMPLETE_FCMTOKEN);
                    }

                } catch (Exception error) {

                    restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
            }
        });
        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ADD_QUEUE, "", 1);
        MySingleton.getInstance(context).addToRequestQueue(customRequest);
    }
}