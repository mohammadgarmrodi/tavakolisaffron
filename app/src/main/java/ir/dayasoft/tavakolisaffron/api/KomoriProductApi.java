package ir.dayasoft.tavakolisaffron.api;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.core.SharePref;
import ir.dayasoft.tavakolisaffron.model.KomoriProduct;
import ir.dayasoft.tavakolisaffron.model.Result;
import ir.dayasoft.tavakolisaffron.volly.CustomJsonObjectRequest;
import ir.dayasoft.tavakolisaffron.volly.CustomStringRequest;
import ir.dayasoft.tavakolisaffron.volly.MySingleton;

/**
 * Created by mohammad on 01/24/2017.
 */

public class KomoriProductApi {

    public static final Integer STEP_KomoriProductAPI_COMPLETE = 10;

    static final String REST_RESOURCE_NAME = "KomoriProduct";
    static final String REST_KomoriProduct = REST_RESOURCE_NAME + "/Get?offset=%s&pageSize=%s&updateDate=%s";
    static final String REST_CheckStock = REST_RESOURCE_NAME + "/CheckStock";
    String restApiPostfixUrl;
    RestApiLifeCycle restApiLifeCycle;
    Integer offset;
    Integer pageSize;
    private String userID;
    String updateDate;


    public KomoriProductApi(RestApiLifeCycle restApiLifeCycle, Context context) {
        this.restApiLifeCycle = restApiLifeCycle;
        this.restApiPostfixUrl = Constants.RestAPIPostfix;
        userID = String.valueOf(SharePref.getUserId(context));

    }

    public static String getRestResourceName() {
        return REST_RESOURCE_NAME;
    }

    public static String getREST_Category() {
        return REST_KomoriProduct;
    }

    public String getRestApiPostfixUrl() {
        return restApiPostfixUrl;
    }

    public void setRestApiPostfixUrl(String restApiPostfixUrl) {
        this.restApiPostfixUrl = restApiPostfixUrl;
    }

    public RestApiLifeCycle getRestApiLifeCycle() {
        return restApiLifeCycle;
    }

    public void setRestApiLifeCycle(RestApiLifeCycle restApiLifeCycle) {
        this.restApiLifeCycle = restApiLifeCycle;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate.replace(" ", "T");
    }

    static public KomoriProduct jsonToKomoriProductObject(String response) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return Arrays.asList(gson.fromJson(response, KomoriProduct.class)).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public List<KomoriProduct> jsonToKomoriProductObjectArray(String response) {
        try {

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.serializeNulls().create();
            return Arrays.asList(gson.fromJson(response, KomoriProduct[].class));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int GetKomoriProduct(final Context context) {//getAll

        String url = restApiPostfixUrl + REST_KomoriProduct;
        url = String.format(url, offset, pageSize, updateDate);
        RequestKomoriProduct(context, url);

        return 1;
    }

    public int PostCheckStock(final Context context, KomoriProduct komoriProduct) {//postAll

        String url = restApiPostfixUrl + REST_CheckStock;
        url = String.format(url);
        RequestPostAttendance(context, url, komoriProduct);

        return 1;
    }

    static public String CheckStockObjectToJson(KomoriProduct komoriProduct) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return gson.toJson(komoriProduct);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void RequestKomoriProduct(final Context context, final String url) {
        CustomStringRequest customRequest = new CustomStringRequest(Request.Method.GET, url, userID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Result result = Result.jsonToObject(response.toString());
                    if (!result.getError()) {
                        List<KomoriProduct> komoriProductList = jsonToKomoriProductObjectArray(result.getValue());
                        KomoriProduct komoriProduct = new KomoriProduct();
                        komoriProduct.Insert(context, komoriProductList);

                        if (komoriProductList.size() > 0 && komoriProductList.size() >= pageSize) {

                            offset += pageSize;
                            GetKomoriProduct(context);

                            return;
                        } else {

                            restApiLifeCycle.feedback(RestApiLifeCycle.STEP_COMPELETE, "", STEP_KomoriProductAPI_COMPLETE);
                            return;
                        }
                    }
                } catch (Exception error) {

                    restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);

            }
        });
        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ADD_QUEUE, "", 1);
        MySingleton.getInstance(context).addToRequestQueue(customRequest);

    }

    private void RequestPostAttendance(final Context context, final String url, KomoriProduct komoriProduct) {

        CustomJsonObjectRequest customRequest = new CustomJsonObjectRequest(Request.Method.POST, url, userID, CheckStockObjectToJson(komoriProduct), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    Result result = Result.jsonToObject(response.toString());
                    if (!result.getError()) {
                        if (result.getCode() == 1) {
                            restApiLifeCycle.feedback(RestApiLifeCycle.STEP_COMPELETE, result.getValue(), RestApiLifeCycle.STEP_COMPLETE_CHECKSTOCK);
                        } else if (result.getCode() == 2) {
                            Type type = new TypeToken<List<String>>(){}.getType();
                            Gson gsonReceiver = new Gson();
                            List<String> obj = gsonReceiver.fromJson(result.getValue(),type);
                            restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, obj, RestApiLifeCycle.STEP_ERROR_CHECKSTOCK);
                        }
                    } else {
                        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
                    }

                } catch (Exception error) {

                    restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ERROR, "", 1);
            }
        });
        restApiLifeCycle.feedback(RestApiLifeCycle.STEP_ADD_QUEUE, "", 1);
        MySingleton.getInstance(context).addToRequestQueue(customRequest);
    }


}



