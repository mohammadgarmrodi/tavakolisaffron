package ir.dayasoft.tavakolisaffron.api;

import java.util.List;
import java.util.Objects;

import ir.dayasoft.tavakolisaffron.model.KomoriProduct;
import ir.dayasoft.tavakolisaffron.model.SyncObject;
import ir.dayasoft.tavakolisaffron.model.User;

/**
 * Created by mohammad on 12/26/2016.
 */

public interface RestApiLifeCycle {
    static final int STEP_COMPLETE_FCMTOKEN = 25;
    static final int STEP_ERROR_FCMTOKEN = 26;
    static final int STEP_ADD_QUEUE_FCMTOKEN = 27;
    static final int STEP_START_FCMTOKEN = 28;

    static final int STEP_COMPLETE_LOGIN = 30;
    static final int STEP_ERROR_LOGIN = 31;
    static final int STEP_ADD_QUEUE_LOGIN = 32;
    static final int STEP_START_LOGIN = 33;

    static final int STEP_COMPLETE_CHECKSTOCK= 35;
    static final int STEP_ERROR_CHECKSTOCK= 36;
    static final int STEP_ADD_QUEUE_CHECKSTOCK= 37;
    static final int STEP_START_CHECKSTOCK= 38;

    static final int STEP_COMPLETE_CATEGORY= 40;
    static final int STEP_ERROR_CATEGORY= 41;
    static final int STEP_ADD_QUEUE_CATEGORY= 42;
    static final int STEP_START_CATEGORY= 43;


    static final int STEP_COMPLETE_INVOICE = 20;
    static final int STEP_ERROR_INVOICE = 21;
    static final int STEP_ADD_QUEUE_INVOICE = 22;
    static final int STEP_START_INVOICE = 23;


    static final int STEP_COMPLETE_INVOICE_PRODUCT = 15;
    static final int STEP_COMPLETE_ERROR_PRODUCT = 16;
    static final int STEP_ADD_QUEUE_INVOICE_PRODUCT = 17;
    static final int STEP_START_INVOICE_PRODUCT = 18;


    static final int STEP_COMPLETE_INVOICE_ITEM = 10;
    static final int STEP_ERROR_INVOICE_ITEM = 11;
    static final int STEP_ADD_QUEUE_INVOICE_ITEM = 12;
    static final int STEP_START_INVOICE_ITEM = 13;

    static final int STEP_ADD_QUEUE = 1;
    static final int STEP_START = 2;
    static final int STEP_COMPELETE = 3;
    static final int STEP_ERROR = 4;

    public abstract void feedback(Integer type, Object object, int section);
    public abstract void passUser(Integer type, User user, int section);
    public abstract void passSyncObject(Integer type, SyncObject syncObject, int section);


}
