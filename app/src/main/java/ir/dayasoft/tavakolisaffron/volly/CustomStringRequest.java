package ir.dayasoft.tavakolisaffron.volly;


import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;


public class CustomStringRequest extends  StringRequest {

    String UserID;


    public CustomStringRequest(String url,String userID, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
        this.UserID=userID;
    }

    public CustomStringRequest(int method, String url,String userID, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        this.UserID=userID;

    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String,String> hashMap = new HashMap<>();
//        hashMap.putAll(super.getHeaders());
        //
        hashMap.put("key","FDSDASDFVVSDSVVVDVVCfsd24231234");
        hashMap.put("userID",UserID);


        return hashMap;
    }

}
