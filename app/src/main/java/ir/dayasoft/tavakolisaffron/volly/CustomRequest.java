package ir.dayasoft.tavakolisaffron.volly;


import com.android.volley.AuthFailureError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CustomRequest extends JsonArrayRequest {


    public CustomRequest(int method,
                         String url,
                         String objectList,
                         Listener<JSONArray> listener,
                         ErrorListener errorListener) {

        super(method, url, objectList, listener, errorListener);
    }


    public String makeJsonObject(Object obj) {
        Gson gson = new Gson();
        return gson.toJson(obj);

    }

    public String makeJsonObject(List<Object> objs) {
        Gson gson = new Gson();
        return gson.toJson(objs);

    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.putAll(super.getHeaders());
        //   hashMap.put("user","hamedi");
        return hashMap;
    }

}