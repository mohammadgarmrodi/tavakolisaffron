package ir.dayasoft.tavakolisaffron.volly;


import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dayasoft on 11/2/16.
 */

public class CustomJsonObjectRequest extends JsonObjectRequest {

    String UserID;


    public CustomJsonObjectRequest(int method, String url,String userID, String requestBody
            , Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
        this.UserID=userID;

    }

    public CustomJsonObjectRequest(int method, String url,String userID,
                                   Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        this.UserID=userID;

    }

    public CustomJsonObjectRequest(String url,String userID, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
        this.UserID=userID;

    }

    /*public CustomJsonObjectRequest(int method, String url, String userId,
                                  String token, JSONArray jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);

        this.UserId=userId;
        this.Token=token;
    }*/

    public CustomJsonObjectRequest(int method, String url,String userID, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
        this.UserID=userID;

    }

   /* public CustomJsonObjectRequest(String url,  String userId,
                                  String token,JSONArray jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, jsonRequest, listener, errorListener);

        this.UserId=userId;
        this.Token=token;
    }*/

    public CustomJsonObjectRequest(String url,String userID,
                                   JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, jsonRequest, listener, errorListener);
        this.UserID=userID;

    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> hashMap = new HashMap<>();
//        hashMap.putAll(super.getHeaders());
//        hashMap.put("userID",UserId);
//        hashMap.put("key",Token);

        hashMap.put("key","FDSDASDFVVSDSVVVDVVCfsd24231234");
        hashMap.put("userID",UserID);
        return hashMap;
    }

}
