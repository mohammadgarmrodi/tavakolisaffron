package ir.dayasoft.tavakolisaffron.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import ir.dayasoft.tavakolisaffron.Constants;
import ir.dayasoft.tavakolisaffron.R;


public class SplashActivity extends AppCompatActivity {
    Context context=this;
    /**
     * Duration of wait
     **/
    private final int SPLASH_DISPLAY_LENGTH = 2000;
    private final boolean IS_SHOW_SPLASH = true;

    /**
     * Called when the activity is first created.
     */
    private Runnable mMyRunnable = new Runnable() {
        @Override
        public void run() {
            if(!Constants.isCustomer){

                Intent intent = new Intent(SplashActivity.this, TestToken.class);
                startActivity(intent);
                finish();

            }
            else {
                Intent intent = new Intent(SplashActivity.this, BaseActivity.class);
                startActivity(intent);
                finish();
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            if (IS_SHOW_SPLASH) {
                setContentView(R.layout.splash_activity);
                Handler myHandler = new Handler();
                myHandler.postDelayed(mMyRunnable, SPLASH_DISPLAY_LENGTH);
            }

        }




}
