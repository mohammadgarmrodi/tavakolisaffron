package ir.dayasoft.tavakolisaffron.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import java.util.List;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.api.CategoryApi;
import ir.dayasoft.tavakolisaffron.api.InvoiceApi;
import ir.dayasoft.tavakolisaffron.api.InvoiceProductApi;
import ir.dayasoft.tavakolisaffron.api.KomoriProductApi;
import ir.dayasoft.tavakolisaffron.api.ProductApi;
import ir.dayasoft.tavakolisaffron.api.RestApiLifeCycle;
import ir.dayasoft.tavakolisaffron.core.AppConfig;
import ir.dayasoft.tavakolisaffron.core.NormalTextView;
import ir.dayasoft.tavakolisaffron.core.SharePref;
import ir.dayasoft.tavakolisaffron.fragment.BuyBasketFragment;
import ir.dayasoft.tavakolisaffron.fragment.CategoryFragment;
import ir.dayasoft.tavakolisaffron.fragment.KomoriProductFragment;
import ir.dayasoft.tavakolisaffron.fragment.OrderListFragment;
import ir.dayasoft.tavakolisaffron.model.Category;
import ir.dayasoft.tavakolisaffron.model.Invoice;
import ir.dayasoft.tavakolisaffron.model.InvoiceProduct;
import ir.dayasoft.tavakolisaffron.model.KomoriProduct;
import ir.dayasoft.tavakolisaffron.model.Product;
import ir.dayasoft.tavakolisaffron.model.SyncObject;
import ir.dayasoft.tavakolisaffron.model.User;
import ir.dayasoft.tavakolisaffron.utility.NetworkHelper;

/**
 * Created by mohammad on 01/17/2017.
 */

public class BaseActivity extends AppCompatActivity implements RestApiLifeCycle, View.OnClickListener {
    protected Toolbar toolbar;
    protected AppBarLayout appBar;
    protected BottomNavigationView bottomNavigation;
    protected NormalTextView toolbarNameTextView;
    protected FrameLayout fragmentContainer;
    private String userName;
    public static final int FRAGMENT_MAIN_ANIMAL_CATEGORY = 1;
    public static final int FRAGMENT_MAIN_ORDERLIST = 2;
    public static final int FRAGMENT_BUYBASKET = 3;

    CategoryApi categoryApi;
    KomoriProductApi komoriProductApi;
    ProductApi productApi;
    private ProgressDialog dataProgressDialog, internetProgressDialog;
    Context context = this;
    private int fragmentItem;
    public static final int STEP_FIRST_LOGIN = 0;
    public static final int STEP_FINISH_LOGIN = 1;
    User user;
    Invoice invoice;
    private Typeface typeface;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_base);
        getIntentData();
        initView();
        loadFragment();

        loadData();
        if (NetworkHelper.isOnline(context)) {
            if (fragmentItem == FRAGMENT_MAIN_ANIMAL_CATEGORY) {
                dataProgressDialog.show();
                categoryApi = new CategoryApi(this, context);
                categoryApi.setOffset(0);
                categoryApi.setPageSize(20);
                categoryApi.setUpdateDate(Category.GetLast(this).getUpdateDate());
                categoryApi.GetCategory(this);
            }
        } else {
            new android.support.v7.app.AlertDialog.Builder(context)
                    .setTitle("اینترنت شما قطع می باشد!")
                    .setNegativeButton("خب", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
        }
    }

    private void initView() {

        bottomNavigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        appBar = (AppBarLayout) findViewById(R.id.appBar);
        fragmentContainer = (FrameLayout) findViewById(R.id.fragment_container);
        bottomNavigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        appBar = (AppBarLayout) findViewById(R.id.appBar);
        internetProgressDialog = new ProgressDialog(context);
        internetProgressDialog.setCancelable(false);
        dataProgressDialog = new ProgressDialog(context);
        dataProgressDialog.setMessage("لطفا صبر کنید...");
        dataProgressDialog.setCancelable(false);
        toolbarNameTextView = (NormalTextView) findViewById(R.id.toolbar_name_textView);
        toolbarNameTextView.setOnClickListener(BaseActivity.this);
        typeface = AppConfig.GetFontTypeFace(this, false);

    }

    @Override
    protected void onResume() {
        super.onResume();
     //   loadData();
      //  loadFragment();
    }

    public void getIntentData() {

        fragmentItem = getIntent().getIntExtra("fragmentItem", FRAGMENT_MAIN_ANIMAL_CATEGORY);
    }

    public void openFragment(int typeFragment) {


        Log.i("navid", "" + typeFragment );
        FragmentManager fragmentManager = BaseActivity.this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (isProductOnStack())
        {
            fragmentManager.popBackStack();
        }

        switch (typeFragment) {

            case FRAGMENT_MAIN_ORDERLIST:
                fragmentTransaction.replace(R.id.fragment_container, OrderListFragment.newInstance(),"2");
                break;
            case FRAGMENT_BUYBASKET:
                fragmentTransaction.replace(R.id.fragment_container, BuyBasketFragment.newInstance(),"3");
                break;
            case FRAGMENT_MAIN_ANIMAL_CATEGORY:
                fragmentTransaction.replace(R.id.fragment_container, CategoryFragment.newInstance(),"1");
                break;
        }
        fragmentTransaction.commit();
    }

    private void loadData() {

        if (SharePref.getLoginStep(this) == STEP_FIRST_LOGIN) {
            toolbarNameTextView.setText("ثبت نام");
        } else if (SharePref.getLoginStep(this) == STEP_FINISH_LOGIN) {
            user = new User();
            userName = user.Get(this, SharePref.getUserId(this)).getLastName();
            toolbarNameTextView.setText(userName);
        }
        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                if (item.getItemId() == R.id.item_category_button) {
                    openFragment(FRAGMENT_MAIN_ANIMAL_CATEGORY);
                    bottomNavigation.getMenu().getItem(1).setChecked(false);
                    bottomNavigation.getMenu().getItem(2).setChecked(true);
                    bottomNavigation.getMenu().getItem(0).setChecked(false);
                } else if (item.getItemId() == R.id.item_orderList_button) {

                    openFragment(FRAGMENT_MAIN_ORDERLIST);
                    bottomNavigation.getMenu().getItem(1).setChecked(true);
                    bottomNavigation.getMenu().getItem(2).setChecked(false);
                    bottomNavigation.getMenu().getItem(0).setChecked(false);

                } else if (item.getItemId() == R.id.item_buyBasket_button) {
                    openFragment(FRAGMENT_BUYBASKET);
                    bottomNavigation.getMenu().getItem(1).setChecked(false);
                    bottomNavigation.getMenu().getItem(2).setChecked(false);
                    bottomNavigation.getMenu().getItem(0).setChecked(true);
                }
                return true;
            }
        });


    }

    private void loadFragment() {
        if (fragmentItem == FRAGMENT_MAIN_ORDERLIST) {
            openFragment(FRAGMENT_MAIN_ORDERLIST);
            bottomNavigation.getMenu().getItem(1).setChecked(true);
            bottomNavigation.getMenu().getItem(2).setChecked(false);
            bottomNavigation.getMenu().getItem(0).setChecked(false);
            View view = bottomNavigation.findViewById(R.id.item_orderList_button);
            view.performClick();
        } else if (fragmentItem == FRAGMENT_MAIN_ANIMAL_CATEGORY) {
            openFragment(FRAGMENT_MAIN_ANIMAL_CATEGORY);
            bottomNavigation.getMenu().getItem(1).setChecked(false);
            bottomNavigation.getMenu().getItem(2).setChecked(false);
            bottomNavigation.getMenu().getItem(0).setChecked(false);
            View view = bottomNavigation.findViewById(R.id.item_category_button);
            view.performClick();
        }
    }

    @Override
    public void feedback(Integer type, Object object, int section) {
        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:
                if (section == RestApiLifeCycle.STEP_COMPLETE_CATEGORY) {

                    komoriProductApi = new KomoriProductApi(this, context);
                    komoriProductApi.setOffset(0);
                    komoriProductApi.setPageSize(5);
                    komoriProductApi.setUpdateDate(KomoriProduct.GetLast(this).getUpdateDate());
                    komoriProductApi.GetKomoriProduct(this);

                }
                if (section == KomoriProductApi.STEP_KomoriProductAPI_COMPLETE) {

                    dataProgressDialog.cancel();

                    InvoiceProductApi invoiceProductApi = new InvoiceProductApi(this, context);
                    invoiceProductApi.setOffset(0);
                    invoiceProductApi.setPageSize(5);
                    invoiceProductApi.setUpdateDate(InvoiceProduct.GetLast(this).getUpdateDate());
                    invoiceProductApi.GetInvoiceProduct(this);

                    openFragment(BaseActivity.FRAGMENT_MAIN_ANIMAL_CATEGORY);

                }
                if (section == InvoiceProductApi.STEP_Invoice_KomoriProductAPI_COMPLETE) {


                    productApi = new ProductApi(this, context);
                    productApi.setOffset(0);
                    productApi.setPageSize(5);
                    productApi.setUpdateDate(Product.GetLast(this).getUpdateDate());
                    productApi.GetProduct(this);

                }

                if (section == ProductApi.STEP_ProductAPI_COMPLETE) {

                    InvoiceApi invoiceApi = new InvoiceApi(this, context);
                    invoiceApi.setOffset(0);
                    invoiceApi.setPageSize(5);
                    invoiceApi.setUpdateDate(Invoice.GetLast(this).getUpdateDate());
                    invoiceApi.GetInvoice(this);


                }


           //    FragmentManager fm = getSupportFragmentManager();
           //    CategoryFragment fragment = (CategoryFragment) fm.findFragmentById(R.id.fragment_container);
           //    fragment.refreshfragment();
           //    loadData();


                break;
            case RestApiLifeCycle.STEP_ERROR:
                if (section == RestApiLifeCycle.STEP_ERROR_CATEGORY) {
                    new android.support.v7.app.AlertDialog.Builder(context)
                            .setTitle("خطای سروری رخ داده است")
                            .setMessage("تلاش مجدد؟")
                            .setPositiveButton("بلی", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dataProgressDialog.setMessage("لطفا منتظر بمانید");
                                    dataProgressDialog.show();
                                    categoryApi.setOffset(0);
                                    categoryApi.setPageSize(5);
                                    categoryApi.setUpdateDate(Category.GetLast(context).getUpdateDate());
                                    categoryApi.GetCategory(context);
                                }
                            })
                            .setNegativeButton("بی خیال", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();
                }


                break;
            case RestApiLifeCycle.STEP_START:
                break;
        }
    }

    @Override
    public void passUser(Integer type, User user, int section) {

    }

    @Override
    public void passSyncObject(Integer type, SyncObject syncObject, int section) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_name_textView:
                if (toolbarNameTextView.getText().toString().equals("ثبت نام")) {
                    Intent intent = new Intent(this, RegisterActivity.class);
                    startActivity(intent);
                } else {
                    new AlertDialog.Builder(this).setTitle("هشدار")
                            .setMessage("آیا میخواهید از حساب کاربری خارج شوید؟!")
                            .setPositiveButton("بلی", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    invoice = new Invoice();
                                    invoice.Clear(context);
                                    user = new User();
                                    user.Clear(context);
                                    SharePref.setLoginStep(context, STEP_FIRST_LOGIN);
                                    SharePref.setUserId(context, 0L);
                                    toolbarNameTextView.setText("ثبت نام");
                                    openFragment(FRAGMENT_BUYBASKET);
                                    View view = bottomNavigation.findViewById(R.id.item_buyBasket_button);
                                    view.performClick();
                                }
                            })
                            .setNegativeButton("خیر", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();
                }
                break;
//
        }
    }



    public Boolean isProductOnStack()
    {
        List<Fragment> fragments =  (List<Fragment>) getSupportFragmentManager().getFragments();


        if (fragments !=null && fragments.size()>0) {
            if (fragments.get(fragments.size()-1) instanceof KomoriProductFragment) {
               return true;
            }
        }
        return false;
    }
    @Override
    public void onBackPressed() {


        List<Fragment> fragments =  (List<Fragment>) getSupportFragmentManager().getFragments();


        if (fragments.size()>0)
        {
           if ( fragments.get(fragments.size()-1) instanceof CategoryFragment )
           {
              if (((CategoryFragment)fragments.get(fragments.size()-1)).popUpCategory())
               super.onBackPressed();
           }
           else
           {
               super.onBackPressed();
           }
        }
        else
            super.onBackPressed();


    }
}