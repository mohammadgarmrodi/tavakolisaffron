package ir.dayasoft.tavakolisaffron.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.adapter.AnimalOrderListDetailAdapter;
import ir.dayasoft.tavakolisaffron.adapter.VegetableOrderListDetailAdapter;
import ir.dayasoft.tavakolisaffron.api.InvoiceApi;
import ir.dayasoft.tavakolisaffron.api.InvoiceItemApi;
import ir.dayasoft.tavakolisaffron.api.PaymentMethodApi;
import ir.dayasoft.tavakolisaffron.api.RestApiLifeCycle;
import ir.dayasoft.tavakolisaffron.core.Constants;
import ir.dayasoft.tavakolisaffron.core.Core;
import ir.dayasoft.tavakolisaffron.core.NormalTextView;
import ir.dayasoft.tavakolisaffron.model.Invoice;
import ir.dayasoft.tavakolisaffron.model.InvoiceItem;
import ir.dayasoft.tavakolisaffron.model.InvoiceProduct;
import ir.dayasoft.tavakolisaffron.model.PreFactor;
import ir.dayasoft.tavakolisaffron.model.SyncObject;
import ir.dayasoft.tavakolisaffron.model.User;

public class OrderListDetailActivity extends AppCompatActivity implements RestApiLifeCycle, View.OnClickListener {
    protected ImageView informationIcon;
    protected RelativeLayout informationRelativeLayout;
    protected NormalTextView firstGroupTextView;
    protected RelativeLayout firstGroupRelativeLayout;
    protected NormalTextView secondGroupTextView;
    protected RelativeLayout secondGroupRelativeLayout;
    protected RelativeLayout orderListDetailSecondRecycleView;
    protected RelativeLayout firstRelativeLayout;
    protected NormalTextView orderListDetailTotalPriceTextView;
    protected NormalTextView orderListDetailTotalCountTextView;
    protected RelativeLayout orderListDetailAnimalRelativeLayout;
    protected RelativeLayout orderListDetailVegetableRelativeLayout;
    protected LinearLayout orderListDetailAnimalLinearLayout;
    protected LinearLayout orderListDetailVegetableLinearLayout;
    protected NormalTextView orderListDetailStatusTextView;
    protected NormalTextView orderListDetailInvoiceNumberTextView;
    protected Toolbar toolbar;
    protected AppBarLayout appBar;
    protected RelativeLayout orderListDetailRadioGroupRelativeLayout;
    protected LinearLayout orderListDetailPaymentRelativeLayout;
    protected CoordinatorLayout orderListDetailSnackBarPosition;
    protected NormalTextView toolbarTextView;
    protected ProgressBar progressBar;
    protected NormalTextView onlinePaymentTextView;
    LinearLayoutManager linearLayoutManager;
    private List<PreFactor> animalList, vegetableList;
    RecyclerView animalRecycleView, vegetableRecycleView;
    AnimalOrderListDetailAdapter animalOrderListDetailAdapter;
    VegetableOrderListDetailAdapter vegetableOrderListDetailAdapter;
    private int invoiceID, invoiceStatus;
    Context context = this;
    private ProgressDialog dataprogressDialog;
    InvoiceProduct invoiceProduct;
    private int totalCount, totalPrice;
    public static final int FRAGMENT_MAIN_ANIMAL_CATEGORY = 1;
    public static final int FRAGMENT_MAIN_ORDERLIST = 2;
    public static final int FRAGMENT_BUYBASKET = 3;
    public static final int FRAGMENT_MAIN_VEGETABLE_CATEGORY = 4;
    public static final int ARG_COMPLETE_STATUS = 3;
    public static final int ARG_CHECK_STATUS = 1;
    public static final int ARG_CANCEL_STATUS = 2;
    private Invoice invoice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list_detail_acitivty);
        getIntentData();
        initView();
        loadData();


    }

    private void initView() {
        animalRecycleView = (RecyclerView) findViewById(R.id.orderListDetail_animal_RecycleView);
        firstGroupRelativeLayout = (RelativeLayout) findViewById(R.id.firstGroupRelativeLayout);
        firstRelativeLayout = (RelativeLayout) findViewById(R.id.firstRelativeLayout);
        orderListDetailTotalPriceTextView = (NormalTextView) findViewById(R.id.orderListDetail_totalPrice_textView);
        orderListDetailTotalCountTextView = (NormalTextView) findViewById(R.id.orderListDetail_totalCount_textView);
        orderListDetailAnimalLinearLayout = (LinearLayout) findViewById(R.id.orderListDetail_animal_linearLayout);
        orderListDetailStatusTextView = (NormalTextView) findViewById(R.id.orderListDetail_status_textView);
        orderListDetailInvoiceNumberTextView = (NormalTextView) findViewById(R.id.orderListDetail_invoiceNumber_textView);
        informationRelativeLayout = (RelativeLayout) findViewById(R.id.information_RelativeLayout);
        orderListDetailPaymentRelativeLayout = (LinearLayout) findViewById(R.id.orderListDetail_payment_relativeLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        appBar = (AppBarLayout) findViewById(R.id.appBar);
        informationIcon = (ImageView) findViewById(R.id.information_icon);
        orderListDetailSnackBarPosition = (CoordinatorLayout) findViewById(R.id.orderListDetail_snackBarPosition);
        dataprogressDialog = new ProgressDialog(context);
        dataprogressDialog.setTitle("لطفا صبر کنید...");
        dataprogressDialog.setCancelable(false);
        toolbarTextView = (NormalTextView) findViewById(R.id.activity_orderList_toolbar_textView);
//        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        onlinePaymentTextView = (NormalTextView) findViewById(R.id.orderListDetail_onlinePayment_textView);
        onlinePaymentTextView.setOnClickListener(this);
    }

    private void getIntentData() {
        Intent intent = getIntent();
        invoiceID = intent.getIntExtra("invoiceID", 0);
        if (invoiceID == 0) {
            invoiceID = Integer.parseInt(intent.getStringExtra("id").toString());
        }
    }

    private void loadData() {
        dataprogressDialog.show();
        InvoiceApi invoiceApi;
        invoiceApi = new InvoiceApi(this, context);
        invoiceApi.setOffset(0);
        invoiceApi.setPageSize(5);
        invoiceApi.setUpdateDate(Invoice.GetLast(this).getUpdateDate());
        invoiceApi.GetInvoice(this);


    }

    private void bindData() {

        invoice = new Invoice();
        invoiceStatus = invoice.Get(this, invoiceID).getStatus();
        invoice = invoice.Get(this, invoiceID);
        invoiceProduct = new InvoiceProduct();
        animalList = invoiceProduct.GetAnimalInvoiceProduct(this, invoiceID);
        totalCount = InvoiceProduct.GetListInvoice(this, invoiceID).size();
        totalPrice = InvoiceProduct.GetSum(this, invoiceID).getPrice();
        orderListDetailTotalCountTextView.setText(String.valueOf(totalCount));
        orderListDetailTotalPriceTextView.setText(String.valueOf(Core.Converter.DoubleToString(totalPrice)));
        orderListDetailInvoiceNumberTextView.setText(String.valueOf(invoiceID));

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        animalRecycleView.setLayoutManager(linearLayoutManager);
        animalOrderListDetailAdapter = new AnimalOrderListDetailAdapter(this, animalList);
        animalRecycleView.setAdapter(animalOrderListDetailAdapter);

        if (invoiceStatus == ARG_CHECK_STATUS) {
            toolbarTextView.setText("پیش شماره فاکتور");
            orderListDetailStatusTextView.setText("سفارش شما در حال بررسی می باشد");
        } else if (invoiceStatus == ARG_CANCEL_STATUS) {
            toolbarTextView.setText("شماره فاکتور");
            orderListDetailStatusTextView.setText("سفارش شما کنسل شده است");
        } else if (invoiceStatus == ARG_COMPLETE_STATUS) {
            dataprogressDialog.show();
            toolbarTextView.setText("شماره فاکتور");

            if (invoice.getInvoiceItemPaymentStatus() == 1) {
                orderListDetailStatusTextView.setText("سفارش شما نهایی شد");

            } else if (invoice.getInvoiceItemPaymentStatus() == 2) {
                orderListDetailStatusTextView.setText("سفارش شما نهایی و هزینه پرداخت شد");
            } else {
                orderListDetailStatusTextView.setText("سفارش شما تایید شد");
                orderListDetailPaymentRelativeLayout.setVisibility(View.VISIBLE);
            }


        }
    }

    private void attachFragment() {
        // getSupportFragmentManager().a
    }

    private void showDialge() {
    }

    @Override
    public void feedback(Integer type, Object object, int section) {
        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:
                if (section == RestApiLifeCycle.STEP_COMPLETE_INVOICE) {
                    dataprogressDialog.dismiss();
                    bindData();
                }
                if (object.equals("3")) {
//                    progressBar.setVisibility(View.GONE);
                    orderListDetailPaymentRelativeLayout.setVisibility(View.GONE);
                } else if (object.equals("2")) {
//                    progressBar.setVisibility(View.GONE);
                    orderListDetailPaymentRelativeLayout.setVisibility(View.GONE);
                } else if (object.equals("1")) {
//                    progressBar.setVisibility(View.GONE);
                    orderListDetailPaymentRelativeLayout.setVisibility(View.GONE);
                }
                dataprogressDialog.cancel();
                break;
            case RestApiLifeCycle.STEP_ERROR:
                dataprogressDialog.dismiss();
                break;
            case RestApiLifeCycle.STEP_START:
                break;
        }
    }

    @Override
    public void passUser(Integer type, User user, int section) {


    }

    @Override
    public void passSyncObject(Integer type, SyncObject syncObject, int section) {
        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:
                if (section == RestApiLifeCycle.STEP_COMPLETE_INVOICE_ITEM) {
                    dataprogressDialog.dismiss();
                    loadData();
                }
                break;
            case RestApiLifeCycle.STEP_ERROR:
                if (section == RestApiLifeCycle.STEP_ERROR_INVOICE) {
                    Toast.makeText(context, "خطا در برقراری ارتباط با سرور", Toast.LENGTH_SHORT).show();
                }
                break;
            case RestApiLifeCycle.STEP_START:
                break;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.orderListDetail_onlinePayment_textView:
                new AlertDialog.Builder(this).setTitle("پرداخت در محل")
                        .setMessage("در صورت تایید،فاکتور شما نهایی خواهد شد")
                        .setPositiveButton("تایید", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dataprogressDialog.show();
                                Handler handler1 = new Handler();
                                handler1.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        InvoiceItemApi invoiceItemApi = new InvoiceItemApi(OrderListDetailActivity.this, context);
                                        InvoiceItem invoiceItem = new InvoiceItem();
//                                        invoiceItem.setBankName();
                                        invoiceItem.setStatus(1);
                                        invoiceItem.setPaymentStatus(1);
                                        invoiceItem.setFK_InvoiceID(invoiceID);
                                        invoiceItemApi.PostIInvoiceItem(context, invoiceItem);
                                        orderListDetailPaymentRelativeLayout.setVisibility(View.GONE);


                                    }
                                }, 2000);
                                Snackbar snackbar = Snackbar.make(orderListDetailSnackBarPosition, "سفارش شما با موفقیت نهایی شد", Snackbar.LENGTH_LONG);
                                snackbar.show();

                            }
                        })
                        .setNegativeButton("برگشت", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();

                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(OrderListDetailActivity.this, BaseActivity.class);
        intent.putExtra("fragmentItem", FRAGMENT_MAIN_ORDERLIST);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


}
