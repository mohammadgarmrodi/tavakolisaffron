package ir.dayasoft.tavakolisaffron.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.api.InvoiceApi;
import ir.dayasoft.tavakolisaffron.api.RestApiLifeCycle;
import ir.dayasoft.tavakolisaffron.api.UserApi;
import ir.dayasoft.tavakolisaffron.core.AppConfig;
import ir.dayasoft.tavakolisaffron.core.SharePref;
import ir.dayasoft.tavakolisaffron.model.Invoice;
import ir.dayasoft.tavakolisaffron.model.SyncObject;
import ir.dayasoft.tavakolisaffron.model.User;

public class ActivationActivity extends AppCompatActivity  implements RestApiLifeCycle,View.OnClickListener {

    protected RelativeLayout submitButton;
    protected Button button;
    private EditText tokenEditText;
    private TextView userNameTextView;
    Context context = this;
    private Long userID;
    private String userIDString,userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_activation);
        getIntentData();
        initView();
        loadData();
        bindData();

    }

    private void getIntentData() {
        Intent intent = getIntent();
        userID = intent.getLongExtra("userID",-1);
        userName=intent.getStringExtra("userName");
        userIDString=String.valueOf(userID);
    }

    private void initView() {
        submitButton = (RelativeLayout) findViewById(R.id.loginActivation_back_relativeLayout);
        tokenEditText = (EditText) findViewById(R.id.activation_token_editText);
        userNameTextView=(TextView)findViewById(R.id.Activation_phoneNumber_textView);
    }

    private void bindData() {
      submitButton.setOnClickListener(this);
    }

    private void loadData() {

        userNameTextView.setText(userName);

    }

    @Override
    public void feedback(Integer type, Object object, int section) {


    }

    @Override
    public void passUser(Integer type, User user, int section) {


        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:
                Toast.makeText(context, "Complete!", Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(ActivationActivity.this, CategoryActivity.class);
//                startActivity(intent);
//                finish();
                break;
            case RestApiLifeCycle.STEP_ERROR:
                Toast.makeText(context, "Error!", Toast.LENGTH_SHORT).show();
                break;
            case RestApiLifeCycle.STEP_START:
                break;

        }

    }

    @Override
    public void passSyncObject(Integer type, SyncObject syncObject, int section) {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.Activation_submit_relativeLayout:

                UserApi userApi = new UserApi(ActivationActivity.this,context);
                userApi.setToken(tokenEditText.getText().toString());
                userApi.setUserID(userIDString);
                userApi.GetCheckToken(context);

                break;

        }
    }
}
