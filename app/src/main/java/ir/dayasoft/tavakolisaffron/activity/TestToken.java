package ir.dayasoft.tavakolisaffron.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.firebase.iid.FirebaseInstanceId;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.api.FcmTokenUserApi;
import ir.dayasoft.tavakolisaffron.api.RestApiLifeCycle;
import ir.dayasoft.tavakolisaffron.core.NormalEditTextView;
import ir.dayasoft.tavakolisaffron.model.FcmTokenUser;
import ir.dayasoft.tavakolisaffron.model.SyncObject;
import ir.dayasoft.tavakolisaffron.model.User;


public class TestToken extends AppCompatActivity implements RestApiLifeCycle {

    protected NormalEditTextView edittext;
    protected Button button;
    Context context=this;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_test_token);
        initView();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FcmTokenUserApi fcmTokenUserApi=new FcmTokenUserApi(TestToken.this,context);
                FcmTokenUser fcmTokenUser=new FcmTokenUser();
                token= FirebaseInstanceId.getInstance().getToken();
                fcmTokenUser.setToken(token);
                fcmTokenUser.setUserID(Integer.valueOf(edittext.getText().toString()));
                fcmTokenUserApi.PostFcmTokenUser(context,fcmTokenUser);
            }
        });
    }

    private void initView() {
        edittext = (NormalEditTextView) findViewById(R.id.edittext);
        button = (Button) findViewById(R.id.button);
    }

    @Override
    public void feedback(Integer type, Object object, int section) {

    }

    @Override
    public void passUser(Integer type, User user, int section) {

    }

    @Override
    public void passSyncObject(Integer type, SyncObject syncObject, int section) {

    }
}
