package ir.dayasoft.tavakolisaffron.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.api.FcmTokenUserApi;
import ir.dayasoft.tavakolisaffron.api.InvoiceApi;
import ir.dayasoft.tavakolisaffron.api.RestApiLifeCycle;
import ir.dayasoft.tavakolisaffron.api.UserApi;
import ir.dayasoft.tavakolisaffron.core.NormalEditTextView;
import ir.dayasoft.tavakolisaffron.core.NormalTextView;
import ir.dayasoft.tavakolisaffron.core.SharePref;
import ir.dayasoft.tavakolisaffron.model.FcmTokenUser;
import ir.dayasoft.tavakolisaffron.model.Invoice;
import ir.dayasoft.tavakolisaffron.model.SyncObject;
import ir.dayasoft.tavakolisaffron.model.User;

public class RegisterActivity extends AppCompatActivity implements RestApiLifeCycle, View.OnClickListener {

    protected RelativeLayout registerCancelRelativeLayout;
    protected NormalTextView registerNameTextView;
    protected RelativeLayout registerNameRelativeLayout;
    protected NormalTextView registerPhoneNumberTextView;
    protected RelativeLayout registerPhoneNumberRelativeLayout;
    protected NormalTextView registerPasswordTextView;
    protected NormalEditTextView registerpasswordEditText;
    protected RelativeLayout registerPasswordRelativeLayout;
    protected NormalTextView registerAddressTextView;
    protected RelativeLayout registerAddressRelativeLayout;
    protected NormalTextView registerSubmitTextView;
    protected NormalTextView registerQuestionTextView;
    protected RelativeLayout activityRegister;
    protected Toolbar toolbar;
    protected AppBarLayout appBar;
    protected NormalTextView registerTitleTextView;
    protected ImageView registerStarImageView;
    private CoordinatorLayout coordinatorLayout;
    Context context = this;
    private TextView loginButton;
    protected EditText nameEditText;
    protected EditText usernameEditText;
    protected EditText addressEditText;
    protected RelativeLayout registerButton;
    private ImageView backButton;
    private String name, userName, address, password;
    User user;
    private String token;
    private Boolean nameBoolean = false, usernameBoolean = false, addressBoolean = false, passwordBoolean = false;
    private ProgressDialog dataProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_register);
        initView();
        loadData();
    }

    private void initView() {
        nameEditText = (EditText) findViewById(R.id.register_name_editText);
        usernameEditText = (EditText) findViewById(R.id.register_phoneNumber_editText);
        addressEditText = (EditText) findViewById(R.id.register_address_editText);
        registerButton = (RelativeLayout) findViewById(R.id.register_submit_relativeLayout);
        loginButton = (TextView) findViewById(R.id.register_login_textView);
        backButton = (ImageView) findViewById(R.id.register_back_imageView);
        registerCancelRelativeLayout = (RelativeLayout) findViewById(R.id.register_cancel_relativeLayout);
        registerNameTextView = (NormalTextView) findViewById(R.id.register_name_textView);
        registerNameRelativeLayout = (RelativeLayout) findViewById(R.id.register_name_RelativeLayout);
        registerPhoneNumberTextView = (NormalTextView) findViewById(R.id.register_phoneNumber_textView);
        registerPhoneNumberRelativeLayout = (RelativeLayout) findViewById(R.id.register_phoneNumber_RelativeLayout);
        registerPasswordTextView = (NormalTextView) findViewById(R.id.register_password_textView);
        registerpasswordEditText = (NormalEditTextView) findViewById(R.id.register_password_editText);
        registerPasswordRelativeLayout = (RelativeLayout) findViewById(R.id.register_password_RelativeLayout);
        registerAddressTextView = (NormalTextView) findViewById(R.id.register_address_textView);
        registerAddressRelativeLayout = (RelativeLayout) findViewById(R.id.register_address_RelativeLayout);
        registerSubmitTextView = (NormalTextView) findViewById(R.id.register_submit_textView);
        registerQuestionTextView = (NormalTextView) findViewById(R.id.register_question_textView);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.snackbarPosition);
        appBar = (AppBarLayout) findViewById(R.id.appBar);
        registerTitleTextView = (NormalTextView) findViewById(R.id.register_title_textView);
        registerStarImageView = (ImageView) findViewById(R.id.register_star_imageView);
        activityRegister = (RelativeLayout) findViewById(R.id.activity_register);
        backButton.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        registerButton.setOnClickListener(this);
        registerTitleTextView.requestFocus();
        dataProgressDialog = new ProgressDialog(context);
        dataProgressDialog.setMessage("لطفا صبر کنید...");
        dataProgressDialog.setCancelable(false);

    }

    private void loadData() {


    }

    private void bindData() {


    }

    @Override
    public void feedback(Integer type, final Object object, int section) {
        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:

                break;
            case RestApiLifeCycle.STEP_ERROR:
                dataProgressDialog.cancel();
                if (object.equals(0)) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "خطا در برقراری ارتباط", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (object.equals(2)) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "کاربر وجود ندارد", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (object.equals(3)) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "کاربر تایید نشده است", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (object.equals(4)) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "خطا در ارسال کد فعال سازی", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (object.equals(5)) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "رمز عبور اشتباه می باشد", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (object.equals(6)) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "کد فعال سازی اشتباه می باشد", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (object.equals(7)) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "انقضا کد فعال سازی به پایان رسیده است", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (object.equals(8)) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "شماره وارد شده،وجود دارد", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                break;
            case RestApiLifeCycle.STEP_START:
                break;
        }
    }

    @Override
    public void passUser(Integer type, User user, int section) {
        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:
                dataProgressDialog.cancel();
                Snackbar snackbar1 = Snackbar.make(coordinatorLayout, "ثبت نام با موفقیت انجام شد", Snackbar.LENGTH_LONG);
                snackbar1.show();
                User user1=new User();
                SharePref.setUserId(context, user.getUserID());
                user1.Insert(this, user);
                token = FirebaseInstanceId.getInstance().getToken();
                FcmTokenUserApi fcmTokenUserApi = new FcmTokenUserApi(RegisterActivity.this, context);
                FcmTokenUser fcmTokenUser = new FcmTokenUser();
                fcmTokenUser.setUserID(user.getUserID());
                fcmTokenUser.setToken(token);
                fcmTokenUserApi.PostFcmTokenUser(this, fcmTokenUser);
                Handler handler1 = new Handler();
                handler1.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(RegisterActivity.this, BaseActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, 1500);


                break;
            case RestApiLifeCycle.STEP_ERROR:
                break;
            case RestApiLifeCycle.STEP_START:
                break;
        }
    }

    @Override
    public void passSyncObject(Integer type, SyncObject syncObject, int section) {


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_back_imageView:
                onBackPressed();
                break;
            case R.id.register_login_textView:
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.register_submit_relativeLayout:
                user = new User();
                name = nameEditText.getText().toString();
                userName = usernameEditText.getText().toString();
                password = registerpasswordEditText.getText().toString();
                address = addressEditText.getText().toString();
                if (userName.length() == 0 & password.length() == 0 & address.length() == 0 & name.length() == 0) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "فیلد های خالی را پر کنید", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    if (password.length() == 0 & address.length() == 0 & name.length() == 0) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "فیلد های خالی را پر کنید", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else {
                        if (userName.length() == 0 & address.length() == 0 & name.length() == 0) {
                            Snackbar snackbar = Snackbar.make(coordinatorLayout, "فیلد های خالی را پر کنید", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        } else {
                            if (userName.length() == 0 & password.length() == 0 & address.length() == 0) {
                                Snackbar snackbar = Snackbar.make(coordinatorLayout, "فیلد های خالی را پر کنید", Snackbar.LENGTH_LONG);
                                snackbar.show();
                            } else {
                                if (userName.length() == 0 & password.length() == 0 & name.length() == 0) {
                                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "فیلد های خالی را پر کنید", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                } else {
                                    if (userName.length() == 0) {
                                        passwordBoolean = false;
                                        nameBoolean = false;
                                        addressBoolean = false;
                                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "شماره همراه را وارد کنید", Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                    } else {
                                        usernameBoolean = true;
                                        user.setUserName(userName);
                                    }
                                    if (password.length() == 0) {
                                        usernameBoolean = false;
                                        nameBoolean = false;
                                        addressBoolean = false;
                                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "رمز عبور را وارد کنید", Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                    } else {
                                        passwordBoolean = true;
                                        user.setPassword(password);
                                    }
                                    if (address.length() == 0) {
                                        usernameBoolean = false;
                                        nameBoolean = false;
                                        passwordBoolean = false;
                                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "آدرس را وارد کنید", Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                    } else {
                                        addressBoolean = true;
                                        user.setAddress(address);
                                    }
                                    if (name.length() == 0) {
                                        usernameBoolean = false;
                                        addressBoolean = false;
                                        passwordBoolean = false;
                                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "نام و نام خانوادگی را وارد کنید", Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                    } else {
                                        nameBoolean = true;
                                        user.setLastName(name);
                                    }
                                }
                                if (password.length() < 6) {
                                    usernameBoolean = false;
                                    nameBoolean = false;
                                    addressBoolean = false;
                                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "تعداد حروف رمز عبور باید از 6 حرف بیشتر باشد", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                } else {
                                    passwordBoolean = true;
                                }
                                if (userName.length() != 11 & userName.length() > 0) {
                                    addressBoolean = false;
                                    passwordBoolean = false;
                                    nameBoolean = false;
                                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "شماره همراه را صحیح وارد کنید", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                } else {
                                    passwordBoolean = true;
                                }
                                if (nameBoolean & addressBoolean & usernameBoolean & passwordBoolean) {
                                    dataProgressDialog.show();
                                    UserApi userApi = new UserApi(RegisterActivity.this, context);
                                    userApi.PostUser(context, user);
                                }
                            }
                            break;
                        }
                    }
                }
        }
    }
}