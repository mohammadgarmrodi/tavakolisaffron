package ir.dayasoft.tavakolisaffron.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.api.RestApiLifeCycle;
import ir.dayasoft.tavakolisaffron.core.NormalTextView;
import ir.dayasoft.tavakolisaffron.model.KomoriProduct;
import ir.dayasoft.tavakolisaffron.model.SyncObject;
import ir.dayasoft.tavakolisaffron.model.User;
import ir.dayasoft.tavakolisaffron.volly.MySingleton;

public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener, RestApiLifeCycle {

    protected ImageView activityProductDetailToolbarBackImageView;
    protected ImageView activityProductDetailToolbarShareImageView;
    protected Toolbar activityProductDetailToolbar;
    protected NetworkImageView activityProductDetailPictureImageView;
    protected NormalTextView activityProductDetailNameTextView;
    protected ImageView komoriProductChooseIconImageView;
    protected NormalTextView activityProductDetailWeightTextView;
    protected NormalTextView activityProductDetailPriceTextView;
    protected RelativeLayout activityProductDetailDetailRelativeLayout;
    protected NormalTextView activityProductDetailDescriptionTitleTextView;
    protected NormalTextView activityProductDetailDescriptionTextView;
    protected ProgressBar activityProductDetailProgressBar;
    private String NAME = "Name";
    private String WEIGHT = "Weight";
    private String PRICE = "Price";
    private String DESCRIPTION = "Description";
    private String IMAGE = "Image";
    private String Count = "Count";
    private String name, description, image;
    private int price, weight, count;
    private List<KomoriProduct> komoriProductList;
    private ImageLoader imageLoader;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_product_detail);
        initView();
        getIntentData();
        loadData();
        bindData();
    }

    private void initView() {
        activityProductDetailToolbarBackImageView = (ImageView) findViewById(R.id.activity_productDetail_toolbar_back_imageView);
        activityProductDetailToolbar = (Toolbar) findViewById(R.id.activity_productDetail_toolbar);
        activityProductDetailPictureImageView = (NetworkImageView) findViewById(R.id.activity_productDetail_picture_imageView);
        activityProductDetailNameTextView = (NormalTextView) findViewById(R.id.activity_productDetail_name_textView);
        komoriProductChooseIconImageView = (ImageView) findViewById(R.id.komoriProduct_chooseIcon_imageView);
        activityProductDetailWeightTextView = (NormalTextView) findViewById(R.id.activity_productDetail_weight_textView);
        activityProductDetailPriceTextView = (NormalTextView) findViewById(R.id.activity_productDetail_price_textView);
        activityProductDetailDetailRelativeLayout = (RelativeLayout) findViewById(R.id.activity_productDetail_detail_relativeLayout);
        activityProductDetailDescriptionTitleTextView = (NormalTextView) findViewById(R.id.activity_productDetail_descriptionTitle_textView);
        activityProductDetailDescriptionTextView = (NormalTextView) findViewById(R.id.activity_productDetail_description_textView);
        activityProductDetailToolbarBackImageView.setOnClickListener(this);
    }

    private void getIntentData() {
        name = getIntent().getStringExtra(NAME);
        price = getIntent().getIntExtra(PRICE, 0);
        weight = getIntent().getIntExtra(WEIGHT, 0);
        description = getIntent().getStringExtra(DESCRIPTION);
        image = getIntent().getStringExtra(IMAGE);
        count = getIntent().getIntExtra(Count, -1);
    }

    private void loadData() {

    }

    private void bindData() {
        activityProductDetailNameTextView.setText(name);
        if (weight == 0) {
            activityProductDetailWeightTextView.setVisibility(View.GONE);

        } else {
            activityProductDetailWeightTextView.setText(String.valueOf(weight) + " گرم");

        }
        activityProductDetailPriceTextView.setText(String.valueOf(price) + " تومان ");
        activityProductDetailDescriptionTextView.setText(description);
        imageLoader = MySingleton.getInstance(this).getImageLoader();
        final String IMAGE_URL = image;
        if (IMAGE_URL != null) {
            activityProductDetailPictureImageView.setImageUrl(IMAGE_URL, imageLoader);
        } else {
            activityProductDetailPictureImageView.setDefaultImageResId(R.drawable.no_image);
        }
        if (count == 0) {
            komoriProductChooseIconImageView.setVisibility(View.GONE);
        } else {
            komoriProductChooseIconImageView.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void feedback(Integer type, Object object, int section) {
        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:
                break;
            case RestApiLifeCycle.STEP_ERROR:
                break;
            case RestApiLifeCycle.STEP_START:
                break;
        }
    }

    @Override
    public void passSyncObject(Integer type, SyncObject syncObject, int section) {
        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:
                break;
            case RestApiLifeCycle.STEP_ERROR:
                break;
            case RestApiLifeCycle.STEP_START:
                break;
        }
    }

    @Override
    public void passUser(Integer type, final User user, int section) {
        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:
                break;
            case RestApiLifeCycle.STEP_ERROR:
                break;
            case RestApiLifeCycle.STEP_START:

                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_productDetail_toolbar_back_imageView:
                finish();
                break;
        }

    }
}