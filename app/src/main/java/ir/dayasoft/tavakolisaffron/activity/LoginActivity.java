package ir.dayasoft.tavakolisaffron.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.api.FcmTokenUserApi;
import ir.dayasoft.tavakolisaffron.api.InvoiceApi;
import ir.dayasoft.tavakolisaffron.api.InvoiceProductApi;
import ir.dayasoft.tavakolisaffron.api.RestApiLifeCycle;
import ir.dayasoft.tavakolisaffron.api.UserApi;
import ir.dayasoft.tavakolisaffron.core.SharePref;
import ir.dayasoft.tavakolisaffron.fragment.BuyBasketFragment;
import ir.dayasoft.tavakolisaffron.model.FcmTokenUser;
import ir.dayasoft.tavakolisaffron.model.Invoice;
import ir.dayasoft.tavakolisaffron.model.InvoiceProduct;
import ir.dayasoft.tavakolisaffron.model.SyncObject;
import ir.dayasoft.tavakolisaffron.model.User;

public class LoginActivity extends AppCompatActivity implements RestApiLifeCycle, View.OnClickListener {


    protected EditText userNameEditText, passwordEditText;
    protected RelativeLayout loginButton;
    private TextView registerTextView;
    private String username, password;
    private CoordinatorLayout coordinatorLayout;
    Context context = this;
    UserApi userApi;
    private Boolean usernameBoolean = false, passwordBoolean = false;
    private String token;
    private Long userID;
    private ProgressDialog dataProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_login);
        initView();
        loadData();
        bindData();
    }

    private void initView() {
        userNameEditText = (EditText) findViewById(R.id.login_phoneNumber_editText);
        loginButton = (RelativeLayout) findViewById(R.id.login_submit_relativeLayout);
        registerTextView = (TextView) findViewById(R.id.login_register_textView);
        passwordEditText = (EditText) findViewById(R.id.login_password_editText);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.Login_snackBarPosition);
        dataProgressDialog = new ProgressDialog(context);
        dataProgressDialog.setMessage("لطفا صبر کنید...");
        dataProgressDialog.setCancelable(false);
        loginButton.setOnClickListener(this);
        registerTextView.setOnClickListener(this);
        loginButton.requestFocus();
    }

    private void loadData() {

    }

    private void bindData() {
        username = userNameEditText.getText().toString();
        password = passwordEditText.getText().toString();
    }

    @Override
    public void feedback(Integer type, Object object, int section) {
        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:
                break;
            case RestApiLifeCycle.STEP_ERROR:
                if (section == RestApiLifeCycle.STEP_ERROR_LOGIN) {
                    dataProgressDialog.cancel();
                    if (object.equals(0)) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "خطا در برقراری ارتباط", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else if (object.equals(2)) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "کاربر وجود ندارد", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else if (object.equals(3)) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "کاربر تایید نشده است", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else if (object.equals(4)) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "خطا در ارسال کد فعال سازی", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else if (object.equals(5)) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "رمز عبور اشتباه می باشد", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else if (object.equals(6)) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "کد فعال سازی اشتباه می باشد", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else if (object.equals(7)) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "انقضا کد فعال سازی به پایان رسیده است", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else if (object.equals(8)) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "شماره وارد شده،وجود دارد", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    break;
                }
            case RestApiLifeCycle.STEP_START:

                break;
        }

    }

    @Override
    public void passSyncObject(Integer type, SyncObject syncObject, int section) {
        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:
                if (section == RestApiLifeCycle.STEP_COMPLETE_FCMTOKEN) {
                    dataProgressDialog.cancel();
                    User user = new User();
                    user.setFcmToken(token);
                    user.setUserID(Long.parseLong(syncObject.getServerID()));
                    user.Insert(this, user);
                }
                break;
            case RestApiLifeCycle.STEP_ERROR:
                if (section == RestApiLifeCycle.STEP_ERROR_LOGIN) {

                    break;
                }
            case RestApiLifeCycle.STEP_START:
                break;
        }
    }

    @Override
    public void passUser(Integer type, final User user, int section) {
        switch (type) {
            case RestApiLifeCycle.STEP_ADD_QUEUE:
                break;
            case RestApiLifeCycle.STEP_COMPELETE:
                if (section == RestApiLifeCycle.STEP_COMPLETE_LOGIN) {
                    dataProgressDialog.cancel();
                    InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    userID = user.getUserID();
                    SharePref.setLoginStep(context, User.STEP_FINISH_LOGIN);
                    SharePref.setUserId(context, userID);
                    InvoiceProductApi invoiceProductApi = new InvoiceProductApi(this, context);
                    invoiceProductApi.setOffset(0);
                    invoiceProductApi.setPageSize(5);
                    invoiceProductApi.setUpdateDate(InvoiceProduct.GetLast(this).getUpdateDate());
                    invoiceProductApi.GetInvoiceProduct(this);

                    InvoiceApi invoiceApi = new InvoiceApi(this, context);
                    invoiceApi.setOffset(0);
                    invoiceApi.setPageSize(5);
                    invoiceApi.setUpdateDate(Invoice.GetLast(this).getUpdateDate());
                    invoiceApi.GetInvoice(this);

                    token = FirebaseInstanceId.getInstance().getToken();
                    FcmTokenUserApi fcmTokenUserApi = new FcmTokenUserApi(LoginActivity.this, context);
                    FcmTokenUser fcmTokenUser = new FcmTokenUser();
                    fcmTokenUser.setUserID(userID);
                    fcmTokenUser.setToken(token);
                    fcmTokenUserApi.PostFcmTokenUser(this, fcmTokenUser);
                    Snackbar snackbar1 = Snackbar.make(coordinatorLayout, "خوش آمدید", Snackbar.LENGTH_LONG);
                    snackbar1.show();
                    Handler handler1 = new Handler();
                    handler1.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(LoginActivity.this, BaseActivity.class);
                            intent.putExtra("userID", userID);
                            startActivity(intent);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            finish();
                        }
                    }, 1500);
                }
                break;
            case RestApiLifeCycle.STEP_ERROR:
                break;
            case RestApiLifeCycle.STEP_START:

                break;
        }
    }

    @Override
    public void onClick(View v) {
        bindData();
        switch (v.getId()) {
            case R.id.login_submit_relativeLayout:
                if (password.length() == 0 & username.length() == 0) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "شماره همراه و رمز عبور را وارد کنید", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    if (username.length() == 0) {
                        passwordBoolean = false;
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "شماره همراه را وارد کنید", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else {
                        usernameBoolean = true;
                    }
                    if (password.length() == 0) {
                        usernameBoolean = false;
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "رمز عبور را وارد کنید", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else {
                        passwordBoolean = true;
                    }
                }
                if (usernameBoolean & passwordBoolean) {
                    dataProgressDialog.show();
                    userApi = new UserApi(LoginActivity.this, context);
                    userApi.setUser(username);
                    userApi.setPassword(password);
                    userApi.GetLogin(context);
                }
                break;
            case R.id.login_register_textView:
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
                break;
        }


    }
}

