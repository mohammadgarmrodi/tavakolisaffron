package ir.dayasoft.tavakolisaffron.model;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.repository.CartRepo;
import ir.dayasoft.tavakolisaffron.repository.CategoryRepo;

/**
 * Created by mohammad on 01/23/2017.
 */

public class Cart {

    private Integer cartID;
    private Integer FK_ProductID;
    private Integer Count;
    private KomoriProduct komoriProduct;
    private Category category;

    public Integer getCartID() {
        return cartID;
    }

    public void setCartID(Integer cartID) {
        this.cartID = cartID;
    }

    public Integer getFK_ProductID() {
        return FK_ProductID;
    }

    public void setFK_ProductID(Integer FK_ProductID) {
        this.FK_ProductID = FK_ProductID;
    }

    public Integer getCount() {
        return Count;
    }

    public void setCount(Integer count) {
        Count = count;
    }

    public KomoriProduct getKomoriProduct() {
        return komoriProduct;
    }

    public void setKomoriProduct(KomoriProduct komoriProduct) {
        this.komoriProduct = komoriProduct;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public static int Insert(Context context, List<Cart> cartList) {

        CartRepo cartRepo = new CartRepo(context);
        int cnt = 0;
        try {
            cartRepo.open();
            cnt = cartRepo.insert(cartList);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            cartRepo.close();
        }

        return cnt;
    }

    public Cart Insert(Context context, Cart cart) {
        Cart result = null;
        CartRepo cartRepo = new CartRepo(context);
        try {
            cartRepo.open();
            result = cartRepo.Insert(cart);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            cartRepo.close();
        }
        return result;
    }

    public static List<Cart> Get(Context context) {

        List<Cart> appList = new ArrayList<>();
        CartRepo cartRepo = new CartRepo(context);

        try {
            cartRepo.open();
            appList = cartRepo.Get();
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            cartRepo.close();
        }

        return appList;
    }

    static public Cart Get(Context context, int productID) {
        Cart cart = new Cart();
        CartRepo cartRepo = new CartRepo(context);

        try {
            cartRepo.open();
            cart = cartRepo.Get(productID);

        } catch (Exception e) {


        } finally {
            cartRepo.close();
        }
        return cart;
    }

    public Void Delete(Context context, int cartID) {
        CartRepo cartRepo = new CartRepo(context);
        try {
            cartRepo.open();
            cartRepo.Delete(cartID);
        } catch (Exception e) {

        } finally {
            cartRepo.close();
        }
        return null;
    }

    public Void Clear(Context context) {

        CartRepo cartRepo = new CartRepo(context);
        try {
            cartRepo.open();
            cartRepo.clear();
        } catch (Exception e) {


        } finally {
            cartRepo.close();
        }
        return null;
    }

    public int Update(Context context, Cart cart) {

        CartRepo cartRepo = new CartRepo(context);
        int numRowEffected = 0;
        try {
            cartRepo.open();
            numRowEffected = cartRepo.Update(cart);


        } catch (Exception e) {

        } finally {
            cartRepo.close();
        }

        return numRowEffected;
    }

    public static List<Cart> GetCartList(Context context) {

        List<Cart> appList = new ArrayList<>();
        CartRepo cartRepo = new CartRepo(context);

        try {
            cartRepo.open();
            appList = cartRepo.GetCartList();
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            cartRepo.close();
        }

        return appList;
    }

    static public Cart GetCart(Context context) {
        Cart cart = new Cart();
        CartRepo cartRepo = new CartRepo(context);

        try {
            cartRepo.open();
            cart = cartRepo.GetCart();

        } catch (Exception e) {


        } finally {
            cartRepo.close();
        }
        return cart;
    }

    static public Cart GetSum(Context context) {

        Cart cart = new Cart();
        CartRepo cartRepo = new CartRepo(context);

        try {
            cartRepo.open();
            cart = cartRepo.GetSum();

        } catch (Exception e) {


        } finally {
            cartRepo.close();
        }
        return cart;
    }

    public Integer InsertCart(Context context, Cart cart) {
        Integer result = null;
        CartRepo cartRepo = new CartRepo(context);
        try {
            cartRepo.open();
            result = cartRepo.InsertCart(cart);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            cartRepo.close();
        }
        return result;
    }

    public static List<Cart> GetCartListByCategory(Context context,int categoryID) {

        List<Cart> appList = new ArrayList<>();
        CartRepo cartRepo = new CartRepo(context);

        try {
            cartRepo.open();
            appList = cartRepo.GetCartListByCategory(categoryID);
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            cartRepo.close();
        }

        return appList;
    }



}
