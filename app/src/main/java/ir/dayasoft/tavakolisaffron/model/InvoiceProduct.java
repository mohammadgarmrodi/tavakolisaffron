package ir.dayasoft.tavakolisaffron.model;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.repository.CartRepo;
import ir.dayasoft.tavakolisaffron.repository.InvoiceItemRepo;
import ir.dayasoft.tavakolisaffron.repository.InvoiceProductRepo;
import ir.dayasoft.tavakolisaffron.repository.InvoiceRepo;

/**
 * Created by mohammad on 01/22/2017.
 */

public class InvoiceProduct {

    private int InvoiceProductID;
    private int FK_InvoiceID;
    private int FK_ProductID;
    private int Count;
    private String Description;
    private int Status;
    private int Type;
    private Boolean IsDelete;
    private String UpdateDate;
    private String CreateDate;
    private KomoriProduct komoriProduct;


    public KomoriProduct getKomoriProduct() {
        return komoriProduct;
    }

    public void setKomoriProduct(KomoriProduct komoriProduct) {
        this.komoriProduct = komoriProduct;
    }

    public int getInvoiceProductID() {
        return InvoiceProductID;
    }

    public void setInvoiceProductID(int invoiceProductID) {
        InvoiceProductID = invoiceProductID;
    }

    public int getFK_InvoiceID() {
        return FK_InvoiceID;
    }

    public void setFK_InvoiceID(int FK_InvoiceID) {
        this.FK_InvoiceID = FK_InvoiceID;
    }

    public int getFK_ProductID() {
        return FK_ProductID;
    }

    public void setFK_ProductID(int FK_ProductID) {
        this.FK_ProductID = FK_ProductID;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public Boolean getDelete() {
        return IsDelete;
    }

    public void setDelete(Boolean delete) {
        IsDelete = delete;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        UpdateDate = updateDate;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }


    public static int Insert(Context context, List<InvoiceProduct> invoiceProductList) {

        InvoiceProductRepo invoiceProductRepo = new InvoiceProductRepo(context);
        int cnt = 0;
        try {
            invoiceProductRepo.open();
            cnt = invoiceProductRepo.insert(invoiceProductList);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            invoiceProductRepo.close();
        }

        return cnt;
    }

    public InvoiceProduct Insert(Context context, InvoiceProduct invoiceProduct) {
        InvoiceProduct result = null;
        InvoiceProductRepo invoiceProductRepo = new InvoiceProductRepo(context);
        try {
            invoiceProductRepo.open();
            result = invoiceProductRepo.Insert(invoiceProduct);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            invoiceProductRepo.close();
        }
        return result;
    }

    public static List<PreFactor> GetAnimalInvoiceProduct(Context context, int FK_InvoiceID) {

        List<PreFactor> appList = new ArrayList<>();
        InvoiceProductRepo invoiceProductRepo = new InvoiceProductRepo(context);

        try {
            invoiceProductRepo.open();
            appList = invoiceProductRepo.GetAnimalInvoiceProduct(FK_InvoiceID);
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            invoiceProductRepo.close();
        }

        return appList;
    }

    public static List<PreFactor> GetVegetableInvoiceProduct(Context context, int FK_InvoiceID) {

        List<PreFactor> appList = new ArrayList<>();
        InvoiceProductRepo invoiceProductRepo = new InvoiceProductRepo(context);

        try {
            invoiceProductRepo.open();
            appList = invoiceProductRepo.GetVegetableInvoiceProduct(FK_InvoiceID);
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            invoiceProductRepo.close();
        }

        return appList;
    }

    static public InvoiceProduct Get(Context context, int invoiceProductID) {
        InvoiceProduct invoiceProduct = new InvoiceProduct();
        InvoiceProductRepo invoiceProductRepo = new InvoiceProductRepo(context);

        try {
            invoiceProductRepo.open();
            invoiceProduct = invoiceProductRepo.Get(invoiceProductID);

        } catch (Exception e) {


        } finally {
            invoiceProductRepo.close();
        }
        return invoiceProduct;
    }

    public Void Delete(Context context, int invoiceProductID) {
        InvoiceProductRepo invoiceProductRepo = new InvoiceProductRepo(context);
        try {
            invoiceProductRepo.open();
            invoiceProductRepo.Delete(invoiceProductID);
        } catch (Exception e) {

        } finally {
            invoiceProductRepo.close();
        }
        return null;
    }

    public Void Clear(Context context) {

        InvoiceProductRepo invoiceProductRepo = new InvoiceProductRepo(context);
        try {
            invoiceProductRepo.open();
            invoiceProductRepo.clear();
        } catch (Exception e) {


        } finally {
            invoiceProductRepo.close();
        }
        return null;
    }

    public int Update(Context context, InvoiceProduct invoiceProduct) {

        InvoiceProductRepo invoiceProductRepo = new InvoiceProductRepo(context);
        int numRowEffected = 0;
        try {
            invoiceProductRepo.open();
            numRowEffected = invoiceProductRepo.Update(invoiceProduct);


        } catch (Exception e) {

        } finally {
            invoiceProductRepo.close();
        }

        return numRowEffected;
    }


    public static List<InvoiceProduct> GetListInvoice(Context context, int invoiceProductID) {

        List<InvoiceProduct> appList = new ArrayList<>();
        InvoiceProductRepo invoiceProductRepo = new InvoiceProductRepo(context);

        try {
            invoiceProductRepo.open();
            appList = invoiceProductRepo.GetListInvoice(invoiceProductID);
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            invoiceProductRepo.close();
        }

        return appList;
    }

    static public PreFactor GetSum(Context context, int InvoiceID) {

        PreFactor preFactor = new PreFactor();
        InvoiceProductRepo invoiceProductRepo = new InvoiceProductRepo(context);

        try {
            invoiceProductRepo.open();
            preFactor = invoiceProductRepo.GetSum(InvoiceID);

        } catch (Exception e) {


        } finally {
            invoiceProductRepo.close();
        }
        return preFactor;
    }

    public static InvoiceProduct GetLast(Context context) {

        InvoiceProduct invoiceProduct = new InvoiceProduct();
        InvoiceProductRepo invoiceProductRepo = new InvoiceProductRepo(context);

        try {
            invoiceProductRepo.open();
            invoiceProduct = invoiceProductRepo.GetLast();
        } catch (Exception e) {

            e.getMessage();

        } finally {
            invoiceProductRepo.close();
        }

        return invoiceProduct;


    }

}
