package ir.dayasoft.tavakolisaffron.model;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.repository.CategoryRepo;

/**
 * Created by mohammad on 01/19/2017.
 */

public class Category {

    private int CategoryID;
    private String Name;
    private int ParentID;
    private String UrlPicture;
    private Boolean IsActive;
    private int Status;
    private int Type;
    private Boolean IsDelete;
    private String UpdateDate;
    private String CreateDate;
    private Integer Orders;

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int categoryID) {
        CategoryID = categoryID;
    }

    public String getName() {
        return Name;
    }

    public Integer getOrders() {
        return Orders;
    }

    public void setOrders(Integer orders) {
        Orders = orders;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getParentID() {
        return ParentID;
    }

    public void setParentID(int parentID) {
        ParentID = parentID;
    }

    public String getUrlPicture() {
        return UrlPicture;
    }

    public void setUrlPicture(String urlPicture) {
        UrlPicture = urlPicture;
    }

    public Boolean getActive() {
        return IsActive;
    }

    public void setActive(Boolean active) {
        IsActive = active;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public Boolean getDelete() {
        return IsDelete;
    }

    public void setDelete(Boolean delete) {
        IsDelete = delete;
    }

    public String getUpdateDate() {
        return UpdateDate= UpdateDate.replace(" ", "T");
    }

    public void setUpdateDate(String updateDate) {
        UpdateDate = updateDate;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }


    public static int Insert(Context context, List<Category> categoryList) {

        CategoryRepo categoryRepo = new CategoryRepo(context);
        int cnt = 0;
        try {
            categoryRepo.open();
            cnt = categoryRepo.insert(categoryList);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            categoryRepo.close();
        }

        return cnt;
    }

    public Category Insert(Context context, Category category) {
        Category result = null;
        CategoryRepo categoryRepo = new CategoryRepo(context);
        try {
            categoryRepo.open();
            result = categoryRepo.Insert(category);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            categoryRepo.close();
        }
        return result;
    }

    public static List<Category> Get(Context context) {

        List<Category> appList = new ArrayList<>();
        CategoryRepo categoryRepo = new CategoryRepo(context);

        try {
            categoryRepo.open();
            appList = categoryRepo.Get();
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            categoryRepo.close();
        }

        return appList;
    }

    static public Category Get(Context context, int categoryID) {
        Category category = new Category();
        CategoryRepo categoryRepo = new CategoryRepo(context);

        try {
            categoryRepo.open();
            category = categoryRepo.Get(categoryID);

        } catch (Exception e) {


        } finally {
            categoryRepo.close();
        }
        return category;
    }

    public Void Delete(Context context, int CategoryID) {
        CategoryRepo categoryRepo = new CategoryRepo(context);
        try {
            categoryRepo.open();
            categoryRepo.Delete(CategoryID);
        } catch (Exception e) {

        } finally {
            categoryRepo.close();
        }
        return null;
    }

    public Void Clear(Context context) {

        CategoryRepo categoryRepo = new CategoryRepo(context);
        try {
            categoryRepo.open();
            categoryRepo.clear();
        } catch (Exception e) {


        } finally {
            categoryRepo.close();
        }
        return null;
    }

    public int Update(Context context, Category category) {

        CategoryRepo categoryRepo = new CategoryRepo(context);
        int numRowEffected = 0;
        try {
            categoryRepo.open();
            numRowEffected = categoryRepo.Update(category);


        } catch (Exception e) {

        } finally {
            categoryRepo.close();
        }

        return numRowEffected;
    }

    public static List<Category> GetCategoryByParent(Context context, int ID) {
        CategoryRepo categoryRepo = new CategoryRepo(context);
        List<Category> categoryList = new ArrayList<>();
        try {
            categoryRepo.open();
            categoryList = categoryRepo.GetCategoryByParent(ID);

        } catch (Exception e) {
            e.getMessage();
        } finally {
            categoryRepo.close();
        }
        return categoryList;
    }

    public static Category GetLast(Context context) {

        Category category = new Category();
        CategoryRepo categoryRepo = new CategoryRepo(context);

        try {
            categoryRepo.open();
            category = categoryRepo.GetLast();
        } catch (Exception e) {

            e.getMessage();

        } finally {
            categoryRepo.close();
        }

        return category;


    }





}
