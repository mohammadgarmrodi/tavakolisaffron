package ir.dayasoft.tavakolisaffron.model;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.repository.KomoriProductRepo;

/**
 * Created by mohammad on 01/22/2017.
 */

public class KomoriProduct {


    private int KomoriProductID;
    private int FK_ProductID;
    private int FK_CategoryID;
    private String Name;
    private int Price;
    private String Description;
    private int ProductInventory;
    private int Status;
    private String Weight;
    private int DiscountPrice;
    private int Type;
    private int PrePayment;
    private Boolean IsDelete;
    private String UpdateDate;
    private String CreateDate;
    private Cart cart;
    private Boolean isShowSubDetail = false;
    private List<Cart> cartList;
    private String ImageUrl;
    private String ImageThumbUrl;


    public Boolean getShowSubDetail() {
        return isShowSubDetail;
    }

    public void setShowSubDetail(Boolean showSubDetail) {
        isShowSubDetail = showSubDetail;
    }

    public Cart getCart() {
        return cart;
    }

    public List<Cart> getCartList() {
        return cartList;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getImageThumbUrl() {
        return ImageThumbUrl;
    }

    public void setImageThumbUrl(String imageThumbUrl) {
        ImageThumbUrl = imageThumbUrl;
    }

    public void setCartList(List<Cart> cartList) {
        this.cartList = cartList;
    }

    public void setCart(Cart cart) {
        this.cart = cart;

    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public int getKomoriProductID() {
        return KomoriProductID;
    }

    public void setKomoriProductID(int komoriProductID) {
        KomoriProductID = komoriProductID;
    }

    public int getFK_ProductID() {
        return FK_ProductID;
    }

    public void setFK_ProductID(int FK_ProductID) {
        this.FK_ProductID = FK_ProductID;
    }

    public int getFK_CategoryID() {
        return FK_CategoryID;
    }

    public void setFK_CategoryID(int FK_CategoryID) {
        this.FK_CategoryID = FK_CategoryID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getProductInventory() {
        return ProductInventory;
    }

    public void setProductInventory(int productInventory) {
        ProductInventory = productInventory;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public int getDiscountPrice() {
        return DiscountPrice;
    }

    public void setDiscountPrice(int discountPrice) {
        DiscountPrice = discountPrice;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public int getPrePayment() {
        return PrePayment;
    }

    public void setPrePayment(int prePayment) {
        PrePayment = prePayment;
    }

    public Boolean getDelete() {
        return IsDelete;
    }

    public void setDelete(Boolean delete) {
        IsDelete = delete;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        UpdateDate = updateDate;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }


    public static int Insert(Context context, List<KomoriProduct> komoriProductList) {

        KomoriProductRepo komoriProductRepo = new KomoriProductRepo(context);
        int cnt = 0;
        try {
            komoriProductRepo.open();
            cnt = komoriProductRepo.insert(komoriProductList);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            komoriProductRepo.close();
        }

        return cnt;
    }

    public KomoriProduct Insert(Context context, KomoriProduct komoriProduct) {
        KomoriProduct result = null;
        KomoriProductRepo komoriProductRepo = new KomoriProductRepo(context);
        try {
            komoriProductRepo.open();
            result = komoriProductRepo.Insert(komoriProduct);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            komoriProductRepo.close();
        }
        return result;
    }

    public static List<KomoriProduct> Get(Context context) {

        List<KomoriProduct> appList = new ArrayList<>();
        KomoriProductRepo komoriProductRepo = new KomoriProductRepo(context);

        try {
            komoriProductRepo.open();
            appList = komoriProductRepo.Get();
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            komoriProductRepo.close();
        }

        return appList;
    }


    static public KomoriProduct Get(Context context, int komoriProductID) {
        KomoriProduct komoriProduct = new KomoriProduct();
        KomoriProductRepo komoriProductRepo = new KomoriProductRepo(context);

        try {
            komoriProductRepo.open();
            komoriProduct = komoriProductRepo.Get(komoriProductID);

        } catch (Exception e) {


        } finally {
            komoriProductRepo.close();
        }
        return komoriProduct;
    }

    public Void Delete(Context context, int komoriProductID) {
        KomoriProductRepo komoriProductRepo = new KomoriProductRepo(context);
        try {
            komoriProductRepo.open();
            komoriProductRepo.Delete(komoriProductID);
        } catch (Exception e) {

        } finally {
            komoriProductRepo.close();
        }
        return null;
    }

    public static List<KomoriProduct> GetProudctByCategory(Context context, int categoryID) {

        List<KomoriProduct> appList = new ArrayList<>();
        KomoriProductRepo komoriProductRepo = new KomoriProductRepo(context);

        try {
            komoriProductRepo.open();
            appList = komoriProductRepo.GetProductByCategory(categoryID);
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            komoriProductRepo.close();
        }

        return appList;
    }

    public Void Clear(Context context) {

        KomoriProductRepo komoriProductRepo = new KomoriProductRepo(context);
        try {
            komoriProductRepo.open();
            komoriProductRepo.clear();
        } catch (Exception e) {


        } finally {
            komoriProductRepo.close();
        }
        return null;
    }

    public int Update(Context context, KomoriProduct komoriProduct) {

        KomoriProductRepo komoriProductRepo = new KomoriProductRepo(context);
        int numRowEffected = 0;
        try {
            komoriProductRepo.open();
            numRowEffected = komoriProductRepo.Update(komoriProduct);


        } catch (Exception e) {

        } finally {
            komoriProductRepo.close();
        }

        return numRowEffected;
    }

    public static List<KomoriProduct> GetProductByPrice(Context context, int categoryID) {

        List<KomoriProduct> appList = new ArrayList<>();
        KomoriProductRepo komoriProductRepo = new KomoriProductRepo(context);

        try {
            komoriProductRepo.open();
            appList = komoriProductRepo.GetProductByPrice(categoryID);
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            komoriProductRepo.close();
        }

        return appList;
    }

    public static List<KomoriProduct> GetProductByWeight(Context context, int categoryID) {

        List<KomoriProduct> appList = new ArrayList<>();
        KomoriProductRepo komoriProductRepo = new KomoriProductRepo(context);

        try {
            komoriProductRepo.open();
            appList = komoriProductRepo.GetProductByWeight(categoryID);
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            komoriProductRepo.close();
        }

        return appList;
    }


    public static List<KomoriProduct> GetProductByCompany(Context context, int categoryID) {

        List<KomoriProduct> appList = new ArrayList<>();
        KomoriProductRepo komoriProductRepo = new KomoriProductRepo(context);

        try {
            komoriProductRepo.open();
            appList = komoriProductRepo.GetProductByCompany(categoryID);
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            komoriProductRepo.close();
        }

        return appList;
    }

    public static KomoriProduct GetLast(Context context) {

        KomoriProduct komoriProduct = new KomoriProduct();
        KomoriProductRepo komoriProductRepo = new KomoriProductRepo(context);

        try {
            komoriProductRepo.open();
            komoriProduct = komoriProductRepo.GetLast();
        } catch (Exception e) {

            e.getMessage();

        } finally {
            komoriProductRepo.close();
        }

        return komoriProduct;


    }


}