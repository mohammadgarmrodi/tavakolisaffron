package ir.dayasoft.tavakolisaffron.model;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.repository.ProductRepo;
import ir.dayasoft.tavakolisaffron.repository.UserRepo;

/**
 * Created by mohammad on 12/31/2016.
 */

public class User {
    public static final int STEP_FIRST_LOGIN = 0;
    public static final int STEP_FINISH_LOGIN = 1;

    private Long UserID;
    private int FK_RollID;
    private String FcmToken;
    private int FK_ProfileImageID;
    private String FirstName;
    private String LastName;
    private String NationalCode;
    private Boolean IsConfirmed;
    private Boolean IsSuspended;
    private String UserName;
    private String Email;
    private String Phone1;
    private String Phone2;
    private String CellPhone;
    private String Password;
    private String FirstLogin;
    private String LastLogin;
    private String LastLoginIp;
    private String WebKey;
    private Boolean AllowReceiveMessage;
    private Boolean WantReceiveMessage;
    private Boolean AllowSendMessage;
    private Boolean WantSendMessage;
    private Boolean IsOnline;
    private String Address;
    private String Description;
    private String TokenExpireDate;
    private String Token;
    private String BirthDay;
    private Integer Status;
    private Integer Type;
    private Integer IsDeleted;
    private String CreateDate;
    private String UpdateDate;


    public Long getUserID() {
        return UserID;
    }

    public void setUserID(Long userID) {
        UserID = userID;
    }

    public int getFK_RollID() {
        return FK_RollID;
    }

    public void setFK_RollID(int FK_RollID) {
        this.FK_RollID = FK_RollID;
    }

    public int getFK_ProfileImageID() {
        return FK_ProfileImageID;
    }

    public String getFcmToken() {
        return FcmToken;
    }

    public void setFcmToken(String fcmToken) {
        FcmToken = fcmToken;
    }

    public void setFK_ProfileImageID(int FK_ProfileImageID) {
        this.FK_ProfileImageID = FK_ProfileImageID;

    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getNationalCode() {
        return NationalCode;
    }

    public void setNationalCode(String nationalCode) {
        NationalCode = nationalCode;
    }

    public Boolean getConfirmed() {
        return IsConfirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        IsConfirmed = confirmed;
    }

    public Boolean getSuspended() {
        return IsSuspended;
    }

    public void setSuspended(Boolean suspended) {
        IsSuspended = suspended;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhone1() {
        return Phone1;
    }

    public void setPhone1(String phone1) {
        Phone1 = phone1;
    }

    public String getPhone2() {
        return Phone2;
    }

    public void setPhone2(String phone2) {
        Phone2 = phone2;
    }

    public String getCellPhone() {
        return CellPhone;
    }

    public void setCellPhone(String cellPhone) {
        CellPhone = cellPhone;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getFirstLogin() {
        return FirstLogin;
    }

    public void setFirstLogin(String firstLogin) {
        FirstLogin = firstLogin;
    }

    public String getLastLogin() {
        return LastLogin;
    }

    public void setLastLogin(String lastLogin) {
        LastLogin = lastLogin;
    }

    public String getLastLoginIp() {
        return LastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        LastLoginIp = lastLoginIp;
    }

    public String getWebKey() {
        return WebKey;
    }

    public void setWebKey(String webKey) {
        WebKey = webKey;
    }

    public Boolean getAllowReceiveMessage() {
        return AllowReceiveMessage;
    }

    public void setAllowReceiveMessage(Boolean allowReceiveMessage) {
        AllowReceiveMessage = allowReceiveMessage;
    }

    public Boolean getWantReceiveMessage() {
        return WantReceiveMessage;
    }

    public void setWantReceiveMessage(Boolean wantReceiveMessage) {
        WantReceiveMessage = wantReceiveMessage;
    }

    public Boolean getAllowSendMessage() {
        return AllowSendMessage;
    }

    public void setAllowSendMessage(Boolean allowSendMessage) {
        AllowSendMessage = allowSendMessage;
    }

    public Boolean getWantSendMessage() {
        return WantSendMessage;
    }

    public void setWantSendMessage(Boolean wantSendMessage) {
        WantSendMessage = wantSendMessage;
    }

    public Boolean getOnline() {
        return IsOnline;
    }

    public void setOnline(Boolean online) {
        IsOnline = online;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getTokenExpireDate() {
        return TokenExpireDate;
    }

    public void setTokenExpireDate(String tokenExpireDate) {
        TokenExpireDate = tokenExpireDate;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getBirthDay() {
        return BirthDay;
    }

    public void setBirthDay(String birthDay) {
        BirthDay = birthDay;
    }

    public Integer getStatus() {
        return Status;
    }

    public void setStatus(Integer status) {
        Status = status;
    }

    public Integer getType() {
        return Type;
    }

    public void setType(Integer type) {
        Type = type;
    }

    public Integer getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        IsDeleted = isDeleted;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }


    public void setUpdateDate(String updateDate) {
        UpdateDate = updateDate;
    }


    public static int Insert(Context context, List<User> userList) {

        UserRepo userRepo = new UserRepo(context);
        int cnt = 0;
        try {
            userRepo.open();
            cnt = userRepo.insert(userList);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            userRepo.close();
        }

        return cnt;
    }

    public static User Insert(Context context, User user) {
        User result = null;
        UserRepo userRepo = new UserRepo(context);
        try {
            userRepo.open();
            result = userRepo.Insert(user);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            userRepo.close();
        }
        return result;
    }

    public static List<User> Get(Context context) {

        List<User> appList = new ArrayList<>();
        UserRepo userRepo = new UserRepo(context);

        try {
            userRepo.open();
            appList = userRepo.Get();
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            userRepo.close();
        }

        return appList;
    }

    public static User GetUser(Context context) {

        User user = new User();
        UserRepo userRepo = new UserRepo(context);

        try {
            userRepo.open();
            user = userRepo.GetUser();

        } catch (Exception e) {


        } finally {
            userRepo.close();
        }
        return user;
    }

    static public User Get(Context context, long userID) {
        User user = new User();
        UserRepo userRepo = new UserRepo(context);

        try {
            userRepo.open();
            user = userRepo.Get(userID);

        } catch (Exception e) {


        } finally {
            userRepo.close();
        }
        return user;
    }

    public Void Delete(Context context, Long userID) {
        UserRepo userRepo = new UserRepo(context);
        try {
            userRepo.open();
            userRepo.Delete(userID);
        } catch (Exception e) {

        } finally {
            userRepo.close();
        }
        return null;
    }

    public Void Clear(Context context) {

        UserRepo userRepo = new UserRepo(context);
        try {
            userRepo.open();
            userRepo.clear();
        } catch (Exception e) {


        } finally {
            userRepo.close();
        }
        return null;
    }

    public int Update(Context context, User user) {

        UserRepo userRepo = new UserRepo(context);
        int numRowEffected = 0;
        try {
            userRepo.open();
            numRowEffected = userRepo.Update(user);


        } catch (Exception e) {

        } finally {
            userRepo.close();
        }

        return numRowEffected;
    }


}
