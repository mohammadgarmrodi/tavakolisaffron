package ir.dayasoft.tavakolisaffron.model;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.repository.CategoryRepo;
import ir.dayasoft.tavakolisaffron.repository.KomoriProductRepo;
import ir.dayasoft.tavakolisaffron.repository.ProductRepo;

/**
 * Created by mohammad on 01/18/2017.
 */

public class Product {

    private int ProductID;
    private int Fk_CategoryID;
    private String Name;
    private String Description;
    private int Price;
    private int DiscountPrice;
    private String ManufactureDate;
    private String ExpirationDate;
    private String SerialNumber;
    private Boolean IsDelete;
    private int Status;
    private int Type;
    private String UpdateDate;
    private String CreateDate;


    public int getProductID() {
        return ProductID;
    }

    public void setProductID(int productID) {
        ProductID = productID;
    }

    public int getFk_CategoryID() {
        return Fk_CategoryID;
    }

    public void setFk_CategoryID(int fk_CategoryID) {
        Fk_CategoryID = fk_CategoryID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public int getDiscountPrice() {
        return DiscountPrice;
    }

    public void setDiscountPrice(int discountPrice) {
        DiscountPrice = discountPrice;
    }

    public String getManufactureDate() {
        return ManufactureDate;
    }

    public void setManufactureDate(String manufactureDate) {
        ManufactureDate = manufactureDate;
    }

    public String getExpirationDate() {
        return ExpirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        ExpirationDate = expirationDate;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }

    public Boolean getDelete() {
        return IsDelete;
    }

    public void setDelete(Boolean delete) {
        IsDelete = delete;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        UpdateDate = updateDate;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }



    public static int Insert(Context context, List<Product> productList) {

        ProductRepo productRepo = new ProductRepo(context);
        int cnt = 0;
        try {
            productRepo.open();
            cnt = productRepo.insert(productList);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            productRepo.close();
        }

        return cnt;
    }

    public Product Insert(Context context, Product product) {
        Product result = null;
        ProductRepo productRepo = new ProductRepo(context);
        try {
            productRepo.open();
            result = productRepo.Insert(product);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            productRepo.close();
        }
        return result;
    }

    public static List<Product> Get(Context context) {

        List<Product> appList = new ArrayList<>();
        ProductRepo productRepo = new ProductRepo(context);

        try {
            productRepo.open();
            appList = productRepo.Get();
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            productRepo.close();
        }

        return appList;
    }

    static public Product Get(Context context, int productID) {
        Product product = new Product();
        ProductRepo productRepo = new ProductRepo(context);

        try {
            productRepo.open();
            product = productRepo.Get(productID);

        } catch (Exception e) {


        } finally {
            productRepo.close();
        }
        return product;
    }

    public Void Delete(Context context, int productID) {
        ProductRepo productRepo = new ProductRepo(context);
        try {
            productRepo.open();
            productRepo.Delete(productID);
        } catch (Exception e) {

        } finally {
            productRepo.close();
        }
        return null;
    }

    public Void Clear(Context context) {

        ProductRepo productRepo = new ProductRepo(context);
        try {
            productRepo.open();
            productRepo.clear();
        } catch (Exception e) {


        } finally {
            productRepo.close();
        }
        return null;
    }

    public int Update(Context context, Product product) {

        ProductRepo productRepo = new ProductRepo(context);
        int numRowEffected = 0;
        try {
            productRepo.open();
            numRowEffected = productRepo.Update(product);


        } catch (Exception e) {

        } finally {
            productRepo.close();
        }

        return numRowEffected;
    }

    public static Product GetLast(Context context) {

        Product product = new Product();
        ProductRepo productRepo = new ProductRepo(context);

        try {
            productRepo.open();
            product = productRepo.GetLast();
        } catch (Exception e) {

            e.getMessage();

        } finally {
            productRepo.close();
        }

        return product;


    }

}
