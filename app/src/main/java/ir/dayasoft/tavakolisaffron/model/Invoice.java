package ir.dayasoft.tavakolisaffron.model;

import android.content.Context;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import ir.dayasoft.tavakolisaffron.repository.InvoiceRepo;

/**
 * Created by mohammad on 01/22/2017.
 */

public class Invoice {


    private int InvoiceID;
    private Long Fk_UserID;
    private String Code;
    private int Price;
    private int DiscountPrice;
    private String Description;
    private int Status;
    private int Type;
    private Boolean IsDelete;
    private String PaymentDeadline;
    private String paymentDate;
    private String UpdateDate;
    private String CreateDate;
    private String Address;
    private String phone;
    private Integer invoiceItemStatus;
    private int Count;
    List<Cart> cartList;
    private Integer invoiceItemPaymentStatus;
    Context context;
    InvoiceItem InvoiceItem;

    public Integer getInvoiceItemStatus() {
        return invoiceItemStatus;
    }

    public void setInvoiceItemStatus(Integer invoiceItemStatus) {
        this.invoiceItemStatus = invoiceItemStatus;
    }


    public Integer getInvoiceItemPaymentStatus() {
        return invoiceItemPaymentStatus;
    }

    public void setInvoiceItemPaymentStatus(Integer invoiceItemPaymentStatus) {
        this.invoiceItemPaymentStatus = invoiceItemPaymentStatus;
    }

    public ir.dayasoft.tavakolisaffron.model.InvoiceItem getInvoiceItem() {
        return InvoiceItem;
    }

    public int getInvoiceID() {
        return InvoiceID;
    }

    public void setInvoiceID(int invoiceID) {
        InvoiceID = invoiceID;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Cart> getCartList() {
        return cartList;
    }

    public void setCartList(List<Cart> cartList) {
        this.cartList = cartList;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }


    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public int getDiscountPrice() {
        return DiscountPrice;
    }

    public void setDiscountPrice(int discountPrice) {
        DiscountPrice = discountPrice;
    }

    public Long getFk_UserID() {
        return Fk_UserID;
    }

    public void setFk_UserID(Long fk_UserID) {
        Fk_UserID = fk_UserID;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public Boolean getDelete() {
        return IsDelete;
    }

    public void setDelete(Boolean delete) {
        IsDelete = delete;
    }

    public String getPaymentDeadline() {
        return PaymentDeadline;
    }

    public void setPaymentDeadline(String paymentDeadline) {
        PaymentDeadline = paymentDeadline;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        UpdateDate = updateDate.replace(" ","T");
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public static int Insert(Context context, List<Invoice> invoiceList) {

        InvoiceRepo invoiceRepo = new InvoiceRepo(context);
        int cnt = 0;
        try {
            invoiceRepo.open();
            cnt = invoiceRepo.insert(invoiceList);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            invoiceRepo.close();
        }

        return cnt;
    }

    public Invoice Insert(Context context, Invoice invoice) {
        Invoice result = null;
        InvoiceRepo invoiceRepo = new InvoiceRepo(context);
        try {
            invoiceRepo.open();
            result = invoiceRepo.Insert(invoice);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            invoiceRepo.close();
        }
        return result;
    }


    public static List<Invoice> Get(Context context) {

        List<Invoice> appList = new ArrayList<>();
        InvoiceRepo invoiceRepo = new InvoiceRepo(context);

        try {
            invoiceRepo.open();
            appList = invoiceRepo.Get();
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            invoiceRepo.close();
        }

        return appList;
    }

    static public Invoice Get(Context context, int invoiceID) {
        Invoice invoice = new Invoice();
        InvoiceRepo invoiceRepo = new InvoiceRepo(context);

        try {
            invoiceRepo.open();
            invoice = invoiceRepo.Get(invoiceID);

        } catch (Exception e) {


        } finally {
            invoiceRepo.close();
        }
        return invoice;
    }

    static public Invoice IsExistByInvoiceID(Context context,int invoiceId) {
        Invoice invoice = new Invoice();
        InvoiceRepo invoiceRepo = new InvoiceRepo(context);

        try {
            invoiceRepo.open();
            invoice = invoiceRepo.IsExistByInvoiceID(invoiceId);
        } catch (Exception e) {
            e.getMessage();

        } finally {
            invoiceRepo.close();
        }
        return invoice;
    }


    public Void Delete(Context context, int invoiceID) {
        InvoiceRepo invoiceRepo = new InvoiceRepo(context);
        try {
            invoiceRepo.open();
            invoiceRepo.Delete(invoiceID);
        } catch (Exception e) {

        } finally {
            invoiceRepo.close();
        }
        return null;
    }

    public Void Clear(Context context) {

        InvoiceRepo invoiceRepo = new InvoiceRepo(context);
        try {
            invoiceRepo.open();
            invoiceRepo.clear();
        } catch (Exception e) {


        } finally {
            invoiceRepo.close();
        }
        return null;
    }

    public int Update(Context context, Invoice invoice) {

        InvoiceRepo invoiceRepo = new InvoiceRepo(context);
        int numRowEffected = 0;
        try {
            invoiceRepo.open();
            numRowEffected = invoiceRepo.Update(invoice);


        } catch (Exception e) {

        } finally {
            invoiceRepo.close();
        }

        return numRowEffected;
    }

    public static Invoice GetLast(Context context) {

        Invoice invoice = new Invoice();
        InvoiceRepo invoiceRepo = new InvoiceRepo(context);

        try {
            invoiceRepo.open();
            invoice = invoiceRepo.GetLast();
        } catch (Exception e) {

            e.getMessage();

        } finally {
            invoiceRepo.close();
        }

        return invoice;


    }

}
