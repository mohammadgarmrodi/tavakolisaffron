package ir.dayasoft.tavakolisaffron.model;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.repository.ProductImageRepo;
import ir.dayasoft.tavakolisaffron.repository.ProductRepo;

/**
 * Created by mohammad on 01/22/2017.
 */

public class ProductImage {

    private int ProductImageID;
    private int FK_KomoriProductID;
    private int FK_ImageID;
    private int Status;
    private int Type;
    private String UpdateDate;
    private String CreateDate;

    public int getProductImageID() {
        return ProductImageID;
    }

    public void setProductImageID(int productImageID) {
        ProductImageID = productImageID;
    }

    public int getFK_KomoriProductID() {
        return FK_KomoriProductID;
    }

    public void setFK_KomoriProductID(int FK_KomoriProductID) {
        this.FK_KomoriProductID = FK_KomoriProductID;
    }

    public int getFK_ImageID() {
        return FK_ImageID;
    }

    public void setFK_ImageID(int FK_ImageID) {
        this.FK_ImageID = FK_ImageID;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        UpdateDate = updateDate;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }


    public static int Insert(Context context, List<ProductImage> productImageList) {

        ProductImageRepo productImageRepo = new ProductImageRepo(context);
        int cnt = 0;
        try {
            productImageRepo.open();
            cnt = productImageRepo.insert(productImageList);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            productImageRepo.close();
        }

        return cnt;
    }

    public ProductImage Insert(Context context, ProductImage productImage) {
        ProductImage result = null;
        ProductImageRepo productImageRepo = new ProductImageRepo(context);
        try {
            productImageRepo.open();
            result = productImageRepo.Insert(productImage);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            productImageRepo.close();
        }
        return result;
    }

    public static List<ProductImage> Get(Context context) {

        List<ProductImage> appList = new ArrayList<>();
        ProductImageRepo productImageRepo = new ProductImageRepo(context);

        try {
            productImageRepo.open();
            appList = productImageRepo.Get();
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            productImageRepo.close();
        }

        return appList;
    }

    static public ProductImage Get(Context context, int productImageID) {
        ProductImage productImage = new ProductImage();
        ProductImageRepo productImageRepo = new ProductImageRepo(context);

        try {
            productImageRepo.open();
            productImage = productImageRepo.Get(productImageID);

        } catch (Exception e) {


        } finally {
            productImageRepo.close();
        }
        return productImage;
    }

    public Void Delete(Context context, int productImageID) {
        ProductImageRepo productImageRepo = new ProductImageRepo(context);
        try {
            productImageRepo.open();
            productImageRepo.Delete(productImageID);
        } catch (Exception e) {

        } finally {
            productImageRepo.close();
        }
        return null;
    }

    public Void Clear(Context context) {

        ProductImageRepo productImageRepo = new ProductImageRepo(context);
        try {
            productImageRepo.open();
            productImageRepo.clear();
        } catch (Exception e) {


        } finally {
            productImageRepo.close();
        }
        return null;
    }

    public int Update(Context context, ProductImage productImage) {

        ProductImageRepo productImageRepo = new ProductImageRepo(context);
        int numRowEffected = 0;
        try {
            productImageRepo.open();
            numRowEffected = productImageRepo.Update(productImage);


        } catch (Exception e) {

        } finally {
            productImageRepo.close();
        }

        return numRowEffected;
    }

}
