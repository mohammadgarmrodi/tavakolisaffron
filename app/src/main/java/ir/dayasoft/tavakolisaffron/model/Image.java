package ir.dayasoft.tavakolisaffron.model;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.repository.CategoryRepo;
import ir.dayasoft.tavakolisaffron.repository.ImageRepo;
import ir.dayasoft.tavakolisaffron.repository.InvoiceRepo;

/**
 * Created by mohammad on 12/31/2016.
 */

public class Image {
    private int ImageID;
    private String Url;
    private String ThumbUrl;
    private String Description;
    private String Alt;
    private Integer Orders;
    private Integer Status;
    private Integer Type;
    private Boolean IsDelete;
    private String ImageType;
    private String UpdateDate;
    private String CreateDate;

    public int getImageID() {
        return ImageID;
    }

    public void setImageID(int imageID) {
        ImageID = imageID;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getThumbUrl() {
        return ThumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        ThumbUrl = thumbUrl;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getAlt() {
        return Alt;
    }

    public void setAlt(String alt) {
        Alt = alt;
    }

    public Integer getOrders() {
        return Orders;
    }

    public void setOrders(Integer orders) {
        Orders = orders;
    }

    public Integer getStatus() {
        return Status;
    }

    public void setStatus(Integer status) {
        Status = status;
    }

    public Integer getType() {
        return Type;
    }

    public void setType(Integer type) {
        Type = type;
    }

    public Boolean getDelete() {
        return IsDelete;
    }

    public void setDelete(Boolean delete) {
        IsDelete = delete;
    }

    public String getImageType() {
        return ImageType;
    }

    public void setImageType(String imageType) {
        ImageType = imageType;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        UpdateDate = updateDate;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }


    public static int Insert(Context context, List<Image> imageList) {

        ImageRepo imageRepo = new ImageRepo(context);
        int cnt = 0;
        try {
            imageRepo.open();
            cnt = imageRepo.insert(imageList);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            imageRepo.close();
        }

        return cnt;
    }

    public Image Insert(Context context, Image image) {
        Image result = null;
        ImageRepo imageRepo = new ImageRepo(context);
        try {
            imageRepo.open();
            result = imageRepo.Insert(image);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            imageRepo.close();
        }
        return result;
    }

    public static List<Image> Get(Context context) {

        List<Image> appList = new ArrayList<>();
        ImageRepo imageRepo = new ImageRepo(context);

        try {
            imageRepo.open();
            appList = imageRepo.Get();
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            imageRepo.close();
        }

        return appList;
    }

    static public Image Get(Context context, int imageID) {
        Image image = new Image();
        ImageRepo imageRepo = new ImageRepo(context);

        try {
            imageRepo.open();
            image = imageRepo.Get(imageID);

        } catch (Exception e) {


        } finally {
            imageRepo.close();
        }
        return image;
    }

    public Void Delete(Context context, int imageID) {
        ImageRepo imageRepo = new ImageRepo(context);
        try {
            imageRepo.open();
            imageRepo.Delete(imageID);
        } catch (Exception e) {

        } finally {
            imageRepo.close();
        }
        return null;
    }

    public Void Clear(Context context) {

        ImageRepo imageRepo = new ImageRepo(context);
        try {
            imageRepo.open();
            imageRepo.clear();
        } catch (Exception e) {


        } finally {
            imageRepo.close();
        }
        return null;
    }

    public int Update(Context context, Image image) {

        ImageRepo imageRepo = new ImageRepo(context);
        int numRowEffected = 0;
        try {
            imageRepo.open();
            numRowEffected = imageRepo.Update(image);


        } catch (Exception e) {

        } finally {
            imageRepo.close();
        }

        return numRowEffected;
    }

    public static Image GetLast(Context context) {

        Image image = new Image();
        ImageRepo imageRepo = new ImageRepo(context);

        try {
            imageRepo.open();
            image = imageRepo.GetLast();
        } catch (Exception e) {

            e.getMessage();

        } finally {
            imageRepo.close();
        }

        return image;


    }

}

