package ir.dayasoft.tavakolisaffron.model;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mohammad on 12/27/2016.
 */

public class Result {

    Integer Code;
    String Message;
    Boolean IsError;
    String Value;

    public Integer getCode() {
        return Code;
    }

    public void setCode(Integer code) {
        Code = code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public Boolean getError() {
        return IsError;
    }

    public void setError(Boolean error) {
        IsError = error;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    static public Result jsonToObject(String response) {
        try {


            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates
            Gson gson = gsonBuilder.create();
            return Arrays.asList(gson.fromJson(response, Result.class)).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public List<Result> jsonToObjectArray(String response) {
        try {

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates

            Gson gson = gsonBuilder.create();
            return Arrays.asList(gson.fromJson(response, Result[].class));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    static public String ObjectArrayToJason(List<Result> resultList) {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd kk:mm:ss"); //Format of our JSON dates

            Gson gson = gsonBuilder.create();


            return gson.toJson(resultList);

        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }
    }
}
