package ir.dayasoft.tavakolisaffron.model;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ir.dayasoft.tavakolisaffron.repository.CategoryRepo;
import ir.dayasoft.tavakolisaffron.repository.InvoiceItemRepo;
import ir.dayasoft.tavakolisaffron.repository.InvoiceRepo;

/**
 * Created by mohammad on 01/22/2017.
 */

public class InvoiceItem {

    private int InvoiceItemID;
    private int FK_InvoiceID;
    private int Price;
    private String Code;
    private String BankName;
    private int PaymentStatus;
    private String Description;
    private int Status;
    private int Type;
    private Boolean IsDelete;
    private String PaymentDate;
    private String UpdateDate;
    private String CreateDate;

    public int getInvoiceItemID() {
        return InvoiceItemID;
    }

    public void setInvoiceItemID(int invoiceItemID) {
        InvoiceItemID = invoiceItemID;
    }

    public int getFK_InvoiceID() {
        return FK_InvoiceID;
    }

    public void setFK_InvoiceID(int FK_InvoiceID) {
        this.FK_InvoiceID = FK_InvoiceID;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public int getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(int paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public Boolean getDelete() {
        return IsDelete;
    }

    public void setDelete(Boolean delete) {
        IsDelete = delete;
    }

    public String getPaymentDate() {
        return PaymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.PaymentDate = paymentDate;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        UpdateDate = updateDate;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public static int Insert(Context context, List<InvoiceItem> invoiceItemList) {

        InvoiceItemRepo invoiceItemRepo = new InvoiceItemRepo(context);
        int cnt = 0;
        try {
            invoiceItemRepo.open();
            cnt = invoiceItemRepo.insert(invoiceItemList);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            invoiceItemRepo.close();
        }

        return cnt;
    }

    public InvoiceItem Insert(Context context, InvoiceItem invoiceItem) {
        InvoiceItem result = null;
        InvoiceItemRepo invoiceItemRepo = new InvoiceItemRepo(context);
        try {
            invoiceItemRepo.open();
            result = invoiceItemRepo.Insert(invoiceItem);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            invoiceItemRepo.close();
        }
        return result;
    }

    public static List<InvoiceItem> Get(Context context) {

        List<InvoiceItem> appList = new ArrayList<>();
        InvoiceItemRepo invoiceItemRepo = new InvoiceItemRepo(context);

        try {
            invoiceItemRepo.open();
            appList = invoiceItemRepo.Get();
        } catch (Exception e) {

            Log.i("GetListDownload", e.getMessage());

        } finally {
            invoiceItemRepo.close();
        }

        return appList;
    }

    static public InvoiceItem Get(Context context, int invoiceItemID) {
        InvoiceItem invoiceItem = new InvoiceItem();
        InvoiceItemRepo invoiceItemRepo = new InvoiceItemRepo(context);

        try {
            invoiceItemRepo.open();
            invoiceItem = invoiceItemRepo.Get(invoiceItemID);

        } catch (Exception e) {


        } finally {
            invoiceItemRepo.close();
        }
        return invoiceItem;
    }

    public Void Delete(Context context, int invoiceItemID) {
        InvoiceItemRepo invoiceitemRepo = new InvoiceItemRepo(context);
        try {
            invoiceitemRepo.open();
            invoiceitemRepo.Delete(invoiceItemID);
        } catch (Exception e) {

        } finally {
            invoiceitemRepo.close();
        }
        return null;
    }

    public Void Clear(Context context) {

        InvoiceItemRepo invoiceItemRepo = new InvoiceItemRepo(context);
        try {
            invoiceItemRepo.open();
            invoiceItemRepo.clear();
        } catch (Exception e) {


        } finally {
            invoiceItemRepo.close();
        }
        return null;
    }

    public int Update(Context context, InvoiceItem invoiceItem) {

        InvoiceItemRepo invoiceItemRepo = new InvoiceItemRepo(context);
        int numRowEffected = 0;
        try {
            invoiceItemRepo.open();
            numRowEffected = invoiceItemRepo.Update(invoiceItem);


        } catch (Exception e) {

        } finally {
            invoiceItemRepo.close();
        }

        return numRowEffected;
    }

    public static InvoiceItem GetLast(Context context) {

        InvoiceItem invoiceItem = new InvoiceItem();
        InvoiceItemRepo invoiceItemRepo = new InvoiceItemRepo(context);

        try {
            invoiceItemRepo.open();
            invoiceItem = invoiceItemRepo.GetLast();
        } catch (Exception e) {

            e.getMessage();

        } finally {
            invoiceItemRepo.close();
        }

        return invoiceItem;


    }

}
