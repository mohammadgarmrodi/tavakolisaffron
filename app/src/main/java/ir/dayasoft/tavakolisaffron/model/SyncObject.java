package ir.dayasoft.tavakolisaffron.model;

/**
 * Created by mohammad on 02/07/2017.
 */

public class SyncObject {
    private String ServerID;
    private String ClientID;
    private String Status;

    public String getServerID() {
        return ServerID;
    }

    public void setServerID(String serverID) {
        ServerID = serverID;
    }

    public String getClientID() {
        return ClientID;
    }

    public void setClientID(String clientID) {
        ClientID = clientID;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
