package ir.dayasoft.tavakolisaffron.model;

/**
 * Created by mohammad on 02/18/2017.
 */

public class FcmTokenUser {
    private long userID;
    private String token;

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
