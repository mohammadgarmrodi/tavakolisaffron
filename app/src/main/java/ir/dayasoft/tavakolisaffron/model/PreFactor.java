package ir.dayasoft.tavakolisaffron.model;

/**
 * Created by mohammad on 02/15/2017.
 */

public class PreFactor {
    String Name;
    Integer Count;
    Integer Price;
    Integer Weight;

    public Integer getWeight() {
        return Weight;
    }

    public void setWeight(Integer weight) {
        Weight = weight;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Integer getCount() {
        return Count;
    }

    public void setCount(Integer count) {
        Count = count;
    }

    public Integer getPrice() {
        return Price;
    }


    public void setPrice(Integer price) {
        Price = price;
    }
}
