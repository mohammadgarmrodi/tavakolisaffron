package ir.dayasoft.tavakolisaffron.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import ir.dayasoft.tavakolisaffron.R;
import ir.dayasoft.tavakolisaffron.adapter.CheckStockDialogAdapter;

/**
 * Created by mohammad on 02/20/2017.
 */

public class CreateDialog {
    private Dialog dialog;
    private Context context;

    public CreateDialog(Context context) {
        this.context = context;
        dialog = new Dialog(context, R.style.custom_dialog);
    }

    public void checkProduct(List<String> stringList) {
        dialog.setContentView(R.layout.dialog_checkstock);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.dialog_product_recycleView);
        CheckStockDialogAdapter productListAdapter = new CheckStockDialogAdapter(context, stringList, dialog);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(productListAdapter);
        dialog.show();
    }


}


